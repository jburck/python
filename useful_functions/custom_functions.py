
# #  Custom imputer & data preprocessing functions 

 

import pandas as pd

pd.set_option('display.max_rows', 100)

import numpy as np

import os

import matplotlib.pyplot   as plt

 

path = 'C:/Users/N507830/OneDrive - Nordea/ml_data'.format()

infile='bm_data.CSV'

outfile= "test.csv"

 

df       = pd.read_csv(os.path.join(path,infile), encoding ='latin1')

 


def test_dataset(df, perc_size):

    df_shuffled = df.sample(frac=1, random_state=42)

    df_shuffled.reset_index(drop=True, inplace=True)

    cutoff = int(len(df)*(1-perc_size))

    train = df_shuffled[:cutoff]

    test = df_shuffled[cutoff:]

    return train, test

 


def NumericPreprocessing(parameters=None):

    from sklearn.compose import ColumnTransformer
    from sklearn.impute import SimpleImputer
    from sklearn.linear_model import LogisticRegression
    from sklearn.model_selection import GridSearchCV, RepeatedStratifiedKFold
    from sklearn.pipeline import Pipeline
    from sklearn.preprocessing import OneHotEncoder, StandardScaler, LabelEncoder
    from sklearn.metrics import classification_report

     numeric_transformer = Pipeline(     [('imputer_num', SimpleImputer(strategy = 'median')),

                                        ('scaler', StandardScaler())])
 
    # preprocessor = ColumnTransformer(  [('categoricals', categorical_transformer, cat_cols),
    #                                     ('numericals', numeric_transformer, num_cols)],
    #                                     remainder = 'drop')

     # pipeline = Pipeline(                [('preprocessing', preprocessor),
    #                                     ('clf', LogisticRegression())
    #                                     ])

    # log_cv =    GridSearchCV(
    #             pipeline, 
    #             parameters, 
    #             cv = 5, 
    #             scoring = 'f1'
    #             )

    return numeric_transformer

 

def CategoricalPreprocessing(parameters):

    from sklearn.impute import SimpleImputer

    from sklearn.linear_model import LogisticRegression

    from sklearn.model_selection import GridSearchCV, RepeatedStratifiedKFold

    from sklearn.pipeline import Pipeline

    from sklearn.preprocessing import OneHotEncoder, StandardScaler, LabelEncoder

    categorical_transformer = Pipeline( [

                                        ('imputer_cat', SimpleImputer(strategy = 'constant', fill_value = 'missing')),

                                        ('onehot', OneHotEncoder(handle_unknown ='ignore'))

                                        ])

    return categorical_transformer

 


def UpsampleAndShuffle(X,y, label, samples):

    from sklearn.utils import resample

    xy_joined = X.join(y)

    df_majority = xy_joined[xy_joined[label]==0]

    df_minority = xy_joined[xy_joined[label]==1]

 

    df_minority_upsampled = resample(df_minority, 

                                    replace=True,      # sample with replacement

                                    n_samples=samples,    # to match majority class

                                    random_state=42)   # reproducible results

                                    

    df_upsampled = pd.concat([df_majority, df_minority_upsampled])

    df_upsampled_shuffled = df_upsampled.sample(frac=1)

    df_upsampled_shuffled.reset_index(drop=True, inplace=True)

    return df_upsampled_shuffled.drop(label, axis=1) ,df_upsampled_shuffled[label]


