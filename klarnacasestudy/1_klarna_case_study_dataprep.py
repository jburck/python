import os
import pandas as pd
import numpy as np
from numpy import triu, tril
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder, LabelEncoder, StandardScaler

path =              "D:\Datasets\klarnacasestudy"
infile =            "dataset.csv"
out_training_file = "train.csv"
out_test_file =     "test.csv"
out_score_file =    "score.csv"

test_size =     0.2
random_state =  42
categoricals =  ["merchant_category", "merchant_group", 
                "has_paid", "name_in_email", 
                "account_status", "account_worst_status_0_3m", 
                "account_worst_status_12_24m", "account_worst_status_3_6m", 
                "account_worst_status_6_12m", "worst_status_active_inv",
                "status_last_archived_0_24m", "status_2nd_last_archived_0_24m", 
                "status_3rd_last_archived_0_24m", "status_max_archived_0_6_months", 
                "status_max_archived_0_12_months", "status_max_archived_0_24_months",]
numericals  =   [
                "account_days_in_dc_12_24m",
                "account_days_in_rem_12_24m",  "account_days_in_term_12_24m",
                "account_incoming_debt_vs_paid_0_24m",
                "avg_payment_span_0_12m", "avg_payment_span_0_3m",
                "max_paid_inv_0_12m", "max_paid_inv_0_24m",
                "num_active_div_by_paid_inv_0_12m", "num_arch_written_off_0_12m",
                "num_arch_written_off_12_24m", "time_hours",
                "account_amount_added_12_24m", "age", "num_arch_dc_0_12m",
                "num_arch_dc_12_24m", "num_arch_ok_0_12m",
                "num_arch_ok_12_24m", "num_arch_rem_0_12m",
                "num_unpaid_bills",  "recovery_debt",
                "sum_capital_paid_account_0_12m", "sum_capital_paid_account_12_24m",
                "sum_paid_inv_0_12m"
                ]

###########################
# IMPORT AND STRUCTURE DATA
###########################

df = pd.read_csv(os.path.join(path, infile),  sep = ';')

# Due to an unsolved error with labelencoding on unseen data the data preprocessing steps are undertaken on the whole raw dataset.
# Split training data into numericals and categorials for the sake of data preproccessing
df_cat = df[categoricals]
df_rest = df.drop(categoricals, axis=1)

#Convert categorical variables into numericals & one hot encode
le = LabelEncoder()
df_cat.apply(le.fit_transform)

# Fill missing values with -1 
df_cat = df_cat.fillna(-1)

# Onehot encoding
encoder = OneHotEncoder(categories='auto')
df_cat_1hot = pd.DataFrame(encoder.fit_transform(df_cat).toarray(),columns=encoder.get_feature_names())

#Compile data to then split into data to score and model
df_all = df_cat_1hot.join(df_rest)
df_to_score =   df_all[df_all["default"].isnull()]
df_to_model =   df_all[df_all["default"].isnull() == False]

df_to_model_labels =  df_to_model["default"]
df_to_model_features =    df_to_model.drop("default", axis=1)

#####################################################
# DATA EXPLORATION - First glance at the data  
#####################################################

#Quick look at the data structure from the raw data table
# 43 columns in total. Mix of categorical and numerical features 
df.info()

#Check unique values and the count in the categorical variables - has_paid seems to only hold one value. merchant_category has certain categories with very few observations
for i in categoricals:
    print("\nColumn: " + df[i].name + "\n" , df[i].value_counts() )

# Quick look at the distribution of the numerical featues - May features which are skewed abd potential outliers
df[numericals].hist(bins=50)
plt.show()

# The status cloumns seem to be only holding a few integer values and will be considered categorical
print("Investigate discrete variable, status_last_archived_0_24m: ", df["status_last_archived_0_24m"].unique())

# Investigate the number of unique values in categoriacl columns
print("Count of unique values in categorical columns: \n", df[categoricals].nunique().values)

#####################################################################################################
# DATA EXPLORATION - Check correlations and more exploratory analysis - Note: Should be done solely on calibration sample but due to the premature data preprocessing for categoricals, this is sometimes performed on the full raw data
#####################################################################################################

# Plot count of default
sns.countplot(x='default', data=df_to_model)
plt.show()

# approx 1,5% default rate overall
print("Overall defaultrate is roughly 1,5% \n", df_to_model.groupby("default").count()["uuid"])

# Create correlation matrix
corr_matrix = df.corr()
mask = np.triu(corr_matrix)

# Graphical representatin of correlation as a heatmap
ax = sns.heatmap(
    corr_matrix, 
    vmin=-1, vmax=1, center=0,
    cmap=sns.diverging_palette(20, 220, n=200),
    square=True, mask=mask
    )
ax.set_xticklabels(
    ax.get_xticklabels(),
    rotation=45,
    horizontalalignment='right')
plt.show()

# Find the highest absolute correlations to default so that extra attention can be paid to these.
corr_matrix_transposed = corr_matrix.abs().stack().reset_index()
corr_matrix_transposed.columns = ["col1", "col2", "correlation"]
corr_matrix_transposed.sort_values("correlation", inplace=True)
high_corr = corr_matrix_transposed.loc[(corr_matrix_transposed["correlation"] != 1) & (corr_matrix_transposed["col1"] == "default")].tail(10)
print("10 highest correlating variables with default: \n", high_corr)

# check for missing data, invalid values
print("Missing values in the labels:", df_to_model_labels.isnull().values.any())
print("Missing values in the features:", df_to_model_features.isnull().values.any())

# Create heatmat over features with missing values
plt.figure(figsize=(10,10))
sns.heatmap(df.isnull(), yticklabels=False, cbar=False)
plt.show()

print("Count of missing values per feature: \n", df.isnull().sum())

##############################################################
# DATA CLEANING - impute missing values & feature scaling
##############################################################

# Split data into training and validation datasets 
X_train, X_test, y_train, y_test = train_test_split(df_to_model_features, df_to_model_labels, \
                                    test_size=test_size, random_state=random_state)

# Split training data into numericals and categorials for the sake of data preproccessing
X_train_num = X_train[numericals]

# Impute missing values in training data
imputer = SimpleImputer(strategy="median")
imputer.fit(X_train_num)
print("Imputing values:\n", imputer.statistics_)
X_train_num_imputed = pd.DataFrame(imputer.transform(X_train_num), columns=X_train_num.columns)

# Due to some variables exhibiting heavy right skew and potential outliers (judging from the histograms) standardiation will be used instead of normalization  
scaler = StandardScaler()
scaler.fit(X_train_num_imputed)
X_train_num_preprocessed = pd.DataFrame(scaler.transform(X_train_num_imputed), columns=X_train_num_imputed.columns)

def numerical_processing(df):
    X_train_num_imputed = pd.DataFrame(imputer.transform(df), columns=df.columns)
    X_train_num_preprocessed = pd.DataFrame(scaler.transform(X_train_num_imputed), columns=X_train_num_imputed.columns)
    return X_train_num_preprocessed

##############################################################################
# DATA COMPILATION - Prepare calibration, out of sample and scoring datasets
##############################################################################

# Calibration data
X_train_rest = X_train.drop(numericals, axis=1)
X_train_rest.reset_index(drop=True, inplace=True)
y_train.reset_index(drop=True, inplace=True)
X_train_prepared = X_train_rest.join(X_train_num_preprocessed)
X_train_prepared["default"] = y_train

# Validation data
X_test_num = X_test[numericals]
X_test_rest = X_test.drop(numericals, axis=1)
X_test_processed_num = numerical_processing(X_test_num)
X_test_rest.reset_index(drop=True, inplace=True)
y_test.reset_index(drop=True, inplace=True)
X_test_prepared = X_test_rest.join(X_test_processed_num)
X_test_prepared["default"] = y_test


# Data to score
df_to_score_num = df_to_score[numericals]

#Due to an exception the drop function is not used to create the rest dataset for the scoring data
new_cat_columns = X_train_rest.columns
df_to_score_rest = df_to_score[new_cat_columns]
df_to_score_processed_num = numerical_processing(df_to_score_num)
df_to_score_rest.reset_index(drop=True, inplace=True)
df_to_score_processed_num.reset_index(drop=True, inplace=True)
df_to_score_prepared = df_to_score_rest.join(df_to_score_processed_num)


##############################################################
# DATA EXPORT - Export datasets
##############################################################
# Export final datasets to csv files


X_train_prepared.to_csv(os.path.join(path, out_training_file), index=False)
X_test_prepared.to_csv(os.path.join(path, out_test_file), index=False)
df_to_score_prepared.to_csv(os.path.join(path, out_score_file) , index=False)

