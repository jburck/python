import os
import pandas as pd
import numpy as np
from numpy import triu, tril
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder, LabelEncoder, StandardScaler

path =          "D:\Python\klarnacasestudy"
infile =        "dataset.csv"
outfile =       ""
test_size =     0.2
random_state =  42
categoricals =  ["merchant_category", "merchant_group", 
                "has_paid", "name_in_email", 
                "account_status", "account_worst_status_0_3m", 
                "account_worst_status_12_24m", "account_worst_status_3_6m", 
                "account_worst_status_6_12m", "worst_status_active_inv",
                "status_last_archived_0_24m", "status_2nd_last_archived_0_24m", 
                "status_3rd_last_archived_0_24m", "status_max_archived_0_6_months", 
                "status_max_archived_0_12_months", "status_max_archived_0_24_months",]
numericals  =   [
                "account_days_in_dc_12_24m",
                "account_days_in_rem_12_24m",  "account_days_in_term_12_24m",
                "account_incoming_debt_vs_paid_0_24m",
                "avg_payment_span_0_12m", "avg_payment_span_0_3m",
                "max_paid_inv_0_12m", "max_paid_inv_0_24m",
                "num_active_div_by_paid_inv_0_12m", "num_arch_written_off_0_12m",
                "num_arch_written_off_12_24m", "time_hours",
                "account_amount_added_12_24m", "age", "num_arch_dc_0_12m",
                "num_arch_dc_12_24m", "num_arch_ok_0_12m",
                "num_arch_ok_12_24m", "num_arch_rem_0_12m",
                "num_unpaid_bills",  "recovery_debt",
                "sum_capital_paid_account_0_12m", "sum_capital_paid_account_12_24m",
                "sum_paid_inv_0_12m"
                ]

###########################
# IMPORT AND STRUCTURE DATA
###########################

df =            pd.read_csv(os.path.join(path, infile),  sep = ';')
df_to_score =   df[df["default"].isnull()]
df_to_model =   df[df["default"].isnull() == False]

df_to_model_labels =  df_to_model["default"]
df_to_model_features =    df_to_model.drop("default", axis=1)

#####################################################
# DATA EXPLORATION - First glance at the data  
#####################################################

#Quick look at the data structure from the raw data table
# 43 columns in total. Mix of categorical and numerical features 
df.info()

#Check unique values and the count in the categorical variables - has_paid seems to only hold one value. merchant_category has certain categories with very few observations
for i in categoricals:
    print("\nColumn: " + df[i].name + "\n" , df[i].value_counts() )

# Quick look at the distribution of the numerical featues - May features which are skewed abd potential outliers
df[numericals].hist(bins=50)
plt.show()

# The status cloumns seem to be only holding a few integer values and will be considered categorical
print("Investigate discrete variable, status_last_archived_0_24m: ", df["status_last_archived_0_24m"].unique())

# Investigate the number of unique values in categoriacl columns
print("Count of unique values in categorical columns: \n", df[categoricals].nunique().values)

#####################################################################################################
# DATA EXPLORATION - Check correlations and more exploratory analysis - Note: on calibration sample
#####################################################################################################

# Plot count of default
sns.countplot(x='default', data=df_to_model)
plt.show()

# approx 1,5% default rate overall
print("Overall defaultrate is roughly 1,5% \n", df_to_model.groupby("default").count()["uuid"])

# Create correlation matrix
corr_matrix = df_to_model.corr()
mask = np.triu(corr_matrix)

# Graphical representatin of correlation as a heatmap
ax = sns.heatmap(
    corr_matrix, 
    vmin=-1, vmax=1, center=0,
    cmap=sns.diverging_palette(20, 220, n=200),
    square=True, mask=mask
    )
ax.set_xticklabels(
    ax.get_xticklabels(),
    rotation=45,
    horizontalalignment='right')
plt.show()

# Find the highest absolute correlations to default so that extra attention can be paid to these.
corr_matrix_transposed = corr_matrix.abs().stack().reset_index()
corr_matrix_transposed.columns = ["col1", "col2", "correlation"]
corr_matrix_transposed.sort_values("correlation", inplace=True)
high_corr = corr_matrix_transposed.loc[(corr_matrix_transposed["correlation"] != 1) & (corr_matrix_transposed["col1"] == "default")].tail(10)
print("10 highest correlating variables with default: \n", high_corr)

# check for missing data, invalid values
print("Missing values in the labels:", df_to_model_labels.isnull().values.any())
print("Missing values in the features:", df_to_model_features.isnull().values.any())

# Create heatmat over features with missing values
plt.figure(figsize=(10,10))
sns.heatmap(df_to_model_features.isnull(), yticklabels=False, cbar=False)
plt.show()

print(df_to_model_features.isnull().sum())

##############################################################
# DATA CLEANING - impute missing values, one-hot encoding
##############################################################

# Split data into training and validation datasets 
X_train, X_test, y_train, y_test = train_test_split(df_to_model_features, df_to_model_labels, \
                                    test_size=test_size, random_state=random_state)


# Split training data into numericals and categorials for the sake of data preproccessing
X_train_num = X_train[numericals]
X_train_cat = X_train[categoricals]

# Impute missing values in training data
imputer = SimpleImputer(strategy="median")
imputer.fit(X_train_num)
print("Imputing values:\n", imputer.statistics_)
X_train_num_imputed = pd.DataFrame(imputer.transform(X_train_num), columns=X_train_num.columns)

#Convert categorical variables into numericals & one hot encode
le = LabelEncoder()
X_train_cat.apply(le.fit_transform)

# Fill missing values with -1 
X_train_cat = X_train_cat.fillna(-1)

encoder = OneHotEncoder(categories='auto')
X_train_cat_1hot = pd.DataFrame(encoder.fit_transform(X_train_cat).toarray(),columns=encoder.get_feature_names())

##############################################################
# DATA PREPROCESSING - Feature scaling
##############################################################

# Due to some variables exhibiting heavy right skew and potential outliers (judging from the histograms) standardiation will be used instead of normalization  
scaler = StandardScaler()
X_train_num_preprocessed = pd.DataFrame(scaler.fit_transform(X_train_num_imputed), columns=X_train_num_imputed.columns)

##############################################################
# DATA PREPERATION - Prepare out of sample and scoring datasets
##############################################################

# def cat_data_prep(df):
#     df.apply(le.transform)
#     df = df.fillna(-1)
#     df_cat_1hot = pd.DataFrame(encoder.fit_transform(df).toarray(),columns=encoder.get_feature_names())
#     return df_cat_1hot
pd.options.mode.chained_assignment = None 
X_train_cat

X_test_cat = X_test[categoricals]

for col in X_train_cat:
    le = LabelEncoder()
    le.fit(X_train_cat[col])
    X_train_cat[col] = le.fit_transform(X_train_cat[col])
    le.transform(X_test_cat[[col]])

le = LabelEncoder()
le.fit(X_train_cat["merchant_group"])
X_train_cat["merchant_group"] = le.fit_transform(X_train_cat["merchant_group"])
le.transform(X_test_cat["merchant_group"])



X_train_cat.apply(le.fit_transform)






##############################################################
# DATA COMPILATION - Compile and export datasets
##############################################################
# Compile final dataset
X_train_preprocessed = X_train_cat_1hot.join(X_train_num_preprocessed)
