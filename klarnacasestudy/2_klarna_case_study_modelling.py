import os
import pandas as pd
from sklearn.linear_model import RidgeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV, cross_val_predict, cross_val_score
import warnings
warnings.filterwarnings('ignore')
from sklearn.metrics import recall_score, precision_score , confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sn
from mlxtend.classifier import EnsembleVoteClassifier
import pickle
import time

path =             "D:\Datasets\klarnacasestudy"
in_training_file = "train.csv"
in_test_file =     "test.csv"
in_score_file =    "score.csv"
outfile =           "submission.csv"
random_state =      42
label =             "default"
folds =             3
sample_size =       4000 #Full sample: 71980

# Random forest grid search parameters
rnd_param_grid = {"max_depth": [5,10,15], "n_estimators": [5,10,50], "max_features": [ 100,125, 150]}
# Logistic ridge regression parameters
ridge_param_grid = {"alpha": [0.1, 0.01]}
#Support vector machine parameters
svm_param_grid = {'C':[1,10], 'kernel':['poly']}

###########################
# IMPORT AND STRUCTURE DATA
###########################

df_train = pd.read_csv(os.path.join(path, in_training_file))
df_test = pd.read_csv(os.path.join(path, in_test_file))
df_score = pd.read_csv(os.path.join(path, in_score_file))

y_train = df_train[label]
X_train = df_train.drop(label, axis=1)
X_train.drop(columns="uuid", inplace=True)
y_test = df_test[label]
X_test = df_test.drop(label, axis=1)
X_test.drop(columns="uuid", inplace=True)

def UpsampleAndShuffle(X,y, label, samples):

    from sklearn.utils import resample
    xy_joined = X.join(y)
    df_majority = xy_joined[xy_joined[label]==0]
    df_minority = xy_joined[xy_joined[label]==1]
    df_minority_upsampled = resample(df_minority, 
                                    replace=True,      # sample with replacement
                                    n_samples=samples,    # to match majority class
                                    random_state=random_state)   # reproducible results

    df_upsampled = pd.concat([df_majority, df_minority_upsampled])
    df_upsampled_shuffled = df_upsampled.sample(frac=1)
    df_upsampled_shuffled.reset_index(drop=True, inplace=True)
    return df_upsampled_shuffled.drop(label, axis=1) ,df_upsampled_shuffled[label]

upsample_size = y_train.value_counts()[0]
X_train_upsampled, y_train_upsampled = UpsampleAndShuffle(X_train, y_train, "default", upsample_size)

# Create small samples for execution time during model development
X_train_upsampled = X_train_upsampled[:sample_size]
y_train_upsampled = y_train_upsampled[:sample_size]
X_train = X_train[:sample_size]
y_train = y_train[:sample_size]

##################################
# SELECT AND TRAIN CANDIDATE MODELS
####################################

############ Random forest ############
tic = time.perf_counter()
clf_rnd = RandomForestClassifier(random_state=random_state)
grid_search_rnd = GridSearchCV(clf_rnd, rnd_param_grid, cv=folds, scoring="recall")
grid_search_rnd.fit(X_train,y_train)

#Alternative classifier on upsampled data
grid_search_rnd_up = GridSearchCV(clf_rnd, rnd_param_grid, cv=folds, scoring="recall")
grid_search_rnd_up.fit(X_train_upsampled,y_train_upsampled)

print("Random forest best parameters: ",grid_search_rnd.best_params_)
# Get cross validation predictions and score for Random Forest
clf_rnd_2 = RandomForestClassifier(max_depth=grid_search_rnd.best_params_["max_depth"], \
    n_estimators=grid_search_rnd.best_params_["n_estimators"], \
        max_features=grid_search_rnd.best_params_["max_features"])
clf_rnd_2.fit(X_train,y_train)

pred_rnd = cross_val_predict(clf_rnd_2, X_train, y_train, cv=folds)
scores_rnd = cross_val_score(clf_rnd_2, X_train, y_train, cv=folds, scoring='f1')
toc = time.perf_counter()
print("Random Forest executed in {} seconds".format(toc - tic))

############ Ridge regression ############
tic = time.perf_counter()
clf_ridge = RidgeClassifier(random_state=random_state)
grid_search_ridge = GridSearchCV(clf_ridge, ridge_param_grid, cv=folds, scoring="recall")
grid_search_ridge.fit(X_train,y_train)

#Alternative classifier on upsampled data
grid_search_ridge_up = GridSearchCV(clf_ridge, ridge_param_grid, cv=folds, scoring="recall")
grid_search_ridge_up.fit(X_train_upsampled,y_train_upsampled)

print("Ridge regression best alpha: ", grid_search_ridge.best_params_)
# Get cross validation predictions and score for Ridge regression
pred_ridge = cross_val_predict(grid_search_ridge, X_train, y_train, cv=folds)
scores_ridge = cross_val_score(grid_search_ridge, X_train, y_train, cv=folds, scoring='f1')
toc = time.perf_counter()
print("Regression executed in {} seconds".format(toc - tic))

############ Support Vector Machines classifier ############
tic = time.perf_counter()
clf_svm = SVC(random_state=random_state)
grid_search_svm = GridSearchCV(clf_svm, svm_param_grid, cv=folds, scoring="recall")
grid_search_svm.fit(X_train,y_train)
toc = time.perf_counter()
print("SVM - part 1 - executed in {} seconds".format(toc - tic))

#Alternative classifier on upsampled data
grid_search_svm_up = GridSearchCV(clf_svm, svm_param_grid, cv=folds, scoring="recall")
grid_search_svm_up.fit(X_train_upsampled,y_train_upsampled)

print("SVM best parameters: ", grid_search_svm.best_params_)

clf_svm_2 = SVC(C=grid_search_svm.best_params_["C"] , kernel=grid_search_svm.best_params_["kernel"])
clf_svm_2.fit(X_train,y_train)
pred_svm = cross_val_predict(clf_svm_2, X_train, y_train, cv=folds)
scores_svm = cross_val_score(clf_svm_2, X_train, y_train, cv=folds, scoring='f1')

toc2 = time.perf_counter()
print("SVM executed in {} seconds".format(toc2 - tic))

############ Vote Classifier ############
# fit_base_estimators=False is incompatible to any form of cross-validation that requires refitting
eclf = EnsembleVoteClassifier(clfs=[clf_rnd_2, grid_search_ridge, clf_svm_2], weights=[1,1,1], fit_base_estimators=False)
labels = ['Random Forest', 'SVM', 'Logistic Regression', 'Ensemble']
eclf.fit(X_train, y_train)

############ Vote Classifier # 2 ############
# include classifiers based on upsampled dataset
eclf_2 = EnsembleVoteClassifier(clfs=[clf_rnd_2, grid_search_ridge, clf_svm_2, grid_search_rnd_up, grid_search_ridge_up, grid_search_svm_up], weights=[1,1,1,1,1,1], fit_base_estimators=False)
labels = ['Random Forest', 'SVM', 'Logistic Regression', 'Random Forest ups', 'SVM ups', 'Logistic Regression ups', 'Ensemble']
eclf_2.fit(X_train, y_train)

##################################
# PERFORMANCE MEASURES
##################################
print("Random Forest \n Cross validation mean f1:", scores_rnd.mean(), "\n Precision Score: ", precision_score(y_train, pred_rnd), "\n Recall Score: ", recall_score(y_train, pred_rnd))
print(confusion_matrix(y_train, pred_rnd))
print("Ridge regression \n Cross validation mean f1:", scores_ridge.mean(), "\n Precision Score: ", precision_score(y_train, pred_ridge), "\n Recall Score: ", recall_score(y_train, pred_ridge))
print(confusion_matrix(y_train, pred_ridge))
print("Support Vector Machines \n Cross validation mean f1:", scores_svm.mean(), "\n Precision Score: ", precision_score(y_train, pred_svm), "\n Recall Score: ", recall_score(y_train, pred_svm))
print(confusion_matrix(y_train, pred_svm))

##################################
# Confusion matrix plots
##################################
# Random forest confusion matrix plot
sn.set(font_scale=1.0)
#Matplotlib bug is off-cenetring the annotations
annot_kws = {"size": 16, "ha": 'left',"va": 'center_baseline', "C": "grey"}
sn.heatmap(confusion_matrix(y_train, pred_rnd), annot=True, annot_kws=annot_kws, fmt="d") # font size
plt.show()

# Ridge regression confusion matrix plot
sn.set(font_scale=1.0)
annot_kws = {"size": 16, "ha": 'left',"va": 'center_baseline', "C": "grey"}
sn.heatmap(confusion_matrix(y_train, pred_ridge), annot=True, annot_kws=annot_kws, fmt="d") # font size
plt.show()

# Suppoort Vector Machines confusion matrix plot
sn.set(font_scale=1.0)
annot_kws = {"size": 16, "ha": 'left',"va": 'center_baseline', "C": "grey"}
sn.heatmap(confusion_matrix(y_train, pred_svm), annot=True, annot_kws=annot_kws, fmt="d") # font size
plt.show()

##################################
# TEST SET CROSS VALIDATION
####################################
#Evaluate candidate model predictive power on the test set

# Make predict for the test set 
rnd_test_pred =         clf_rnd_2.predict(X_test)
ridge_test_pred =       grid_search_ridge.predict(X_test)
svm_test_pred =         clf_svm_2.predict(X_test)
ensemble_test_pred =    eclf.predict(X_test)
ensemble_2_test_pred =  eclf_2.predict(X_test)

# Calculate precision
rnd_precision =         precision_score(y_test, rnd_test_pred)
ridge_precision =       precision_score(y_test, ridge_test_pred)
svm_precision =         precision_score(y_test, svm_test_pred)
ensemble_precision =    precision_score(y_test, ensemble_test_pred)
ensemble_2_precision =  precision_score(y_test, ensemble_2_test_pred)

# Calculate recall
rnd_recall =            recall_score(y_test, rnd_test_pred)
ridge_recall =          recall_score(y_test, ridge_test_pred)
svm_recall =            recall_score(y_test, svm_test_pred)
ensemble_recall =       recall_score(y_test, ensemble_test_pred)
ensemble_2_recall =     recall_score(y_test, ensemble_2_test_pred)

print("Random Forest hold out sample validation statistics:\n Precision:", rnd_precision, "\n Recall:", rnd_recall)
print("Logistic ridge regression hold out sample validation statistics:\n Precision:", ridge_precision, "\n Recall:", ridge_recall)
print("Support Vector Machine hold out sample validation statistics:\n Precision:", svm_precision, "\n Recall:", svm_recall)
print("Voting classifier hold out sample validation statistics:\n Precision:", ensemble_precision, "\n Recall:", ensemble_recall)
print("Voting classifier w/ upsampling hold out sample validation statistics:\n Precision:", ensemble_2_precision, "\n Recall:", ensemble_2_recall)

predictions = [rnd_test_pred, ridge_test_pred, svm_test_pred, ensemble_test_pred, ensemble_2_test_pred]
predictions_labels = ["rnd_test_pred", "ridge_test_pred", "svm_test_pred", "ensemble_test_pred", "ensemble_2_test_pred"]

for i, j in zip(predictions, predictions_labels):
    sn.set(font_scale=1.0)
    annot_kws = {"size": 16, "ha": 'left',"va": 'center_baseline', "C": "grey"}
    ax = plt.axes()
    sn.heatmap(confusion_matrix(y_test, i), annot=True, annot_kws=annot_kws, fmt="d", ax = ax)
    ax.set_title(j)
    plt.show()

##################################
# EXPORT AND SCORE
####################################
# Save the classifiers
classifiers = [clf_rnd_2, grid_search_ridge, clf_svm_2, eclf, eclf_2]
classifier_names = ["randomforest", "ridgeregression", "SVM", "Voteclf", "Voteclf_2"]
for i, j in zip(classifiers, classifier_names):
    print("Saving classifier to {}.sav".format(j))
    pickle.dump(i, open(j, 'wb'))

# Score the final dataset for submission with proposed model

ids = df_score["uuid"]
X_score = df_score.drop("uuid", axis=1)
final_preds = clf_rnd_2.predict_proba(X_score)
probabilities = pd.DataFrame(final_preds[:, 1], columns=["pd"])
df_submission = probabilities.join(ids)

df_submission.to_csv(os.path.join(path, outfile), index=False)