
# Google maps API guide: https://developers.google.com/maps/documentation/javascript/get-api-key
# Google Maps API: AIzaSyAy0IvFkqb2Ehxi-70hT0shPyfhU4Ykd3Q
# GEOCODING DOCUMENATTION: https://developers.google.com/maps/documentation/geocoding/overview
# Google cloud platform: https://console.cloud.google.com/apis/credentials?_ga=2.261191629.1573229473.1603623644-119712484.1581861159&project=hemnet-coordinates
# Google developers: https://console.cloud.google.com/freetrial/signup/billing/SE?redirectPath=%2Fgoogle%2Fmaps-apis%2Fwelcome%3F_ga%3D2.223571481.326966309.1603624815-119712484.1581861159%26_gac%3D1.225425768.1603624262.Cj0KCQjwxNT8BRD9ARIsAJ8S5xbLn7e8FkzBah2Nm8LayCKidsyRdm2yXMPTDZvO7LvshM8T78YOjIgaArNkEALw_wcB%26project%3Dhemnet-coordinates%26step%3Dproduct_selection&flow=maps&project=637013270386

import requests
import smtplib
import pandas as pd
import urllib.parse
import pickle

infile = "2021_test_run_sollentuna_prepped_5.csv"
outfile = "2021_test_run_sollentuna_prepped_5_coordinates.csv"
# Load API key
f = open('D:\\MyStuff\\API key\\google_geocode_API.pckl', 'rb')
api_key = pickle.load(f)
f.close()

df = pd.read_csv("D:\\Datasets\\Hemnet\\1_data_prep\\{}".format(infile))

locations = []
lat = []
lng = []

# location should just be the kommun, since this seem to be more stable than area + kommun
for i in df.locations:
    try:
        loc = i.split(", ")[1]
    except:
        loc = i
    locations.append(loc)

df_adress = df.gata + " " + locations

def geoCoder(locations):
    coords = []

    url = "https://maps.googleapis.com/maps/api/geocode/json?"
    for l in range(len(locations)):
        adress = urllib.parse.quote(df_adress[l])
        r = requests.get(url + "address=" + adress + "&key=" + api_key)
        r_json = r.json()
        try:
            lat_tmp = r_json["results"][0]["geometry"]["location"]["lat"]
            lng_tmp = r_json["results"][0]["geometry"]["location"]["lng"]
        except:
            lat_tmp = -1
            lng_tmp = -1
        lat.append(lat_tmp)
        lng.append(lng_tmp)
        print("\n Adress: " + df_adress[l])
        try:
            print("Latitude: ", r_json["results"][0]["geometry"]["location"]["lat"])
            print("Longitude: ", r_json["results"][0]["geometry"]["location"]["lng"])
        except:
            print("Did not find:", l)
        r.close
    return lat, lng

# Populate lat and lng columns
df["lat"], df["lng"] = geoCoder(df_adress)

# Export to csv
df.to_csv("D:\\Datasets\\Hemnet\\\\data_prep\\{}".format(outfile), index=False)