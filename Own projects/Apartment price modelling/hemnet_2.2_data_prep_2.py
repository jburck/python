
#########################################################################
############ HEMNET DATA PREPROCESSING ##################################
#########################################################################
import pandas as pd
from sklearn.preprocessing import MinMaxScaler, PolynomialFeatures, OneHotEncoder
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.model_selection import train_test_split
from pickle import dump, load

label =         "price"
numeric_columns =       ['kvm'] #'lat', 'lng', 'months',  'patio', 'fee', 'cost', 'construction_year', 'years_old' , 'ask_price', 'years', 'balcony', floor
categorical_columns =    [] # "Area2"

infile = "all_links_20_21_batch_0_data_prepped.csv"
outfile = "all_links_20_21_batch_0_data_prepped_2.csv"

df = pd.read_csv("D:\\Datasets\\Hemnet\\1_data_prep\\{}".format(infile))

df_label = df[label]
df_num = df[numeric_columns]

# Filter out extreme coordinates (less than 0,1%)
# df = df.loc[(df["lng"] >= 17.6) & (df["lng"] < 18.2) & (df["lat"] >= 59.1) & (df["lat"] < 59.6)]
# df = df.reset_index(drop=True)

# Impute missing values

num_pipeline = Pipeline(steps=[
        ("imputer", SimpleImputer(strategy="median")),
        ("scaler", MinMaxScaler())
        ])
cat_pipeline = Pipeline(steps= [("onehot", OneHotEncoder(handle_unknown="ignore"))])

preprocessor = ColumnTransformer(
    transformers=[
        ('num_pipelne', num_pipeline, numeric_columns),
        ('cat_pipelne', cat_pipeline, categorical_columns)])

# poly_featues = PolynomialFeatures(degree=2,include_bias=False)
# X_poly = pd.DataFrame(poly_featues.fit_transform(df[numericals]), columns=poly_featues.get_feature_names(df[numericals].columns))

# scaler =        MinMaxScaler()
# X_poly_scaled = pd.DataFrame(scaler.fit_transform(X_poly), columns=X_poly.columns)

X = df.drop(label, axis=1)
y = df[label]
X_train, X_test, y_train, y_test = train_test_split( X, y, test_size=0.2, random_state=42)

from sklearn.linear_model import LinearRegression
ols = Pipeline(steps=[('preprocessor', preprocessor),
                      ('classifier', LinearRegression())])

ols.fit(X_train, y_train)

ols["classifier"].coef_
ols["classifier"].intercept_

# Compile and export data
df_processed = X_poly_scaled.join(df[label])
df_processed.to_csv("D:\\Datasets\\Hemnet\\\\data_prep\\{}".format(outfile), index=False)

# Save data preprocessing objects
# dump(scaler, open('D:\\Python\\Own projects\\Hemnet\\scaler.pkl', 'wb'))
# dump(poly_featues, open('D:\\Python\\Own projects\\Hemnet\\poly_featues.pkl', 'wb'))

