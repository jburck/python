# Web spider with selenium

# Regular expressions: https://regexr.com/35f54
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC
import time
import pandas as pd

start_time = time.time()

# Problematisk länk: https://www.hemnet.se/bostad/lagenhet-3rum-centrala-solna-solna-kommun-hannebergsgatan-11-16484435
# https://www.hemnet.se/salda/lagenhet-3rum-bromma-annedal-stockholms-kommun-pellepennans-gata-3-1287884

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 

infile="D:\\Datasets\\Hemnet\\0_scraped_data\\house_links.csv"
outfile = "house_data.csv"
path = "C:/chromedriver.exe"
max_xpath = 7

driver = webdriver.Chrome(path)
# driver.implicitly_wait(0.5)
df_links = pd.read_csv(infile)

df_links_len = len(df_links)

streets, locations, dates, brokers, links,  ask_prices, prices = ([] for i in range(7))
dict = {
    "Antal rum" : [],
    "Boarea" : [],
    "Biarea" : [],
    "Tomtarea" : [],
    "Balkong" : [],
    "Uteplats" : [],
    "Våning" : [],
    "Byggår" : [],
    "Avgift/månad" : [],
    "Driftskostnad" : []
    }


first_run = True
for link, i in zip(df_links.links, df_links.index):
    
    if i%5 == 0:
        perc = round((i/df_links_len) * 100, 2)
        print(str(perc) + "% done")
    
    driver.get(link)
    
    if first_run:
        time.sleep(1)
        try:
            driver.find_element(By.XPATH, "/html/body/div[8]/div/div/div/div/div/div[2]/div[2]/div[2]/button").click() 
        except:
            pass
        first_run = False

    links.append(link)

    try:
        broker = driver.find_element(By.CLASS_NAME, "broker-card__info")
        brokers.append(broker.text)
    except:
        broker = "Missing"
        brokers.append(broker)
    

    try:
        street = driver.find_element(By.XPATH, "//*[@id='page-content']/div[1]/h1")
        streets.append(street.text)
    except:
        street = "Missing"
        streets.append(street)
    
    try:
        location = driver.find_element(By.CSS_SELECTOR, "#page-content > div.column.large > p")
        locations.append(location.text)
    except:
        location = "Missing"
        locations.append(location)
    
    try:
        date = driver.find_element(By.XPATH, "//*[@id='page-content']/div[1]/p/time")
        dates.append(date.text)
    except:
        date = "Missing"
        dates.append(date)
    
    try:
        price = driver.find_element(By.XPATH, "//*[@id='page-content']/div[1]/div[1]/div[2]/div/span[2]")
        prices.append(price.text)
    except:
        price = "Missing"
        prices.append(price)

    try:
        ask_price = driver.find_element(By.XPATH, "//*[@id='page-content']/div[1]/div[1]/div[3]/dl[1]/dd[2]")
        ask_prices.append(ask_price.text)
    except:
        ask_price = "Missing"
        ask_prices.append(ask_price)
    
# loop over all possible XPATHs and get values depending on /dt reference
    
    skips = 0 # integers to skip in the j-loop due to successful matches that don't have to be iterated
    for i in dict:
        for j in range(max_xpath - skips+1):
            xpath = 3 + j + skips
            try:
                string = driver.find_element(By.XPATH, "//*[@id='page-content']/div[1]/div[1]/div[3]/dl[2]/dt[" + str(xpath) + "]").text 
                if string == i:
                    value = driver.find_element(By.XPATH, "//*[@id='page-content']/div[1]/div[1]/div[3]/dl[2]/dd[" + str(xpath) + "]").text
                    dict[i].append(value)
                    skips += 1
                    break
                elif j == max_xpath - skips:
                    value = "Missing"    
                    dict[i].append(value)
            except:
                value = "Missing"    
                dict[i].append(value)
                break
            
driver.close()
print("Scraping done!")

dict["links"]  = links
dict["brokers"]  = brokers
dict["streets"]  = streets
dict["locations"]  = locations
dict["dates"]  = dates
dict["prices"]  = prices
dict["ask_prices"]  = ask_prices

df = pd.DataFrame(dict)
df.to_csv("D:\\Datasets\\Hemnet\\0_scraped_data\\{}.csv".format(outfile),  header="locations",index=False)

print("Data exported")
print("Execution time: ",time.time() - start_time)
