# Web spider with selenium

# Regular expressions: https://regexr.com/35f54
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC
import time
import re
import os
import threading
import pandas as pd

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 

outfile = "house_links"
path = "C:/chromedriver.exe"
hemnet_path = "https://www.hemnet.se/"

# limited run y/n
limited_run = 'y'
max_btn_nr = 5

import warnings
def fxn():
    warnings.warn("deprecated", DeprecationWarning)
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    fxn()

# For a bigger list of areas in Stockholm, see D:\Datasets\Hemnet\sthlm_locations_list.txt
Areas = [
"Alvesta kommun",
"Älmhults kommun"
]

driver = webdriver.Chrome(path)
driver.implicitly_wait(5) # seconds

results, full_locations, gata, sizes, \
    prices_dates, fee, brokers, links, område, rum, kvm, floor  = ([] for i in range(12))





#Load into DataFrame and export to csv
df = pd.DataFrame(links, columns=['links'])

df.to_csv("D:\\Datasets\\Hemnet\\0_scraped_data\\{}.csv".format(outfile),  header="locations",index=False)
driver.close()
print("Scraping done!")

