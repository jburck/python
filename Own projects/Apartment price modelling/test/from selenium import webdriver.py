from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import re
import os
import threading
import pandas as pd
from selenium.webdriver.chrome.options import Options


path = "C:/chromedriver.exe"



driver = webdriver.Chrome(path)
driver.maximize_window()
#driver.implicitly_wait(30)
wait = WebDriverWait(driver, 30)

driver.get("https://www.hemnet.se/salda/bostader?location_ids%5B%5D=473241&item_types%5B%5D=bostadsratt")
try:
    cookies = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.consent__button-wrapper .hcl-button--primary")))
    driver.execute_script("arguments[0].scrollIntoView(true);", cookies)
    driver.execute_script("arguments[0].click();", cookies)
except:
    print("Could not click")
    driver.quit()
    pass

all_hrefs = driver.find_elements(By.CSS_SELECTOR, "a.hcl-card")

for href in all_hrefs:
    driver.execute_script("arguments[0].scrollIntoView(true);", href)
    link = href.get_attribute('href')
    print(link)