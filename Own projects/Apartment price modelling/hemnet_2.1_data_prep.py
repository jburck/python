import pandas as pd
import numpy as np
import re
from datetime import datetime
import os
import xlsxwriter
print(os.getcwd())

in_path =  "D:\\Datasets\\Hemnet\\1_data_prep"
outpath = "D:\\Datasets\\Hemnet\\1_data_prep"

infile = "all_20_21_data_red_parsed.csv"
outfile = "all_20_21_data_prepped.csv"

df = pd.read_csv(os.path.join(in_path, infile))

##################
# NAME FIXES
##################

# Sollentuna area-name fixes
df["locations_fix"] = df["locations_parsed"]
df.loc[df["locations_parsed"].str.contains("Centrala Sollentuna"), "locations_fix"] = 'Centrum, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Sollentuna Centrum"), "locations_fix"] = 'Centrum, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Helenelund"), "locations_fix"] = 'Helenelund, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Tureberg"), "locations_fix"] = 'Tureberg, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Töjnan"), "locations_fix"] = 'Tureberg, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Silverdal"), "locations_fix"] = 'Silverdal, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Tegelhagen"), "locations_fix"] = 'Silverdal, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Edsängen"), "locations_fix"] = 'Vaxmora, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Törnskogen"), "locations_fix"] = 'Vaxmora, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Solängen"), "locations_fix"] = 'Vaxmora, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Helenelund"), "locations_fix"] = 'Helenelund, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Eriksberg"), "locations_fix"] = 'Helenelund, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Häggvik"), "locations_fix"] = 'Häggvik, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Rotebro"), "locations_fix"] = 'Rotebro, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Edsberg"), "locations_fix"] = 'Edsberg, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Landsnora"), "locations_fix"] = 'Edsberg, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Kvarnskogen"), "locations_fix"] = 'Edsberg, Sollentuna kommun'
df.loc[df["locations_parsed"].str.contains("Väsjön"), "locations_fix"] = 'Edsberg, Sollentuna kommun'

# Solna area-name fixes
df.loc[df["locations_parsed"].str.contains("Solna C"), "locations_fix"] = 'Centrum, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Rudviken"), "locations_fix"] = 'Centrum, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Centrala Solna"), "locations_fix"] = 'Centrum, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Råsunda"), "locations_fix"] = 'Råsunda, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Näckrosen"), "locations_fix"] = 'Råsunda, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Filmstaden"), "locations_fix"] = 'Råsunda, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Skytteholm"), "locations_fix"] = 'Råsunda, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Vireberg"), "locations_fix"] = 'Råsunda, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Huvudsta", case=False), "locations_fix"] = 'Huvudsta, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Västra Skogen"), "locations_fix"] = 'Huvudsta, Solna kommun'
df.loc[(df["locations_parsed"].str.contains("Järva") & df["locations_parsed"].str.contains("Solna")), "locations_fix"] = 'Järvastaden, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Frösunda"), "locations_fix"] = 'Frösunda, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Bergshamra"), "locations_fix"] = 'Bergshamra, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Arenastaden"), "locations_fix"] = 'Arenastaden, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Ritorp"), "locations_fix"] = 'Ritorp, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Haga Norra"), "locations_fix"] = 'Haga, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Hagalund"), "locations_fix"] = 'Haga, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Karolinska"), "locations_fix"] = 'Haga, Solna kommun'
df.loc[df["locations_parsed"].str.contains("Ulriksdal"), "locations_fix"] = 'Ulriksdal, Solna kommun'

# Upplands Väsby area-name fixes
df.loc[(df["locations_parsed"].str.contains("Centralt") & df["locations_parsed"].str.contains("Upplands Väsby kommun")), "locations_fix"] = 'Centrala Väsby, Upplands Väsby kommun'
df.loc[(df["locations_parsed"].str.contains("Centrala") & df["locations_parsed"].str.contains("Upplands Väsby kommun")), "locations_fix"] = 'Centrala Väsby, Upplands Väsby kommun'
df.loc[(df["locations_parsed"].str.contains("Apoteksskogen") & df["locations_parsed"].str.contains("Upplands Väsby kommun")), "locations_fix"] = 'Centrala Väsby, Upplands Väsby kommun'
df.loc[df["locations_parsed"].str.contains("Bollstanäs"), "locations_fix"] = 'Bollstanäs, Upplands Väsby kommun'
df.loc[df["locations_parsed"].str.contains("Grimstaby, Upplands Väsby kommun"), "locations_fix"] = 'Bollstanäs, Upplands Väsby kommun'
df.loc[df["locations_parsed"].str.contains("Prästgårdsmarken, Upplands Väsby kommun"), "locations_fix"] = 'Carlslund, Upplands Väsby kommun'
df.loc[df["locations_parsed"].str.contains("Brunnby Park, Upplands Väsby kommun"), "locations_fix"] = 'Carlslund, Upplands Väsby kommun'
df.loc[(df["locations_parsed"].str.contains("Karlslund") & df["locations_parsed"].str.contains("Upplands Väsby kommun")), "locations_fix"] = 'Carlslund, Upplands Väsby kommun'
df.loc[df["locations_parsed"].str.contains("Njursta"), "locations_fix"] = 'Runby, Upplands Väsby kommun'
df.loc[df["locations_parsed"].str.contains("Prästhagen, Upplands Väsby kommun"), "locations_fix"] = 'Runby, Upplands Väsby kommun'

# Kista-Rinkeby name fixes
df.loc[df["locations_parsed"].str.contains("Akalla", case=False), "locations_fix"] = 'Akalla, Kista-Rinkeby'
df.loc[df["locations_parsed"].str.contains("Husby", case=False), "locations_fix"] = 'Husby, Kista-Rinkeby'
df.loc[df["locations_parsed"].str.contains("Rinkeby", case=False), "locations_fix"] = 'Rinkeby, Kista-Rinkeby'
df.loc[df["locations_parsed"].str.contains("Kista", case=False), "locations_fix"] = 'Kista, Kista-Rinkeby'

# Sundbyberg name fixes
df.loc[df["locations_parsed"].str.contains("Ursvik", case=False), "locations_fix"] = 'Ursvik, Sundbybergs kommun'
df.loc[df["locations_parsed"].str.contains("Rissne", case=False), "locations_fix"] = 'Rissne, Sundbybergs kommun'
df.loc[df["locations_parsed"].str.contains("Lilla Alby", case=False), "locations_fix"] = 'Lilla Alby, Sundbybergs kommun'
df.loc[(df["locations_parsed"].str.contains("Centralt") & df["locations_parsed"].str.contains("Sundbybergs kommun")), "locations_fix"] = 'Centrala Sundbyberg, Sundbybergs kommun'
df.loc[(df["locations_parsed"].str.contains("Centrala") & df["locations_parsed"].str.contains("Sundbybergs kommun")), "locations_fix"] = 'Centrala Sundbyberg, Sundbybergs kommun'
df.loc[(df["locations_parsed"].str.contains("Centrum") & df["locations_parsed"].str.contains("Sundbybergs kommun")), "locations_fix"] = 'Centrala Sundbyberg, Sundbybergs kommun'
df.loc[(df["locations_parsed"].str.contains("Järva") & df["locations_parsed"].str.contains("Sundbybergs kommun")), "locations_fix"] = 'Järvastaden, Sundbybergs kommun'
df.loc[df["locations_parsed"].str.contains("Brotorp", case=False), "locations_fix"] = 'Järvastaden, Sundbybergs kommun'
df.loc[df["locations_parsed"].str.contains("Bällsta", case=False), "locations_fix"] = 'Bällsta, Sundbybergs kommun'
df.loc[df["locations_parsed"].str.contains("Bergaliden", case=False), "locations_fix"] = 'Duvbo, Sundbybergs kommun'

# Bromma name fixes
df.loc[df["locations_parsed"].str.contains("Traneberg", case=False), "locations_fix"] = 'Traneberg, Bromma'
df.loc[df["locations_parsed"].str.contains("Blackeberg", case=False), "locations_fix"] = 'Blackeberg, Bromma'
df.loc[df["locations_parsed"].str.contains("Mariehäll", case=False), "locations_fix"] = 'Mariehäll, Bromma'
df.loc[df["locations_parsed"].str.contains("Bällsta", case=False), "locations_fix"] = 'Mariehäll, Bromma'
df.loc[df["locations_parsed"].str.contains("Riksby", case=False), "locations_fix"] = 'Riksby, Bromma'
df.loc[df["locations_parsed"].str.contains("Annedal", case=False), "locations_fix"] = 'Annedal, Bromma'
df.loc[df["locations_parsed"].str.contains("Beckomberga", case=False), "locations_fix"] = 'Beckomberga, Bromma'
df.loc[df["locations_parsed"].str.contains("Söderberga", case=False), "locations_fix"] = 'Beckomberga, Bromma'
df.loc[df["locations_parsed"].str.contains("Abrahamsberg", case=False), "locations_fix"] = 'Abrahamsberg, Bromma'
df.loc[df["locations_parsed"].str.contains("Äppelviken", case=False), "locations_fix"] = 'Äppelviken, Bromma'
df.loc[df["locations_parsed"].str.contains("Norra Ängby", case=False), "locations_fix"] = 'Norra Ängby, Bromma'
df.loc[df["locations_parsed"].str.contains("Ulvsunda", case=False), "locations_fix"] = 'Ulvsunda, Bromma'
df.loc[df["locations_parsed"].str.contains("Nockeby", case=False), "locations_fix"] = 'Nockeby, Bromma'
df.loc[df["locations_parsed"].str.contains("Brommaplan", case=False), "locations_fix"] = 'Brommaplan, Bromma'
df.loc[df["locations_parsed"].str.contains("Åkeshov", case=False), "locations_fix"] = 'Åkeshov, Bromma'
df.loc[df["locations_parsed"].str.contains("Minneberg", case=False), "locations_fix"] = 'Minneberg, Bromma'
df.loc[df["locations_parsed"].str.contains("Stora Mossen", case=False), "locations_fix"] = 'Stora Mossen, Bromma'
df.loc[df["locations_parsed"].str.contains("Alvik", case=False), "locations_fix"] = 'Alvik, Bromma'
df.loc[df["locations_parsed"].str.contains("Åkeslund", case=False), "locations_fix"] = 'Åkeslund, Bromma'
df.loc[df["locations_parsed"].str.contains("Olovslund", case=False), "locations_fix"] = 'Olovslund, Bromma'
df.loc[df["locations_parsed"].str.contains("Johannesfred", case=False), "locations_fix"] = 'Johannesfred, Bromma'
df.loc[df["locations_parsed"].str.contains("Bromma, Stockholms kommun", case=False), "locations_fix"] = 'Bromma, Bromma'
df.loc[df["locations_parsed"].str.contains("Eneby", case=False), "locations_fix"] = 'Eneby, Bromma'
df.loc[df["locations_parsed"].str.contains("Ålsten", case=False), "locations_fix"] = 'Ålsten, Bromma'
df.loc[df["locations_parsed"].str.contains("Smedslätten", case=False), "locations_fix"] = 'Smedslätten, Bromma'

# Hässelby-Vällingby name fixes
df.loc[df["locations_parsed"].str.contains("Parkstad", case=False), "locations_fix"] = 'Råcksta, Hässelby-Vällingby'
df.loc[df["locations_parsed"].str.contains("Råcksta", case=False), "locations_fix"] = 'Råcksta, Hässelby-Vällingby'
df.loc[df["locations_parsed"].str.contains("Hässelby", case=False), "locations_fix"] = 'Hässelby, Hässelby-Vällingby'
df.loc[df["locations_parsed"].str.contains("Vällingby Centrum", case=False), "locations_fix"] = 'Vällingby Centrum, Hässelby-Vällingby'
df.loc[(df["locations_parsed"].str.contains("Grimsta") & df["locations_parsed"].str.contains("Stockholm")), "locations_fix"] = 'Grimsta, Hässelby-Vällingby'
df.loc[df["locations_parsed"].str.contains("Johannelund", case=False), "locations_fix"] = 'Johannelund, Hässelby-Vällingby'
df.loc[df["locations_parsed"].str.contains("Vinsta", case=False), "locations_fix"] = 'Vinsta, Hässelby-Vällingby'
df.loc[df["locations_parsed"].str.contains("Stockholm – Vällingby", case=False), "locations_fix"] = 'Vällingby, Hässelby-Vällingby'
df.loc[df["locations_parsed"].str.contains("Vällingby, Stockholms kommun", case=False), "locations_fix"] = 'Vällingby, Hässelby-Vällingby'
df.loc[df["locations_parsed"].str.contains("Lövsta Allè", case=False), "locations_fix"] = 'Lövsta Allè, Hässelby-Vällingby'
df.loc[df["locations_parsed"].str.contains("Solhem", case=False), "locations_fix"] = 'Solhem, Hässelby-Vällingby'

# Järfälla name fixes
df.loc[df["locations_parsed"].str.contains("Jakobsberg", case=False), "locations_fix"] = 'Jakobsberg, Järfälla kommun'
df.loc[df["locations_parsed"].str.contains("Kallhäll", case=False), "locations_fix"] = 'Kallhäll, Järfälla kommun'
df.loc[df["locations_parsed"].str.contains("Barkarby", case=False), "locations_fix"] = 'Barkarby, Järfälla kommun'
df.loc[df["locations_parsed"].str.contains("Viksjö", case=False), "locations_fix"] = 'Viksjö, Järfälla kommun'
df.loc[df["locations_parsed"].str.contains("Söderdalen", case=False), "locations_fix"] = 'Söderdalen, Järfälla kommun'
df[["Area1", "Area2"]] = df["locations_fix"].str.split(', ',n=1,expand=True)
df["Area1"] = df["Area1"].str.strip()
df["Area2"] = df["Area2"].str.strip()

# Area 2 blank space name fixes
df.loc[(df["Area1"].str.contains("Solna", case=False) & df["Area2"].isnull()), "Area2"] = 'Solna kommun'
df.loc[(df["Area1"].str.contains("Bromma", case=False) & df["Area2"].isnull()), "Area2"] = 'Bromma'
df.loc[(df["Area1"].str.contains("Bolinder strand", case=False) & df["Area2"].isnull()), "Area2"] = 'Järfälla kommun'
df.loc[(df["Area1"].str.contains("Sollentuna", case=False) & df["Area2"].isnull()), "Area2"] = 'Sollentuna kommun'
df.loc[(df["Area1"].str.contains("Stockholm", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Aspudden", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Aspudden", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Kungsholmen", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Vasastan", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Södermalm", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Fruängen", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Östermalm", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Blåsut", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Hammarby Sjöstad", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Liljeholmskajen", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Midsommarkransen", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Spånga", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Telefonplan", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Hägersten", case=False) & df["Area2"].isnull()), "Area2"] = 'Stockholms kommun'
df.loc[(df["Area1"].str.contains("Sundbyberg", case=False) & df["Area2"].isnull()), "Area2"] = 'Sundbybergs kommun'
df.loc[(df["Area1"].str.contains("Vällingby", case=False) & df["Area2"].isnull()), "Area2"] = 'Hässelby-Vällingby'


# Area 2 fallback fixes

Area2 = df["Area2"].copy()
location_list = ['Stockholms kommun','Sundbybergs kommun', 'Solna kommun', 'Bromma', 'Sollentuna kommun']

for i in range(len(Area2)):
    if Area2[i] == None:
        Area2[i] = "Missing"
    else:
        for j in location_list:
            if j in Area2[i]:
                Area2[i] = j
df["Area2"] = Area2       
print(df.Area2.value_counts())

##########################
# RENAME AND DROP
##########################

df.drop(["locations_fix", "locations_parsed"], axis=1, inplace=True)
df.rename(columns={"prices_parsed": "price", 
                    "streets_parsed": "street",
                    "våning_parsed": "floor",
                    "ask_prices_parsed": "ask_price",
                    "brokers2": "broker",
                    "fees": "fee",
                    "costs": "cost"
                    }, inplace=True)

##########################
# FEATURE ENGINEERING & CLEANING
##########################
# Create age column for apartment at purchase. Remove older than 200 years.
df["years_old"] = df.years - df.construct_year
df.loc[(df["years_old"] < 0) | (df["years_old"] >= 200), "years_old"] = -1
df.loc[(df.floor == -1) & (df.patio == 1), "floor"] = 1

# Drop observations with missing target variable
df = df.loc[df.price != -1]

# Construction year / decade indicator?

# Translate into missing values
df.replace(-1, np.nan, inplace=True)

##########################
# EXPORT DATA
##########################
df.to_csv(os.path.join(outpath, outfile), index=False)

# Use ExcelWriter to stop text to e translated as URLS since excel has a limmit of 65k URLS per workbook
writer = pd.ExcelWriter(os.path.join(outpath,"prepped_data.xlsx"), engine='xlsxwriter', options={'strings_to_urls': False})
# Convert the dataframe to an XlsxWriter Excel object.
df.to_excel(writer, sheet_name='Data')
# Close the Pandas Excel writer and output the Excel file.
writer.save()


# df.to_excel(os.path.join(outpath,"prepped_data.xlsx"))
print("Done prepping!")