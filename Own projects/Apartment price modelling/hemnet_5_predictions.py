
import pandas as pd
import numpy as np
import pickle
from sklearn.linear_model import LinearRegression, ElasticNet
from sklearn.preprocessing import PolynomialFeatures, MinMaxScaler
from sklearn.ensemble import RandomForestRegressor

random_state = 42

df = pd.read_csv("D:\\Datasets\\Hemnet\\1_data_prep\\hemnet_complete_prepped.csv")
numericals =    ['kvm', 'rum', 'fee', 'floor', 'lat', 'lng', 'year', 'month']
df_num = df[numericals]

#### LOAD MODELS AND DATA PREP ###
scaler = pickle.load(open("D:\\Python\\Own projects\\Hemnet\\scaler.pkl", 'rb'))
scaler_y = pickle.load(open("D:\\Python\\Own projects\\Hemnet\\scaler_y.pkl", 'rb'))
poly_features = pickle.load(open("D:\\Python\\Own projects\\Hemnet\\poly_featues.pkl", 'rb'))
reg_ols = pickle.load(open("D:\\Python\\Own projects\\Hemnet\\stored_models\\reg_ols.pkl", 'rb'))
reg_elastic = pickle.load(open("D:\\Python\\Own projects\\Hemnet\\stored_models\\reg_elastic.pkl", 'rb'))
reg_rnd = pickle.load(open("D:\\Python\\Own projects\\Hemnet\\stored_models\\reg_rnd.pkl", 'rb'))
reg_svr = pickle.load(open("D:\\Python\\Own projects\\Hemnet\\stored_models\\reg_svr.pkl", 'rb')) 

#### PREDICT SINGLE CASE ###
# Östervägen 14B: [41, 2,2500,3,59.363339,18.001249,7,10]
# Jungfrudansen: 59.348871633175705,17.985932859771268
# Odenplpan: 59.34211513478174, 18.049052390496318
# Rinkeby: 59.38739081083896, 17.92643045951792
# Häggvik: 59.44175409549479, 17.940581782625777

forecast = pd.DataFrame([[41, 2,2500,3, 59.363339,18.001249,7,10]], columns=df_num.columns)
forecast_poly = pd.DataFrame(poly_features.transform(forecast), columns=poly_features.get_feature_names(df_num.columns))
forecast_processed = pd.DataFrame(scaler.transform(forecast_poly), columns=forecast_poly.columns)

X_predict_ols = forecast_processed[["rum", "lat", "lng", "lat lng"]]
X_predict_ols_2d = X_predict_ols.values.reshape(1,-1)

one_prediction = reg_ols.predict(X_predict_ols_2d)
print("OLS predicted price :",round(one_prediction[0]))

X_predict_2d_poly = forecast_processed.values.reshape(1,-1)
one_prediction_elastic = reg_elastic.predict(X_predict_2d_poly)
print("Elastic Net Predicted price:",round(one_prediction_elastic[0]))

X_predict_2d = forecast_processed[numericals].values.reshape(1,-1)
one_prediction_rnd = reg_rnd.predict(X_predict_2d)
print("Random Forest Predicted price:",round(one_prediction_rnd[0]))


#### PREDICT MULTIPLE CASES ###

X_to_score = df[numericals]

# Make all attributes except lat and lng the same as the forecast example above
forecast_proc_num = forecast_processed[numericals]
for i in numericals:
    if i not in ("lat", "lng"):
        X_to_score[i] = forecast_proc_num[i][0]

df_no_transform = pd.DataFrame(scaler.inverse_transform(df.iloc[:,:44]), columns=df.iloc[:,:44].columns)

# RANDOM FOREST
prediction_rnd = reg_rnd.predict(X_to_score)

# OLS
X_to_score_ols = X_to_score[["rum", "lat", "lng"]]
X_to_score_ols = X_to_score_ols.join(df["lat lng"])
prediction_ols = reg_ols.predict(X_to_score_ols)

# SVR
X_to_score_svr = X_to_score
prediction_svr = reg_svr.predict(X_to_score_svr)

prediction_svr = scaler_y.inverse_transform(prediction_svr.reshape(-1,1))

# Consolidate and export predictions
df_no_transform["rand_forest_prediction"] = prediction_rnd
df_no_transform["ols_prediction"] = prediction_ols
df_no_transform["svr_prediction"] = prediction_svr
df_no_transform["price"] = df["price"]
df_no_transform[["kvm", "rum", "fee", "floor", "lat", "lng", "year", "month", "rand_forest_prediction", "ols_prediction","svr_prediction","price"]].to_csv("D:\\Datasets\\Hemnet\\scored_data\\predictions.csv", index=False)




# Idé loopa över modell-iterationer och outputta heatmaps för varje iteration. Typ regression som kör med högre och högre polynom. ..... eller nåt