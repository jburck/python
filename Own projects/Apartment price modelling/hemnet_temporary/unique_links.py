# Take links from 2020 dataframe and 2021 links. Combine them and remove duplicates. Then use this in web_spider_2 to scrape each webpage.

import pandas as pd

df_new_links = pd.read_csv("D:\\Datasets\\Hemnet\\0_scraped_data\\2021_full_run_links.csv")
df_old_full = pd.read_csv("D:\\Datasets\\Hemnet\\0_scraped_data\\full_run_201116.csv")

df_old_links = df_old_full["links"]
df_new_links_2 = df_new_links["links"]

df_all_links = df_new_links_2.append(df_old_links, ignore_index=True)
df_all_links.drop_duplicates(inplace=True)
df_all_links = df_all_links.sample(frac=1).reset_index(drop=True)
df_all_links_1_perc = df_all_links.sample(frac=0.01).reset_index(drop=True)
df_all_links_01_perc = df_all_links.sample(frac=0.001).reset_index(drop=True)

df_all_links.to_csv("D:\\Datasets\\Hemnet\\0_scraped_data\\all_links_20_21.csv", index=False)
df_all_links_1_perc.to_csv("D:\\Datasets\\Hemnet\\0_scraped_data\\all_links_20_21_1_perc.csv", index=False)
df_all_links_01_perc.to_csv("D:\\Datasets\\Hemnet\\0_scraped_data\\all_links_20_21_01_perc.csv", index=False)


# Re-run links where information failed to be gathered

df_b0 = pd.read_csv("D:\\Datasets\\Hemnet\\0_scraped_data\\all_links_20_21_batch_0_data.csv")
df_b1 = pd.read_csv("D:\\Datasets\\Hemnet\\0_scraped_data\\all_links_20_21_batch_1_data.csv")

b0_links = df_b0.loc[df_b0["prices"] == "Missing"]["links"]
b1_links = df_b1.loc[df_b0["prices"] == "Missing"]["links"]