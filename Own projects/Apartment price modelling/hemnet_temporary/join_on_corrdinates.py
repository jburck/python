# join on coordinates from the 2020 run to the 2021 run

import pandas as pd
import xlsxwriter

df_old = pd.read_csv("D:\\Datasets\\Hemnet\\1_data_prep\\2020 data\\hemnet_complete_coordinates.csv")
df_new = pd.read_csv("D:\\Datasets\\Hemnet\\1_data_prep\\all_20_21_data_prepped.csv")
df_old = df_old[["links", "floor", "lat", "lng"]]

df_joined = pd.merge(df_new, df_old, on="links", how="left")

# Combine old floor and new floor into one
df_joined["floor"] = df_joined.floor_x.combine_first(df_joined.floor_y)


df_joined.to_csv("D:\\Datasets\\Hemnet\\1_data_prep\\all_20_21_data_prepped_joined.csv", index=False)

# Use ExcelWriter to stop text to e translated as URLS since excel has a limmit of 65k URLS per workbook
writer = pd.ExcelWriter("D:\\Datasets\\Hemnet\\1_data_prep\\prepped_data_joined.xlsx", engine='xlsxwriter', options={'strings_to_urls': False})
# Convert the dataframe to an XlsxWriter Excel object.
df_joined.to_excel(writer, sheet_name='Data')
# Close the Pandas Excel writer and output the Excel file.
writer.save()


print("Done!")