import pandas as pd
import os

infile="D:\\Datasets\\Hemnet\\0_scraped_data\\all_links_20_21.csv"
outpath = "D:\\Datasets\\Hemnet\\0_scraped_data\\"

df_links = pd.read_csv(infile)
n_samples = 5
d = {}

for i in range(n_samples):
    start = round(i/n_samples * len(df_links))
    end = round((i+1)/n_samples * len(df_links))
    print(start,end)
    d[i] = pd.DataFrame()

    d[i] = df_links[start:end]
    filename = "all_links_20_21_batch_" + str(i) + ".csv"
    d[i].to_csv(os.path.join(outpath, filename), index=False)
