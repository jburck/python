import pickle
from numpy.core.numeric import cross
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from pandas.core.frame import DataFrame
from sklearn import tree
from sklearn.dummy import DummyRegressor
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import LinearRegression, Lasso
from sklearn.ensemble import RandomForestRegressor

label = "price"
features_dic = {"kvm":41, "rum":2, "floor":3, "fee":3500}
columns = ["kvm", "rum", "floor", "fee", "lat", "lng"]
features = ["kvm", "rum", "floor", "fee"]
# Östervägen 14: 59.363339,  18.001249, 41, 2
lng_grid = 21
lat_grid = 21

df = pd.read_csv("D:\\Datasets\\Hemnet\\1_data_prep\\hemnet_complete_coordinates_2020.csv")
# Reshuffle the data and select samlpe size
df = df.sample(frac=1).reset_index(drop=True)

#####################
# Feature engineering
#####################
df["Östermalm"] = np.where(df["locations"].str.contains("Östermalm"),1,0)
df["Vasastan"] = np.where(df["locations"].str.contains("Vasastan"),1,0)
df["Solna"] = np.where(df["locations"].str.contains("Solna"),1,0)
df["Sundbyberg"] = np.where(df["locations"].str.contains("Sundbyberg"),1,0)
df["Kungsholmen"] = np.where(df["locations"].str.contains("Kungsholmen"),1,0)
df["Huvudsta"] = np.where(df["locations"].str.contains("Huvudsta"),1,0)

# Remove extreme coordinates (less than 0,1%)
df = df.loc[(df["lng"] >= 17.803321399999994) & (df["lng"] <= 18.1417491) & (df["lat"] >= 59.23159980000001) & (df["lat"] <= 59.49046099999999)]

# Define the bounds for sectioning variable
lng_borders = np.linspace(df.lng.min(),df.lng.max(),lng_grid)
lat_borders = np.linspace(df.lat.min(),df.lat.max(),lat_grid)

features_more = features
for i in lng_borders:
    for j in lat_borders:
        colname = str(round(i,2)) + "x" + str(round(j,2))
        if (i != df.lng.min()) & (j != df.lat.min()):
            df[colname] = np.where( (previous_i < df["lng"]) & (df["lng"] <= i) & (previous_j < df["lat"]) & (df["lat"] <= j) ,1,0)
            features_more.append(colname)
            columns.append(colname)
        previous_j = j
    previous_i = i

# X = df[features]
X = df[features]
y = df[label]
X_train_poly, X_test_poly, y_train, y_test = train_test_split( X, y, test_size=0.1, random_state=42)
####################
# Dummy regressor
####################
reg_dummy = DummyRegressor(strategy="mean")
reg_dummy_score_mae = cross_val_score(reg_dummy, X, y, scoring="neg_mean_absolute_error")
print("Mean model MAE: " , (sum(reg_dummy_score_mae) / float(len(reg_dummy_score_mae))))
####################
# Tree regressor
####################
reg_tree = DecisionTreeRegressor(random_state=42,  max_depth=2, min_samples_split=500) # min_samples_split=1000
reg_tree_score_mae = cross_val_score(reg_tree, X, y, scoring="neg_mean_absolute_error")
reg_tree.fit(X,y)
print("Feature importance",reg_tree.feature_importances_)
print("Tree model MAE: " , (sum(reg_tree_score_mae) / float(len(reg_tree_score_mae))))
# Print tree
plt.figure(figsize=(5, 5)) # Resize figure
tree.plot_tree(reg_tree)
plt.show()
####################
# Lasso
####################
reg_ols = Lasso()
reg_ols_score_mae = cross_val_score(reg_ols, X, y, scoring="neg_mean_absolute_error")
reg_ols.fit(X,y)
print("OLS model MAE: " , (sum(reg_ols_score_mae) / float(len(reg_ols_score_mae))))
print("  OLS Model:", "intercept", reg_ols.intercept_)
print("  OLS x variables:", X.columns, "Slope coefficients:", reg_ols.coef_)
####################
# Randomforest
####################
reg_rf = RandomForestRegressor(n_estimators= 5, min_samples_leaf=10 )
reg_rf_score_mae = cross_val_score(reg_rf, X, y, scoring="neg_mean_absolute_error")
reg_rf.fit(X,y)
print("RF model MAE: " , (sum(reg_rf_score_mae) / float(len(reg_rf_score_mae))))

##########
# Score data for plot
##########
forecast = pd.DataFrame(columns=X.columns)
df = df[columns]
for i in features_dic:
    df[i] = features_dic[i]

#Predict for plot
predictions_tree = reg_tree.predict(df[features_more])
df["pred_tree"] = predictions_tree

predictions_ols = reg_ols.predict(df[features_more])
df["pred_ols"] = predictions_ols

predictions_rf = reg_rf.predict(df[features_more])
df["pred_rf"] = predictions_rf

##########
# PLOTTING
##########

# Remove extreme coordinates (less than 0,1%)
#df = df.loc[(df["lng"] >= 17.803321399999994) & (df["lng"] <= 18.1417491) & (df["lat"] >= 59.23159980000001) & (df["lat"] <= 59.49046099999999)]
# Define the bounds for the map
BBox = (df.lng.min(),   df.lng.max(),      
         df.lat.min(), df.lat.max())

# df_to_plot = df.loc[    (df["kvm"] > 0)      &   (df["rum"] >= 2)    ] 

# How to superimpose coordinates on an map-image: https://towardsdatascience.com/easy-steps-to-plot-geographic-data-on-a-map-python-11217859a2db
sthlm_m = plt.imread('D:\Datasets\Hemnet\maps\map_bw.png')

# good colorschemed = gist_rainbow, PuBu, jet
fig, ax = plt.subplots(figsize=(15,15))
sc = ax.scatter(df.lng, df.lat, zorder=1, alpha=0.8, c=df["pred_ols"], s=8, cmap="hsv")
plt.colorbar(sc)
ax.set_title('Plotting Spatial Data on Sockholm')
ax.set_xlim(BBox[0],BBox[1])
ax.set_ylim(BBox[2],BBox[3])
ax.imshow(sthlm_m, zorder=0, extent = BBox, aspect= 'equal')
plt.show()


print("Done")