#######################################################################
############ HEMNET DATA EXPLORATION ##################################
#######################################################################
import os
os.environ["PROJ_LIB"] = "C:\\Users\\Johan\\Anaconda3\\pkgs\\proj4-5.2.0-ha925a31_1\\Library\\share"
#from mpl_toolkits.basemap import Basemap
from pandasql import sqldf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
from matplotlib.colors import ListedColormap
import seaborn as sns
import random

price_kvm_low_lim = 20000
price_kvm_upp_lim = 120000
price_kvm_low_lim_solna = 30000
price_kvm_upp_lim_solna = 80000
residual_low_lim = -200000
residual_upp_lim = 200000
n_gridlines = 10

############################### DATA IMPORT ##############################################
# Origianl dataset
df_orig = pd.read_csv("C:\Users\jbuep\My Drive\Data\Hemnet data\\hemnet_complete_coordinates_2020.csv")

# Data with predictions from program 5
df_pred = pd.read_csv("D:\\Datasets\\Hemnet\\scored_data\\predictions.csv")

df = df_pred.join(df_orig["locations"])

########################### FEATUREE ENGINEERING ################################
df["price/kvm"] = df["price"] / df["kvm"]
df["rnd_residual"] = df["rand_forest_prediction"] - df["price"]
df["ols_residual"] = df["ols_prediction"] - df["price"]
df["svr_residual"] = df["svr_prediction"] - df["price"]

# Create kommun variable
kommun = []
for i in df.locations:
    try:
        loc = i.split(", ")[1]
    except:
        loc = i
    kommun.append(loc)
df["kommun"] = kommun

########################### REDUCE DATA FOR PLOT ################################

# Remove extreme coordinates (less than 0,1%)
df = df.loc[(df["lng"] >= 17.803321399999994) & (df["lng"] <= 18.1417491) & (df["lat"] >= 59.23159980000001) & (df["lat"] <= 59.49046099999999)]
df_solna = df.loc[df["kommun"] == "Solna"]
df_liljeholmen = df.loc[(df["lng"] >= 17.9839) & (df["lng"] <= 18.0383) & (df["lat"] >= 59.3002) & (df["lat"] <= 59.3185)]


# Define the bounds for the map
BBox = (df.lng.min(),   df.lng.max(),      
         df.lat.min(), df.lat.max())

BBox_solna = (df_solna.lng.min(),   df_solna.lng.max(),      
            df_solna.lat.min(), df_solna.lat.max())

BBox_liljeholmen = (df_liljeholmen.lng.min(),   df_liljeholmen.lng.max(),      
            df_liljeholmen.lat.min(), df_liljeholmen.lat.max())


df_to_plot = df.loc[
    (df["kvm"] > 0)  
    & (df["rum"] >= 2)
    ] 

df_to_plot_solna = df_solna.loc[
    (df["kvm"] > 0) 
    & (df["rum"] >= 2)
    ]

df_to_plot_liljeholmen = df_liljeholmen.loc[
    (df["kvm"] > 0) 
    & (df["rum"] >= 2)
    ]

# Get values to plot
vals = df_to_plot["price/kvm"].values
vals_solna = df_to_plot_solna["price/kvm"].values


############################## SETTINGS FOR THE PLOTS ################################# 
# How to superimpose coordinates on an map-image: https://towardsdatascience.com/easy-steps-to-plot-geographic-data-on-a-map-python-11217859a2db

sthlm_m = plt.imread('D:\Datasets\Hemnet\maps\map_bw.png')
solna_m = plt.imread('D:\Datasets\Hemnet\maps\solna_bw.png')
liljeholmen_m = plt.imread('D:\Datasets\Hemnet\maps\liljeholmen_bw.png')

########### STOCKHOLM SCATTERPLOT ##########
# good colorschemed = gist_rainbow, PuBu, jet
fig, ax = plt.subplots(figsize=(20,16))
sc = ax.scatter(df_to_plot.lng, df_to_plot.lat, zorder=1, alpha=0.5, c=vals, s=8, cmap="gist_rainbow")
plt.colorbar(sc)
ax.set_title('Plotting Spatial Data on Sockholm')
ax.set_xlim(BBox[0],BBox[1])
ax.set_ylim(BBox[2],BBox[3])
ax.imshow(sthlm_m, zorder=0, extent = BBox, aspect= 'equal')
# plt.show()

############ SOLNA SCATTERPLOT ##########
fig, ax = plt.subplots(figsize=(10,8))
sc = ax.scatter(df_to_plot_solna.lng, df_to_plot_solna.lat, zorder=1, alpha=0.8, c=vals_solna, s=10, cmap="gist_rainbow")
plt.colorbar(sc)
ax.set_title('Plotting Spatial Data on Solna')
ax.set_xlim(BBox_solna[0],BBox_solna[1])
ax.set_ylim(BBox_solna[2],BBox_solna[3])
ax.imshow(solna_m, zorder=0, extent = BBox_solna, aspect= 'equal')
# plt.show()

################################################################################
##################### HEAT MAPS AND HEXBIN PLOT ################################
################################################################################

# ADD FILTER AND CREATE PRICCE/KVM
df_plt = df_to_plot
df_plt_solna = df_to_plot_solna
df_plt_df_to_plot_liljeholmen = df_to_plot_liljeholmen
df_plt_resid = df_to_plot
df_plt_resid_ols = df_to_plot
df_plt_resid_svr = df_to_plot

# Limit the upper and lower range of price/kvm for better plot
df_plt.loc[df_plt["price/kvm"] < price_kvm_low_lim, "price/kvm"] = price_kvm_low_lim
df_plt.loc[df_plt["price/kvm"] > price_kvm_upp_lim, "price/kvm"] = price_kvm_upp_lim

df_plt_solna.loc[df_plt_solna["price/kvm"] < price_kvm_low_lim_solna, "price/kvm"] = price_kvm_low_lim_solna
df_plt_solna.loc[df_plt_solna["price/kvm"] > price_kvm_upp_lim_solna, "price/kvm"] = price_kvm_upp_lim_solna

df_plt_df_to_plot_liljeholmen.loc[df_plt_df_to_plot_liljeholmen["price/kvm"] < price_kvm_low_lim_solna, "price/kvm"] = price_kvm_low_lim_solna
df_plt_df_to_plot_liljeholmen.loc[df_plt_df_to_plot_liljeholmen["price/kvm"] > price_kvm_upp_lim_solna, "price/kvm"] = price_kvm_upp_lim_solna

df_plt_resid.loc[df_plt_resid["rnd_residual"] < residual_low_lim, "rnd_residual"] = residual_low_lim
df_plt_resid.loc[df_plt_resid["rnd_residual"] > residual_upp_lim, "rnd_residual"] = residual_upp_lim

df_plt_resid_ols.loc[df_plt_resid["ols_residual"] < residual_low_lim, "ols_residual"] = residual_low_lim
df_plt_resid_ols.loc[df_plt_resid["ols_residual"] > residual_upp_lim, "ols_residual"] = residual_upp_lim

df_plt_resid_svr.loc[df_plt_resid["svr_residual"] < residual_low_lim, "svr_residual"] = residual_low_lim
df_plt_resid_svr.loc[df_plt_resid["svr_residual"] > residual_upp_lim, "svr_residual"] = residual_upp_lim


############################### MEAN VALUE MATRIX ####################
# Create a n by n Numpy array with the mean price per coordiinate-bin
gridlines = n_gridlines
lng_gridspace = (df_plt.lng.max() - df_plt.lng.min()) / gridlines
lat_gridspace = (df_plt.lat.max() - df_plt.lat.min()) / gridlines
lng_array = np.arange(df_plt.lng.min(),df.lng.max() ,lng_gridspace)
lat_array = np.arange(df_plt.lat.min(),df_plt.lat.max() ,lat_gridspace)

mean_price_list = []
for i in reversed(lat_array):
    cord_list = []
    for j in lng_array:
        # next_lng_grid = i + lng_gridspace
        # next_lat_grid = i + lat_gridspace
        grd_mean_price = df_plt.loc[
                                            (df_plt["lng"] >= j) & 
                                            (df_plt["lng"] < (j + lng_gridspace)) &
                                            (df_plt["lat"] <= i) &
                                            (df_plt["lat"] > (i - lat_gridspace))
                                            ]["price/kvm"].mean()
        cord_list.append(grd_mean_price)
    mean_price_list.append(cord_list)
mean_matrix = np.array(mean_price_list)

ax = sns.heatmap(mean_matrix, linewidth=0.5, cmap="gist_rainbow")
# plt.show()



# Create a new colorscheme for hexbin so that the low values are transparant

########### REALISED STOCKHOLM PLOT ##########
# good colorschemed = gist_rainbow, PuBu, jet
fig, ax = plt.subplots(figsize=(12,8))
sc = plt.hexbin(df_plt.lng, df_plt.lat, cmap="jet", C=df_plt["price/kvm"], gridsize=200, alpha=0.5, zorder=1)
plt.colorbar()
ax.set_title('Plotting Spatial Data on Stockholm - Realized prices')
ax.set_xlim(BBox[0],BBox[1])
ax.set_ylim(BBox[2],BBox[3])
ax.imshow(sthlm_m, zorder=0, extent = BBox, aspect= 'equal')
plt.show()

########### REALISED SOLNA PLOT ##########
# fig, ax = plt.subplots(figsize=(12,8))
# sc = plt.hexbin(df_plt_solna.lng, df_plt_solna.lat, cmap="jet", C=df_plt_solna["price/kvm"], gridsize=100, alpha=0.7, zorder=1)
# plt.colorbar()
# ax.set_title('Plotting Spatial Data on Solna')
# ax.set_xlim(BBox_solna[0],BBox_solna[1])
# ax.set_ylim(BBox_solna[2],BBox_solna[3])
# ax.imshow(solna_m, zorder=0, extent = BBox_solna, aspect= 'equal')
# plt.show()

########### REALISED LILJEHOLMEN PLOT ##########
# fig, ax = plt.subplots(figsize=(12,8))
# sc = plt.hexbin(df_plt_df_to_plot_liljeholmen.lng, df_plt_df_to_plot_liljeholmen.lat, cmap="jet", C=df_plt_df_to_plot_liljeholmen["price/kvm"], gridsize=100, alpha=0.7, zorder=1)
# plt.colorbar()
# ax.set_title('Plotting Spatial Data on Liljeholmen')
# ax.set_xlim(BBox_liljeholmen[0],BBox_liljeholmen[1])
# ax.set_ylim(BBox_liljeholmen[2],BBox_liljeholmen[3])
# ax.imshow(liljeholmen_m, zorder=0, extent = BBox_liljeholmen, aspect= 'equal')
# plt.show()

########### STOCKHOLM PLOT OF RANDOM FOREST PREDICTIONS ##########
fig, ax = plt.subplots(figsize=(12,8))
sc = plt.hexbin(df_plt.lng, df_plt.lat, cmap="jet", C=df_plt["rand_forest_prediction"], gridsize=200, alpha=0.5, zorder=1)
plt.colorbar()
ax.set_title('Plotting RF Predicted Price on Stockholm')
ax.set_xlim(BBox[0],BBox[1])
ax.set_ylim(BBox[2],BBox[3])
ax.imshow(sthlm_m, zorder=0, extent = BBox, aspect= 'equal')
plt.show()

# ERRORS 
fig, ax = plt.subplots(figsize=(12,8))
sc = plt.hexbin(df_plt_resid.lng, df_plt_resid.lat, cmap="seismic", C=df_plt_resid["rnd_residual"], gridsize=50, alpha=0.5, zorder=1)
plt.colorbar()
ax.set_title('Plotting RF Error on Stockholm')
ax.set_xlim(BBox[0],BBox[1])
ax.set_ylim(BBox[2],BBox[3])
ax.imshow(sthlm_m, zorder=0, extent = BBox, aspect= 'equal')
plt.show()

########### STOCKHOLM PLOT OF OLS PREDICTIONS ##########
fig, ax = plt.subplots(figsize=(12,8))
sc = plt.hexbin(df_plt.lng, df_plt.lat, cmap="jet", C=df_plt["ols_prediction"], gridsize=200, alpha=0.5, zorder=1)
plt.colorbar()
ax.set_title('Plotting OLS Predicted Price on Stockholm')
ax.set_xlim(BBox[0],BBox[1])
ax.set_ylim(BBox[2],BBox[3])
ax.imshow(sthlm_m, zorder=0, extent = BBox, aspect= 'equal')
plt.show()

# ERRORS 
fig, ax = plt.subplots(figsize=(12,8))
sc = plt.hexbin(df_plt_resid_ols.lng, df_plt_resid_ols.lat, cmap="seismic", C=df_plt_resid_ols["ols_residual"], gridsize=50, alpha=0.5, zorder=1)
plt.colorbar()
ax.set_title('Plotting OLS Error on Stockholm')
ax.set_xlim(BBox[0],BBox[1])
ax.set_ylim(BBox[2],BBox[3])
ax.imshow(sthlm_m, zorder=0, extent = BBox, aspect= 'equal')
plt.show()


########### STOCKHOLM PLOT OF SUPPORT VECTOR MACHINES PREDICTIONS ##########
fig, ax = plt.subplots(figsize=(12,8))
sc = plt.hexbin(df_plt.lng, df_plt.lat, cmap="jet", C=df_plt["svr_prediction"], gridsize=200, alpha=0.5, zorder=1)
plt.colorbar()
ax.set_title('Plotting SVR Predicted Price on Stockholm')
ax.set_xlim(BBox[0],BBox[1])
ax.set_ylim(BBox[2],BBox[3])
ax.imshow(sthlm_m, zorder=0, extent = BBox, aspect= 'equal')
plt.show()

# ERRORS 
fig, ax = plt.subplots(figsize=(12,8))
sc = plt.hexbin(df_plt_resid_svr.lng, df_plt_resid_svr.lat, cmap="seismic", C=df_plt_resid_svr["svr_residual"], gridsize=50, alpha=0.5, zorder=1)
plt.colorbar()
ax.set_title('Plotting SVR Error on Stockholm')
ax.set_xlim(BBox[0],BBox[1])
ax.set_ylim(BBox[2],BBox[3])
ax.imshow(sthlm_m, zorder=0, extent = BBox, aspect= 'equal')
plt.show()