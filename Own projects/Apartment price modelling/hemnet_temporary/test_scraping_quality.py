
# Investigate if the number of missing values 

# Also combine and export batch runs


import pandas as pd
df = pd.DataFrame()

for i in range(5):
    df_tmp =  pd.read_csv("D:\\Datasets\\Hemnet\\0_scraped_data\\all_links_20_21_batch_{}_data.csv".format(i))
    df = df.append(df_tmp)

df.reset_index()

df.describe()
print("Number of missing prices:", len(df.loc[df["prices"] == "Missing"]))

df.to_csv("D:\\Datasets\\Hemnet\\0_scraped_data\\all_20_21_data.csv", index=False)