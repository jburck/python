# Text parsing and format corrections


from numpy import NaN
import pandas as pd
import re
from datetime import datetime
import matplotlib.pyplot as plt

import os
print(os.getcwd())

# Run batches y/n - affects the output file naming
batch_run = 'n'

in_path =  "D:\\Datasets\\Hemnet\\0_scraped_data\\"
out_path = "D:\\Datasets\\Hemnet\\1_data_prep"

infile =            "all_20_21_data.csv"
outfile =           "all_20_21_data_parsed.csv"
outfile_reduced =   "all_20_21_data_red_parsed.csv"
sizes, kvm, rum, fee, floor, brokers_person,locations_full, streets2, floors, prices, ask_prices, dates, years, months = [[] for i in range(14)]
df = pd.read_csv(os.path.join(in_path, infile))

# Drop missing prices (about 0,4%)
print("Missing prices:", len(df.loc[df["prices"] == "Missing"]))
df = df[df["prices"] != "Missing"]
print("Missing prices:", len(df.loc[df["prices"] == "Missing"]))

# df = df.loc[df["Våning"].str.contains("1,5")][:10]
df.reset_index(drop=True, inplace=True)

###########################
# LOCATION
###########################

locations_full =   df["locations"].copy()
for i in range(len(locations_full)):
    try:
        locations_full[i] = locations_full[i].split('-')[1]
    except:
        locations_full[i] = "Missing"
df["locations_parsed"] = locations_full

###########################
# STREET
###########################

streets2 =   df["streets"].copy()
for i in range(len(streets2)):
    
    try:
        streets2[i] = streets2[i].split('\n')[1]
        streets2[i] = streets2[i].split(',')[0]
    except:
        streets2[i] = "Missing"
df["streets_parsed"] = streets2

# df["street2"] =          [i.split('\n')[1] for i in df["streets"]]

###########################
# FLOOR
###########################

floors =   df["Våning"].copy()

floors.loc[floors == "Mi"] = "-1"     
floors.loc[floors == "Missing"] = "-1"

for i in range(len(floors)): 
    try:
        floors[i] = floors[i].split("av")[0].strip()
    except:
        floors[i] = "-1"

for i in range(len(floors)):
    try:
        floors[i] = floors[i].split(",")[0]
    except:
        pass

for i in range(len(floors)): 
    try:
        floors[i] = floors[i].split("av")[0].strip()
    except:
        pass

df["våning_parsed"] = floors.apply(lambda x: int(x))

###########################
# PRICE
###########################


prices =   df["prices"].copy()
for i in range(len(prices)):
    try:
        prices[i] = prices[i].replace(" ", "")
    except:
        prices[i] = -1
    try:
        prices[i] = int(prices[i].replace("kr", ""))
    except:
        prices[i] = -1


# First loop not effective on everything
for i in range(len(prices)):
    try:
        prices[i] = prices[i].replace(" ", "")
    except:
        pass
    try:
        prices[i] = int(prices[i].replace("kr", ""))
    except:
        pass

df["prices_parsed"] = prices.apply(lambda x: int(x))

ask_prices =   df["ask_prices"].copy()
for i in range(len(ask_prices)):

    try:
        ask_prices[i] = ask_prices[i].replace(" ", "")
    except:
        prices[i] = -1
    try:
        ask_prices[i] = int(ask_prices[i].replace("kr", ""))
    except:
        ask_prices[i] = -1

for i in range(len(ask_prices)):
    # First loop not effective on everything
    try:
        ask_prices[i] = ask_prices[i].replace(" ", "")
    except:
        pass
    try:
        ask_prices[i] = int(ask_prices[i].replace("kr", ""))
    except:
        pass


df["ask_prices_parsed"] = ask_prices.apply(lambda x: int(x))

###########################
# DATE
###########################

dates = df["locations"].copy()

for i in range(len(dates)):
    try:
        dates[i] = dates[i].split('Såld den', 2)[1].replace(" ", "")
    except:
        dates[i] = "01January1900"
# First loop not effective on everything
for i in range(len(dates)):
    try:
        dates[i] = dates[i].split('Såld den', 2)[1].replace(" ", "")
    except:
        pass

dates = [i.replace("januari", "January")    for i in dates]
dates = [i.replace("februari", "February")  for i in dates]
dates = [i.replace("mars", "March")         for i in dates]
dates = [i.replace("maj", "May")            for i in dates]
dates = [i.replace("juni", "June")          for i in dates]
dates = [i.replace("juli", "July")          for i in dates]
dates = [i.replace("augusti", "August")     for i in dates]
dates = [i.replace("oktober", "October")    for i in dates]
dates = [datetime.strptime(i, '%d%B%Y')   for i in dates]

df["date"] =    dates
df["years"] =   [i.year for i in df["date"]]
df["months"] =  [i.month for i in df["date"]]

###########################
# CONSTRUCTION YEAR
###########################

const_year = df["Byggår"].copy()

for i in range(len(const_year)):
    if const_year[i] == "Missing":
        const_year[i] = -1
    else:
        const_year[i] = const_year[i][:4] # Some construction years has the format "2014 - 2015"
df["construct_year"] = const_year.apply(lambda x: int(x))

###########################
# KVM
###########################
kvm = df["Boarea"].copy()

kvm = [i.replace(" m²", "") for i in kvm]
kvm = [i.replace(",", ".") for i in kvm]

for i in range(len(kvm)):
    if kvm[i] == "Missing":
        kvm[i] = -1

df["kvm"] = kvm
df["kvm"] = pd.to_numeric(df["kvm"], downcast="float")

###########################
# RUM
###########################
rooms = df["Antal rum"].copy()

for i in range(len(rooms)):
    if rooms[i] == "Missing":
        rooms[i] = -1.0
    else:
        try:
            rooms[i] = rooms[i].replace(" rum", "")
            rooms[i] = rooms[i].replace(",", ".")
        except:
            rooms[i] = -1.0

df["rooms"] = rooms.apply(lambda x: float(x))

###########################
# BALCONY / PATIO
###########################
balconies = df["Balkong"].copy()

for i in range(len(balconies)):
    if balconies[i] == "Ja":
        balconies[i] = 1
    elif balconies[i] == "Nej":
        balconies[i] = 0
    else:
        balconies[i] = -1.0
df["balcony"] = balconies.apply(lambda x: int(x))

patio = df["Uteplats"].copy()
for i in range(len(patio)):
    if patio[i] == "Ja":
        patio[i] = 1
    elif patio[i] == "Nej":
        patio[i] = 0
    else:
        patio[i] = -1.0
df["patio"] = patio.apply(lambda x: int(x))


###########################
# BROKER
###########################
brokers = df["brokers"].copy()

for i in range(len(brokers)):
    try:
        brokers[i] = (brokers[i].split("\n")[2])
    except:
        brokers[i] = "Missing"

df["brokers2"] = brokers

###########################
# FEE
###########################
fees = df["Avgift/månad"].copy()
for i in range(len(fees)):
    if fees[i] == "Missing":
        fees[i] = -1.0
    try:
        pos = fees[i].find(" kr/mån")
        fees[i] = fees[i][:pos].replace(" ", "")
    except:
        fees[i] = -1.0
df["fees"] = fees.apply(lambda x: int(x))

costs = df["Driftskostnad"].copy()

for i in range(len(costs)):
    if costs[i] == "Missing":
        costs[i] = -1.0
    else:
        try:
            pos = costs[i].find(" kr/år")
            costs[i] = costs[i][:pos].replace(" ", "")
        except:
            costs[i] = -1.0
df["costs"] = costs.apply(lambda x: int(x))

# Shuffle data and create batches for geocoding
df = df.sample(frac=1).reset_index(drop=True)

print(df.columns)

# Export to csv
print("Done parsing!")


df_reduced = df[["links", "locations_parsed", "streets_parsed", 
    "våning_parsed", "prices_parsed", "ask_prices_parsed", 
    "date", "years", "months",
    "kvm", "rooms", "balcony",
    "patio", "brokers2", "fees",
    "costs", "construct_year"]]

df.to_csv(os.path.join(out_path, outfile), index = False)
df_reduced.to_csv(os.path.join(out_path,outfile_reduced), index=False)
print("Export done")
