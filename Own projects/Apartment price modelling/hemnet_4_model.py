import pandas as pd
import numpy as np
import sklearn
from sklearn.dummy import DummyRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import ElasticNet, LinearRegression
from sklearn.svm import SVR
from sklearn.neighbors import KNeighborsRegressor
from sklearn.model_selection import cross_val_score, train_test_split, GridSearchCV
from sklearn.metrics import mean_absolute_error
from sklearn import tree
import matplotlib.pyplot as plt
import pickle
from pickle import dump, load
import warnings
import time
warnings.filterwarnings("ignore")

tic = time.perf_counter()
random_state = 42
sample_size = 0.5

df = pd.read_csv("D:\\Datasets\\Hemnet\\data_prep\\hemnet_complete_prepped.csv")


''' 
SELECT SAMPLE BASED ON AREA

Stockholms kommun
Solna kommun
Sundbybergs kommun
Bromma
Järfälla kommun
Sollentuna kommun
Hässelby-Vällingby
Kista-Rinkeby
Upplands Väsby kommun
'''


# Reshuffle the data and select samlpe size
df = df.sample(frac=sample_size).reset_index(drop=True)

label = "price"
features = ['kvm', 'rum', 'fee', 'floor', 'lat', 'lng', 'year', 'month']
k_fold = 3

# ELASTIC NET SETTINGS
elastic_alpha_n = 100
elastic_l1_ratio_n = 10
# If number of l1_ratio iterations are changed then the number of lists below needs to match
l1,l2,l3,l4,l5,l6,l7,l8,l9,l10 = [[] for i in range(10)]
elastic_lists = [l1,l2,l3,l4,l5,l6,l7,l8,l9,l10]

# SUPPORT VECTOR MACHINE SETTINGS
param_grid_svr = {
                "kernel": ['rbf', 'poly'],
                "degree": [3,4],
                'coef0' : [1],
                "C": [0.1, 1],
                "gamma": ['scale', 'auto']
                }

# RANDOM FOREST SETTINGS
# param_grid_rnd = {
#                 "max_depth": [8,16,24,48,96,192,384], 
#                 "n_estimators": [24,48,96,192,384,768], 
#                 "max_features": [0.25, 0.5 ,0.75]}

param_grid_rnd = {
                "max_depth": [1,10,100,200,300,400,500,600], 
                "n_estimators": [1,10,100,200,300,400,500,600], 
                "max_features": [0.25, 0.5 ,0.75]}

# K-NEAREST NEIGHBOURS SETTINGS
param_grid_knear = {
                "n_neighbors": [3,5,7,9,11], 
                "metric": ['minkowski', 'euclidean', 'manhattan'], 
                "weights": ['uniform', 'distance']}


# Create train and test datasets
# X = df.drop(label, axis=1)
# y = df[label]
# X_train_poly, X_test_poly, y_train, y_test = train_test_split( X, y, test_size=0.1, random_state=random_state)

# Trainingdata without second power and cross products
X_train = X_train_poly[features]
X_test = X_test_poly[features]

####################################################################################
# Calculate MAE for benchmark model / dummy regressor based on a simple average of price
###################################################################################
reg_dummy = DummyRegressor(strategy="mean")
reg_dummy_score = cross_val_score(reg_dummy, X_train, y=y_train,scoring="neg_mean_absolute_error", cv=k_fold) *-1
print("\nMean k-fold cross validation MAE of dummy regressor: ", round(reg_dummy_score.mean(), -3), ", stddev:", round(reg_dummy_score.std()))

####################################################################################
# Train Simple OLS benchmark model
###################################################################################
X_train_ols = pd.DataFrame(X_train_poly[["rum", "lat", "lng", "lat lng"]])
reg_ols = LinearRegression()
reg_ols_score = cross_val_score(reg_ols, X_train_ols, y_train, cv=k_fold,scoring="neg_mean_absolute_error") * -1
print("\nMean k-fold cross validation MAE of OLS regressor: ", round(reg_ols_score.mean(), -3), ", stddev:", round(reg_ols_score.std()))

# The cross val score procedure does not calibrate the regression insance.
reg_ols.fit(X_train_ols, y_train)
pickle.dump(reg_ols, open("D:\\Python\\Own projects\\Hemnet\\stored_models\\reg_ols.pkl", 'wb'))

####################################################################################
# Elastic net - learning curve optimization
###################################################################################
alpha_list, l1_ratio_list, elastic_score_list = [[] for i in range(3)]
reg_elastic_score_dict = dict()

for i in range(1,elastic_alpha_n):
    for j in range(1,elastic_l1_ratio_n):
        alpha = i / elastic_alpha_n
        l1_ratio = j / elastic_l1_ratio_n
        reg_elastic = ElasticNet(alpha=alpha, l1_ratio=l1_ratio)
        elastic_score = cross_val_score(reg_elastic, X_train_poly, y_train, cv=k_fold, scoring="neg_mean_absolute_error").mean() *-1
        reg_elastic_score_dict[alpha,l1_ratio] = elastic_score

elastic_mae = 0
for i in reg_elastic_score_dict:
    if elastic_mae == 0:
        elastic_mae = reg_elastic_score_dict[i]
    elif reg_elastic_score_dict[i] < elastic_mae:
        elastic_mae = reg_elastic_score_dict[i]
        elastic_key = i

print("\nElastic net best parameters\nalpha: ", elastic_key[0], "\nl1_ratio:", elastic_key[1],"\nMAE: ", elastic_mae)
    
alpha_final = elastic_key[0]
nl1_ratio_final = elastic_key[1]

for i in range(1,elastic_alpha_n):
    alpha = i / elastic_alpha_n
    for j in range(1,elastic_l1_ratio_n):
        l1_ratio = j / elastic_l1_ratio_n
        elastic_lists[j-1].append(reg_elastic_score_dict[(alpha, l1_ratio)])
    # l1.append(reg_elastic_score_dict[(alpha, 0.1)])

for i in range(1,elastic_l1_ratio_n):
    l1_ratio = i / elastic_l1_ratio_n
    plt.plot(elastic_lists[i-1], label="l1_ratio = {}".format(l1_ratio))
    plt.legend(loc=4)
# plt.show()

reg_elastic = ElasticNet(alpha=alpha_final, l1_ratio=nl1_ratio_final)
reg_elastic_score = cross_val_score(reg_elastic, X_train_poly, y_train, cv=k_fold, scoring="neg_mean_absolute_error") *-1
print("\nMean k-fold cross validation MAE of Elassic net regressor: ", round(reg_elastic_score.mean(), -3), ", stddev:", round(reg_elastic_score.std()))
reg_elastic.fit(X_train_poly, y_train)
pickle.dump(reg_elastic, open("D:\\Python\\Own projects\\Hemnet\\stored_models\\reg_elastic.pkl", 'wb')) 

####################################################################################
# Support Vector Machine Regression
###################################################################################
reg_svr = SVR()

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()

y_train_reshaped = y_train.values.reshape(-1, 1)
y_train_scaled = scaler.fit_transform(y_train_reshaped)
dump(scaler, open('D:\\Python\\Own projects\\Hemnet\\scaler_y.pkl', 'wb'))

grid_search_svr = GridSearchCV(reg_svr, param_grid_svr, scoring="r2", cv=k_fold, verbose=10)
grid_search_svr.fit(X_train, y_train_scaled)

print("\nBest SVR parameters:", grid_search_svr.best_params_)
svr_cvres = grid_search_svr.cv_results_
for i,j in zip(svr_cvres['params'], svr_cvres['mean_test_score']):
    print("Support Vector Machine Cross Validation Results:", j, i)

reg_svr_grid = SVR(kernel=grid_search_svr.best_params_['kernel'],
                    # degree=grid_search_svr.best_params_['degree'],
                    # coef0=grid_search_svr.best_params_['coef0'],
                    C=grid_search_svr.best_params_['C'],
                    gamma=grid_search_svr.best_params_['gamma']
                    )

reg_svr_score = cross_val_score(reg_svr_grid, X_train, y_train_scaled, cv=k_fold, scoring="r2")
print("\nMean k-fold cross validation R^2 of SVR: ", round(reg_svr_score.mean(), 3))


reg_svr_grid.fit(X_train, y_train_scaled)
pickle.dump(reg_svr_grid, open("D:\\Python\\Own projects\\Hemnet\\stored_models\\reg_svr.pkl", 'wb'))

####################################################################################
# K-nearest Neighbors - seems to be performing better on a subset of the explanatory variables
###################################################################################

X_train_knear = X_train[["rum", "kvm", "lat", "lng", "year"]]

reg_knear = KNeighborsRegressor()
grid_search_knear = GridSearchCV(reg_knear, param_grid_knear, scoring='r2', cv=k_fold)
grid_search_knear.fit(X_train_knear, y_train)
print("\nBest K-Nearest Neighbor parameters:", grid_search_knear.best_params_)

reg_knear_grid = KNeighborsRegressor(n_neighbors=grid_search_knear.best_params_['n_neighbors'],
    metric=grid_search_knear.best_params_['metric'],
    weights=grid_search_knear.best_params_['weights']
    )

reg_knear_score = cross_val_score(reg_knear_grid, X_train_knear, y_train, cv=k_fold, scoring="neg_mean_absolute_error") *-1
print("\nMean k-fold cross validation MAE of K-Nearest Neighbor: ", round(reg_knear_score.mean(), -3), ", stddev:", round(reg_knear_score.std()))

reg_knear_grid.fit(X_train_knear, y_train)
pickle.dump(reg_knear_grid, open("D:\\Python\\Own projects\\Hemnet\\stored_models\\reg_knear.pkl", 'wb'))

####################################################################################
# Random Forest
###################################################################################
reg_rnd = RandomForestRegressor()
reg_rnd_score = cross_val_score(reg_rnd, X_train, y_train, scoring="neg_mean_absolute_error") *-1 
print("\nMean k-fold cross validation MAE of Random Forest regression: ", round(reg_rnd_score.mean(), -3), ", stddev:", round(reg_rnd_score.std()))

grid_search_rnd = GridSearchCV(reg_rnd, param_grid_rnd, cv=k_fold, scoring="r2", verbose=2)
grid_search_rnd.fit(X_train,y_train)

print("\nBest Random Forest parameters:", grid_search_rnd.best_params_)

reg_rnd_grid = RandomForestRegressor(max_depth=grid_search_rnd.best_params_["max_depth"], \
                                    n_estimators=grid_search_rnd.best_params_["n_estimators"], \
                                    max_features=grid_search_rnd.best_params_["max_features"])

reg_rnd_grid_score = cross_val_score(reg_rnd_grid, X_train, y_train, scoring="neg_mean_absolute_error") *-1 
print("\nMean k-fold cross validation MAE of Random Forest regression - optimized parameters: ", round(reg_rnd_grid_score.mean(), -3), ", stddev:", round(reg_rnd_grid_score.std()))

reg_rnd_grid.fit(X_train, y_train)
pickle.dump(reg_rnd_grid, open("D:\\Python\\Own projects\\Hemnet\\stored_models\\reg_rnd.pkl", 'wb'))

print("\nImportance of features:\n")
for i, j in zip(reg_rnd_grid.feature_importances_, X_train.columns):
    print(j, round(i, 2))


toc = time.perf_counter()
print("\nCode executed in {} seconds".format(toc - tic))

print("\nBest K-Nearest Neighbor parameters:", grid_search_knear.best_params_)
print("\nBest SVR parameters:", grid_search_svr.best_params_)
print("\nBest Random Forest parameters:", grid_search_rnd.best_params_)



####################################################################################
# Validation on test set 
###################################################################################

# plot function
def plot_residuals(x,y, model):
    plt.title(model + " residuals vs realized prices")
    plt.scatter(x,y, alpha=0.1)
    plt.ylabel("Residual")
    plt.ylim(top=4000000)
    plt.ylim(bottom=-4000000)
    plt.xlim(right=10000000, left=100000)
    plt.minorticks_on()
    plt.grid()
    plt.xlabel("Realized")
    plt.ticklabel_format(style='plain')
    plt.show()


# Simple OLS regression
print("\nOLS benchmark model R^2 on calibration:", round(reg_ols.score(X_train_ols, y_train), 2))
pred_ols = reg_ols.predict(X_train_ols)
resid_ols = pred_ols - y_train
plot_residuals(y_train,resid_ols, "OLS")

# Elastic net
print("\nElastic net R^2 on calibration:", round(reg_elastic.score(X_train_poly, y_train), 2))
pred_elastic = reg_elastic.predict(X_train_poly)
resid_elastic = pred_elastic - y_train
plot_residuals(y_train,resid_elastic, "Elastic net")

# SVR
print("\nSVR R^2 on calibration:", round(reg_svr_grid.score(X_train, y_train_scaled), 2))
pred_svr = reg_svr_grid.predict(X_train)
pred_svr = scaler.inverse_transform(pred_svr)
resid_svr = pred_svr - y_train
plot_residuals(y_train, resid_svr, "SVR")

# Random Forest
print("\nRandom Forest R^2 on calibration:", round(reg_rnd_grid.score(X_train, y_train), 2))
pred_rnd = reg_rnd_grid.predict(X_train)
resid_rnd = pred_rnd - y_train
plot_residuals(y_train, resid_rnd, "Random Forest")

# boxplot for the residual per realized bin
y_train_mil = pd.DataFrame(round(y_train, -6))
y_train_mil["residual"] = resid_rnd
y_train_mil.loc[y_train_mil.price < 10000000].boxplot(column="residual", by="price")
plt.show()


####################################################################################
# CHECK PERFORMANCE ON TEST SET 
###################################################################################
# Get candidate model

