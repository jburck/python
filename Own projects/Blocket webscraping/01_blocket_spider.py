

'''
Done:
2022-08-06: Create datetime column and replace in export
'''

# IMPORT
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time
import csv
import pandas as pd 

# CONFIGURATION
path            = "C:/chromedriver.exe"
blocket_path    = "https://www.blocket.se/annonser/hela_sverige/fordon/batar/segelbat?cg=1062"
csv_path        = "C:\\Users\\jbuep\\My Drive\\Data\\sailboats\\"
db_name         = "boat_database_raw.csv"
import_date = "{}-{}-{}".format(str(time.localtime().tm_mday) , \
                                str(time.localtime().tm_mon) ,  \
                                str(time.localtime().tm_year))

# FUNCTIONS
# Add to existing database -  for csv exported by windows add: delimiter=";",encoding='Windows-1252'
def remove_dups_and_append_db(df):
    df_db = pd.read_csv(csv_path + db_name)
    df_db_conc = pd.concat([df_db, df], axis=0, ignore_index=True).drop_duplicates().reset_index(drop=True)
    print(f"Removing {len(df_db)-len(df_db_conc)} duplicates.")
    df_db_conc.to_csv(csv_path + db_name, index=False)
    print("Database updated".center(30))

# INITIALIZATION
my_links = []
my_dict =  {"Link":[], "ImportDate": [], "Title": [], "Place": [], "Time": [], "Price": [], \
            "OriginalPrice": [], "Length": [], "Description": []}
print("Enter how many pages to scrape:")
n_pages         = int(input()) # number of result pages to fetch data from

# RUN SPIDER
driver = webdriver.Chrome()
driver.get(blocket_path)
driver.implicitly_wait(3)

iframe = driver.find_element(By.XPATH, "//*[@title='SP Consent Message']")
driver.switch_to.frame(iframe)
driver.find_element(By.XPATH, "//*[@title='Godkänn alla cookies']").click()

for i in range(n_pages):

    results = driver.find_element(By.CSS_SELECTOR, "#__next > div.LayoutStyles__FullHeightWrapper-sc-du5f0h-0.MediumLayoutstyled__Container-sc-1u6scr-0.esWQar.hLdiFL > main > div.MediumLayoutstyled__BodyWrapper-sc-1u6scr-3.fTgaPw > div.MediumLayoutstyled__BodyLeft-sc-1u6scr-4.iNCQsz > div:nth-child(3)")
    col_a = results.find_elements(By.TAG_NAME,"A")
    #Extract all the links in the a column to get individual ad links

    for href in col_a:
        driver.execute_script("arguments[0].scrollIntoView(true);", href)
        time.sleep(0.1)
        link = href.get_attribute('href')
        my_links.append(link)
    
    driver.get(blocket_path + "&page=" + str(i+2))
    print("Number of links: {}".format(str(len(my_links))))
    time.sleep(1)

driver.get(my_links[1])
for i in range(len(my_links)):
    if my_links[i].find("annons/") >= 0:
        try:
            driver.get(my_links[i])
        except:
            continue
        time.sleep(0.25)
        try:
            Title = str(driver.find_element \
                (By.XPATH, "//*[@id='skip-tabbar']/div[2]/div[2]/div[1]/div/article/div[1]/div[2]/h1").text)
        except:
            Title = "Not found"
            print(f"Warning: Title not found for object: {my_links[i]}")
        try:
            Time = driver.find_element(By.XPATH,"//*[@id='skip-tabbar']/div[2]/div[2]/div[1]/div/article/div[1]/div[2]/div[1]/div[1]/div[1]/div/span").text
        except:
            Time = "Not found"
        try:
            Place = driver.find_element(By.XPATH,"//*[@id='skip-tabbar']/div[2]/div[2]/div[1]/div/article/div[1]/div[2]/div[1]/div[1]/div[2]/div/span/a").text
        except:
            Place = "Not found"
            print(f"Warning: Place not found for object: {my_links[i]}")
        try:
            Price =  driver.find_element(By.XPATH,"//*[@id='skip-tabbar']/div[2]/div[2]/div[1]/div/article/div[1]/div[2]/div[2]/div").text       
        except:
            Price = "Not found"
            print(f"Warning: Price not found for object: {my_links[i]}")
        try:
            Price_org =  driver.find_element(By.XPATH,"//*[@id='skip-tabbar']/div[1]/div[2]/div[1]/div/article/div[1]/div[2]/div[2]/div[2]").text
        except:
            Price_org = "Not found"
        try:
            Length = driver.find_element(By.XPATH,"//*[@id='skip-tabbar']/div[2]/div[2]/div[1]/div/article/div[2]/div[2]/div[2]/div/div/div/div/div[2]").text
        except:
            Length = "Not found"
            print(f"Warning: Length not found for object: {my_links[i]}")
        try:
            Description = driver.find_element(By.XPATH,"//*[@id='skip-tabbar']/div[2]/div[2]/div[1]/div/article/div[2]/div[2]/div[4]/div[2]/div").text
        except:
            Description = "Not found"
        my_dict["Link"].append(my_links[i])
        my_dict["ImportDate"].append(import_date)
        my_dict["Title"].append(Title)
        my_dict["Place"].append(Place)
        my_dict["Time"].append(Time)
        my_dict["Price"].append(Price)
        my_dict["OriginalPrice"].append(Price_org)
        my_dict["Length"].append(Length)
        my_dict["Description"].append(Description)
        print(Title)

df = pd.DataFrame(my_dict)
df["Description"] = df["Description"].str.replace(",", " ")
df["Description"] = df["Description"].str.replace("\n", " ")
df["Length"] = df["Length"].str.replace(",", ".")

filename = "\\scraping_{}_{}_{}.csv".format(str(time.localtime().tm_mday) ,    \
                                                str(time.localtime().tm_mon) , \
                                                str(time.localtime().tm_year))
df.to_csv(csv_path + "\\scrapings" + filename, index=False)


print(f'{len(my_dict["Link"])} observations were scaped.')

remove_dups_and_append_db(df)
print("Script 01_blocket_spider.py is done!".center(30))
quit()
