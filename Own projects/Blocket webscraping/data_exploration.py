import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import chi2_contingency


# SETTINGS
plot = True

csv_path = "C:/Users/jbuep/My Drive/Data/sailboats/cleaned_data.csv"
df = pd.read_csv(csv_path)
n_clusters = 8

# Create boxplot chart for price by group for boats with x obs or more that is ranked on median
if plot:
    b = df["Boat"].value_counts()
    b_index = b[b >= 20].index
    df.sort_values("Length_median", inplace=True)
    sns.boxplot(x='Price', y='Boat', data=df.where(df["Boat"].isin(b_index)), orient='h')
    plt.axvline(x=100000)
    plt.show() 

# Segmentering av data - dimensioner: Pris, längd

from difflib import SequenceMatcher
import numpy as np


b = df["Boat"].value_counts()
b_index = b[b >= 3].index
df_reduced = df[df['Boat'].notnull()].copy()
df_reduced = df_reduced[df_reduced['Boat'].isin(b_index.values)]

df_aggregated = df_reduced.groupby('Boat').median()[['Price', 'Length']]
plt.scatter(y=df_aggregated['Price'], x=df_aggregated['Length'])
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import KMeans
from sklearn import preprocessing
from matplotlib import pyplot
from numpy import unique
from numpy import where


standard = preprocessing.minmax_scale(df_aggregated, feature_range=(0,1))
df_standard = pd.DataFrame(standard, index=df_aggregated.index, columns=df_aggregated.columns)
df_standard.Price = np.log(df_standard.Price)
df_standard.Price = df_standard.Price.replace([np.inf, -np.inf], -5)
df_standard.Price = df_standard.Price / 5
df_standard.loc[df_standard.Length < 0.3, 'Length'] = 0 # Small boats are their own category
df_standard.loc[df_standard.Length < 0.3, 'Price'] = -1 # Small boats are their own category
df_standard.loc[df_standard.Price > (df_standard.Price.mean()+df_standard.Price.std()), 'Price'] = 0.1 # Expensive boats are their own category
df_standard.loc[df_standard.Price > (df_standard.Price.mean()+df_standard.Price.std()), 'Length'] = 1.2 # Expensive boats are their own category

standard = np.array(df_standard)


# define the model
#model = AgglomerativeClustering(n_clusters=n_clusters)
#yhat = model.fit_predict(standard)

model =  KMeans(n_clusters=n_clusters, random_state=42).fit(standard)
yhat = model.labels_

# fit model and predict clusters

# retrieve unique clusters
clusters = unique(yhat)
# create scatter plot for samples from each cluster
for cluster in clusters:
	# get row indexes for samples with this cluster
	row_ix = where(yhat == cluster)
	# create scatter of these samples
	pyplot.scatter(standard[row_ix, 0], standard[row_ix, 1])
# show the plot
pyplot.show()

df_aggregated['Agglomerative_cluster_category'] = yhat

for cluster in clusters:
	# get row indexes for samples with this cluster
	row_ix = where(df_aggregated['Agglomerative_cluster_category'] == cluster)
	# create scatter of these samples
	pyplot.scatter(df_aggregated.iloc[row_ix]['Price'], df_aggregated.iloc[row_ix]['Length'])
# show the plot
pyplot.show()

plt.scatter(df_aggregated.Price, df_aggregated.Length)
df_enriched = df.merge(df_aggregated, how='inner', right_index=True, left_on='Boat')
df_enriched.to_clipboard()

df_to_plot = df[['Price', 'DateTime']]

df_to_plot['year'] = pd.to_datetime(df_to_plot.DateTime).dt.year
df_to_plot['month'] = pd.to_datetime(df_to_plot.DateTime).dt.month
piv = pd.pivot(df_to_plot, index='month', columns='year', values=['Price'])