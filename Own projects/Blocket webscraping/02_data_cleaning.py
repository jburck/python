import pandas as pd
import numpy as np
import regex as r
import matplotlib.pyplot as plt
import time, datetime
import os

# Import scraped data
csv_path = "C:/Users/jbuep/My Drive/Data/sailboats"
csv_name = "boat_database_raw.csv"
import_path = os.path.join(csv_path, csv_name)
df_all = pd.read_csv(import_path)

# Identify and remove duplicates and convert to datetime
df_all.sort_values(by=["ImportDate"], inplace=True)
df_all["DateTime"] = [datetime.datetime.strptime(x, "%d-%m-%Y") for x in df_all.ImportDate]
duplicates = df_all.loc[df_all.duplicated("Link", keep='first')].index.values
print("Number of duplicaes removed: {}".format(str(len(duplicates))))
df = df_all.drop(duplicates)


#Remove price, place and boat name rows without results
removed_obs = df.loc[(df["Price"] == "Not found") | (df["Title"] == "Not found") | (df["Place"] == \
                     "Not found")].index.values
df.drop(removed_obs, axis=0, inplace=True)
#Remove share and rent observations
df = df.loc[df["Title"].str.contains("(?i)Andel |Andelsbåt|Uthyrning|uthyres|köpes") == False]

# Remove (hitta.se) from Place variable
df["Place"] = df["Place"].str.replace(r" \(.*\)","")

# Remove kr and space from price
df["Price"] = df["Price"].str.replace(" kr", "")
df["Price"] = df["Price"].str.replace(" ", "")
df["Price"] = pd.to_numeric(df["Price"])

# Remove not found lengths
df_nolength = df.loc[df["Length"] == "Not found"]
df.drop(df.loc[df["Length"] == "Not found"].index.values, axis=0, inplace=True)

# Remove fot and space from Length
df["Length"] = df["Length"].str.replace(" fot", "")
df["Length"] = df["Length"].str.replace(" meter", "")
df["Length"] = pd.to_numeric(df["Length"])

# Location classifcation 
####################################################################################################

df["Place_fix"] = df["Place"]
df.loc[df["Place"].str.contains("Nacka", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Lidingö", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Bromma", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Haninge", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Huddinge", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Österåker", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Värmdö", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Liljeholmen", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Norrtälje", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Nynäshamn", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Gamla stan", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Danderyd", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Kungsholmen", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Upplands-bro", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Katarina, Sofia", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Ekerö", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Tyresö", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Botkyrka", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Vasastan, Norrmalm", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Östermalm, Djurgården", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Sollentuna", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Solna", case=False), 'Place_fix'] = "Stockholm"
df.loc[df["Place"].str.contains("Västra göteborg", case=False), 'Place_fix'] = "Göteborg"
df.loc[df["Place"].str.contains("Östra göteborg", case=False), 'Place_fix'] = "Göteborg"
df.loc[df["Place"].str.contains("Västra centrum", case=False), 'Place_fix'] = "Göteborg"
df.loc[df["Place"].str.contains("Hisingen", case=False), 'Place_fix'] = "Göteborg"
df.loc[df["Place"].str.contains("Kungsbacka", case=False), 'Place_fix'] = "Göteborg"
df.loc[df["Place"].str.contains("Innerstaden", case=False), 'Place_fix'] = "Göteborg"
df.loc[df["Place"].str.contains("Sydväst", case=False), 'Place_fix'] = "Malmö"
df.loc[df["Place"].str.contains("Centralt söder", case=False), 'Place_fix'] = "Malmö"

df["Place_gt_95k"] = 0
df.loc[df["Place_fix"].str.contains("Stockholm", case=False), 'Place_gt_95k'] = 1
df.loc[df["Place_fix"].str.contains("Göteborg", case=False), 'Place_gt_95k'] = 1
df.loc[df["Place_fix"].str.contains("Malmö", case=False), 'Place_gt_95k'] = 1
df.loc[df["Place"].str.contains("Norrköping", case=False), 'Place_gt_95k'] = 1
df.loc[df["Place"].str.contains("Västerås", case=False), 'Place_gt_95k'] = 1
df.loc[df["Place"].str.contains("Helsingborg", case=False), 'Place_gt_95k'] = 1
df.loc[df["Place"].str.contains("Uppsala", case=False), 'Place_gt_95k'] = 1

# Boat classification 
####################################################################################################

my_boats = {
    "707":                  {"Title": "707", "Quantiles": []},
    "Andunge":              {"Title": "Andunge", "Quantiles": []},
    "Andunge":              {"Title": "Andunge", "Quantiles": []},   
    "Adagio 27":            {"Title": "Adagio 27", "Quantiles": []},
    "Albin 57":             {"Title": "Albin 57", "Quantiles": []},
    "Albin 78":             {"Title": "Albin 78|Albin Cirrus", "Quantiles": []},
    "Albin 79":             {"Title": "Albin 79|Albin79", "Quantiles": []},
    "Albin 82":             {"Title": "Albin 82", "Quantiles": []},
    "Albin Accent":         {"Title": "accent", "Quantiles": []},
    "Albin Alpha":          {"Title": "Albin Alpha", "Quantiles": []},
    "Albin Ballad":         {"Title": "ballad", "Quantiles": []},
    "Albin Cumulus":        {"Title": "Albin Cumulus|Albin 85 Cumulus", "Quantiles": []},
    "Albin Delta":          {"Title": "Albin Delta", "Quantiles": []},        
    "Albin Express":        {"Title": "Albin Express", "Quantiles": []},        
    "Albin Nova":           {"Title": "Albin nova", "Quantiles": []},
    "Albin Vega":           {"Title": "vega", "Quantiles": []},
    "Albin Viggen":         {"Title": "Albin Viggen", "Quantiles": []},
    "Allegro 27":           {"Title": "Allegro 27", "Quantiles": []},
    "Allegro 33":           {"Title": "Allegro 33|Allegro segelbåt långfärd", "Quantiles": []},
    "Amigo 23":             {"Title": "Amigo 23", "Quantiles": []},
    "Amigo 33":             {"Title": "Amigo 33", "Quantiles": []},
    "Amigo 27":             {"Title": "Amigo 27", "Quantiles": []},
    "Aphrodite 22":         {"Title": "Aphrodite 22", "Quantiles": []},
    "Aphrodite 25":         {"Title": "Aphrodite 25", "Quantiles": []},
    "Aphrodite 30":         {"Title": "Aphrodite 30", "Quantiles": []},
    "Aphrodite 34":         {"Title": "Aphrodite 34", "Quantiles": []},
    "Arabesque 30":         {"Title": "Arabesque 30", "Quantiles": []},
    "Arcona 32":            {"Title": "Arcona 32", "Quantiles": []},
    "Arcona 340":           {"Title": "Arcona 340", "Quantiles": []},
    "Arcona 400":           {"Title": "Arcona 400", "Quantiles": []},
    "Arcona 430":           {"Title": "Arcona 430", "Quantiles": []},
    "Arietta 31":           {"Title": "Arietta 31", "Quantiles": []},
    "Askeladden 14":        {"Title": "Askeladden 14", "Quantiles": []},
    "Askeladden 16":        {"Title": "Askeladden 16", "Quantiles": []},
    "Athena 34":            {"Title": "Athena 34", "Quantiles": []},
    "Avance 24":            {"Title": "Avance 24", "Quantiles": []},
    "Avance 36":            {"Title": "Avance 36", "Quantiles": []},
    "Bandholm 26":          {"Title": "Bandholm 26", "Quantiles": []},    
    "Bavaria 30 Cruiser":   {"Title": "Bavaria 30 cruiser|Bavaria 30cr", "Quantiles": []},
    "Bavaria 31":           {"Title": "Bavaria 31", "Quantiles": []},
    "Bavaria 31 Cruiser":   {"Title": "Bavaria 31 cruiser|Bavaria 31cr", "Quantiles": []},
    "Bavaria 32":           {"Title": "Bavaria 32", "Quantiles": []},
    "Bavaria 32 Cruiser":   {"Title": "Bavaria 32 cruiser|Bavaria Cruiser 32", "Quantiles": []},
    "Bavaria 33 Cruiser":   {"Title": "Bavaria 33 cruiser|Bavaria Cruiser 33", "Quantiles": []},
    "Bavaria 34 Cruiser":   {"Title": "Bavaria 34 cruiser|Bavaria Cruiser 34|Bavaria 34 Cr", "Quantiles": []},
    "Bavaria 35 match":     {"Title": "Bavaria 35 match|Bavaria 35match", "Quantiles": []},
    "Bavaria 36":           {"Title": "Bavaria 36|Bavvaria-36|Bavaria 36 Cruiser", "Quantiles": []},
    "Bavaria 36 Cruiser":   {"Title": "Bavaria 36 cruiser|Bavvaria 36 cruiser", "Quantiles": []},
    "Bavaria 37":           {"Title": "Bavaria 37", "Quantiles": []},
    "Bavaria 37 Cruiser":   {"Title": "Bavaria 37 cruiser|Bavaria Cruiser 37", "Quantiles": []},
    "Bavaria 38 Cruiser":   {"Title": "Bavaria 38 Cruiser", "Quantiles": []},    
    "Bavaria 39 Cruiser":   {"Title": "Bavaria 39 Cruiser|Bavaria39 Cruiser", "Quantiles": []},    
    "Bavaria 40":           {"Title": "Bavaria 40", "Quantiles": []},
    "Bavaria 42":           {"Title": "Bavaria 42", "Quantiles": []},
    "Bavaria Vision 42":    {"Title": "Bavaria Vision 42", "Quantiles": []},    
    "Beason 24":            {"Title": "Beason 24", "Quantiles": []},
    "Beason 31":            {"Title": "Beason 31", "Quantiles": []},
    "Beason 36":            {"Title": "Beason 36", "Quantiles": []},
    "Becker 27":            {"Title": "Becker 27", "Quantiles": []},        
    "Bellona 23":           {"Title": "Bellona 23", "Quantiles": []},
    "Beneteau First 27,7":  {"Title": "Beneteau First 27,7", "Quantiles": []},
    "Beneteau First 31.7":  {"Title": "Beneteau First 31,7|Beneteau First 31.7", "Quantiles": []},
    "Beneteau First 36.7":  {"Title": "First 36.7", "Quantiles": []},        
    "Beneteau First 40.7":  {"Title": "Beneteau First 40.7", "Quantiles": []},
    "Beneteau Oceanis 38.1":{"Title": "Oceanis 38.1", "Quantiles": []},
    "Beneteau Oceanis 40.1":{"Title": "Oceanis 40.1", "Quantiles": []},
    "Birdie 24":            {"Title": "Birdie 24|Birdie24", "Quantiles": []},
    "Birdie 32":            {"Title": "Birdie 32", "Quantiles": []},    
    "Boström 31":           {"Title": "Boström 31|B31|B 31|B-31", "Quantiles": []},
    "Brofjord 36":           {"Title": "Brofjord 36", "Quantiles": []},    
    "C55":                  {"Title": "C55", "Quantiles": []},    
    "Carrera Helmsman":     {"Title": "Carrera Helmsman|Carrera Helsman", "Quantiles": []},    
    "CB66 Racer":           {"Title": "CB66 Racer|CB 66 Racer", "Quantiles": []},
    "Colin Archer 35":      {"Title": "Colin Archer 35", "Quantiles": []},    
    "Conqubin 38":          {"Title": "Conqubin 38", "Quantiles": []},
    "Comfort 26":           {"Title": "Comfort 26", "Quantiles": []},
    "Comfort 30":           {"Title": "Comfort 30|Comfort30", "Quantiles": []},
    "Comfort 32":           {"Title": "Comfort 32", "Quantiles": []},
    "Comfort 34":           {"Title": "Comfort 34", "Quantiles": []},    
    "Comfortina 32":        {"Title": "Comfortina 32", "Quantiles": []},
    "Comfortina 39":        {"Title": "Comfortina 39", "Quantiles": []},        
    "Comfortina 44":        {"Title": "Comfortina 44", "Quantiles": []},    
    "Compis 28":            {"Title": "Compis 28|Compis28", "Quantiles": []},
    "Compis 97":            {"Title": "Compis 97", "Quantiles": []},    
    "Contrast 33":          {"Title": "Contrast 33", "Quantiles": []},
    "Cr 370":               {"Title": "Cr 370", "Quantiles": []},
    "Cutlass 27":           {"Title": "Cutlass 27|Cutlas27", "Quantiles": []},
    "Daimio 23":            {"Title": "Daimio 23", "Quantiles": []},    
    "Dehler 31":            {"Title": "Dehler 31|Dehler31", "Quantiles": []},
    "Dehler 34":            {"Title": "Dehler 34", "Quantiles": []},
    "Dehler 35":            {"Title": "Dehler 35", "Quantiles": []},    
    "Dehler 38":            {"Title": "Dehler 38", "Quantiles": []},   
    "Delphia 37":           {"Title": "Delphia 37", "Quantiles": []},   
    "Delphia 40":           {"Title": "Delphia 40", "Quantiles": []},       
    "Diva 35":              {"Title": "Diva 35", "Quantiles": []},
    "Diva 39":              {"Title": "Diva 39", "Quantiles": []},    
    "Dominant 78":          {"Title": "Dominant 78", "Quantiles": []},        
    "Dufour 34 Performance":{"Title": "Dufour 34 Performance|Dufour 34 ePerformance", "Quantiles": []},
    "Dixie 27":             {"Title": "Dixie 27", "Quantiles": []},
    "Dufour 325":           {"Title": "Dufour 325", "Quantiles": []},    
    "Dufour 36":            {"Title": "Dufour 36 ", "Quantiles": []},    
    "Dufour 382":           {"Title": "Dufour 382", "Quantiles": []},
    "Elan 333":             {"Title": "Elan 333", "Quantiles": []},
    "Elan 344 Impression":  {"Title": "Elan 344 Impression", "Quantiles": []},
    "EOL 30":               {"Title": "Eol 30", "Quantiles": []},
    "Fabola Campus 600":    {"Title": "Campus 600", "Quantiles": []},        
    "Fabola Campus 650":    {"Title": "Fabola Campus 650", "Quantiles": []},                
    "Fabola Pokus":         {"Title": "Fabola Pokus", "Quantiles": []},
    "Fabola Scanper":       {"Title": "Fabola Scanper", "Quantiles": []},    
    "Facil 26":             {"Title": "Facil 26", "Quantiles": []},      
    "Facil 35":             {"Title": "Facil 35", "Quantiles": []},       
    "Fingal 28":            {"Title": "Fingal 28", "Quantiles": []},
    "Finnsailer 35":        {"Title": "Finnsailer 35", "Quantiles": []},
    "Flamingo 40":          {"Title": "Flamingo 40", "Quantiles": []},    
    "Forgus 31":            {"Title": "Forgus 31", "Quantiles": []},        
    "Gambler 35":           {"Title": "Gambler 35", "Quantiles": []},    
    "Granada 27":           {"Title": "Granada 27", "Quantiles": []},
    "Granada 31":           {"Title": "Granada 31", "Quantiles": []},  
    "Grand Soleil 45":      {"Title": "Grand Soleil 45", "Quantiles": []},      
    "Guy 22":               {"Title": "Guy 22", "Quantiles": []},
    "H-båt":                {"Title": "H-båt", "Quantiles": []},
    "Hallberg-Rassy 26":    {"Title": "Hallberg Rassy 26|Hellberg Rassy 26|Hallberg-Rassy 26|Hallberg Rasssy 26|Hallberg Ramsay 26", "Quantiles": []},
    "Hallberg-Rassy 29":    {"Title": "Hallberg Rassy 29|Hallberg-Rassy 29", "Quantiles": []},
    "Hallberg-Rassy 31 Mk I":   {"Title": "Hallberg-Rassy 31 Mk I|Hallberg-Rassy 31 Mark I", "Quantiles": []},
    "Hallberg-Rassy 310":   {"Title": "Hallberg-Rassy 310", "Quantiles": []},
    "Hallberg-Rassy 312":   {"Title": "Hallberg Rassy 312|Hallberg-Rassy 312|HR 312 Mk2", "Quantiles": []},
    "Hallberg-Rassy 34":    {"Title": "Hallberg-Rassy 34", "Quantiles": []},
    "Hallberg-Rassy 352":   {"Title": "Hallberg Rassy 352|HR 352", "Quantiles": []},
    "Hallberg-Rassy 37":    {"Title": "Hallberg Rassy 37|Hallberg-Rassy 37", "Quantiles": []},
    "Hallberg-Rassy 38":    {"Title": "Hallberg Rassy 38|Hallberg-Rassy 38", "Quantiles": []},
    "Hallberg-Rassy 42f":   {"Title": "Hallberg Rassy 42f|Hallberg rassy 42 f|Hallberg-Rassy 42 F|Hallberg-Rassy 42F", "Quantiles": []},
    "Hallberg-Rassy 38":    {"Title": "Hallberg-Rassy 43", "Quantiles": []},
    "Hallberg-Rassy 46":    {"Title": "Hallberg-Rassy 46|Hallberg Rassy 46", "Quantiles": []},
    "Hallberg-Rassy 48":    {"Title": "Hallberg-Rassy 48", "Quantiles": []},
    "Hallberg-Rassy Monsun 31":   {"Title": 'Hallberg Rassy Monsun|Hallberg Rassy 31 Monsun|Monsun 31|"Monsun" 31', "Quantiles": []},
    "Hallberg-Rassy Rasmus 35":   {"Title": "Hallberg Rassy Rasmus 35|Hallberg-Rassy 35 Rasmus|Hallberg-Rassy Rasmus 35|Hallberg Rassy, Rasmus|HR 35 Rasmus", "Quantiles": []},
    "Hallberg-Rassy Misil II":    {"Title": "Hallberg Rassy Misil II|Hallberg-Rassy 24 Misil 2|, Missil 2|Hallberg Rassy Misil 2|Hallberg-Rassy Misil II|HR Misil II|Misil 2 Hallby|Misil II |Missile 2|Hallberg rassy Misil2", "Quantiles": []},
    "Hallberg-Rassy 62":    {"Title": "Hallberg-Rassy 62", "Quantiles": []},
    "Hallberg-Rassy 94 Kutter":    {"Title": "Hallberg-Rassy 94 Kutter|Hallberg Rassy 94 kutter|Hallberg Rassy cutter 94|Hallberg Rassy K 94", "Quantiles": []},
    "Hanse 315":            {"Title": "Hanse 315", "Quantiles": []},
    "Hanse 320":            {"Title": "Hanse 320", "Quantiles": []},
    "Hanse 341":            {"Title": "Hanse 341", "Quantiles": []},
    "Hanse 355":            {"Title": "Hanse 355", "Quantiles": []},
    "Hanse 370":            {"Title": "HANSE 370", "Quantiles": []},
    "Hanse 388":            {"Title": "HANSE 388", "Quantiles": []},
    "Havsfidra":            {"Title": "Havsfidra", "Quantiles": []},
    "Hanse 371":            {"Title": "Hanse 371", "Quantiles": []},        
    "Helmsman 23":          {"Title": "Helmsman 23", "Quantiles": []},
    "Hobie cat 16":          {"Title": "Hobie cat 16|Hobie 16", "Quantiles": []},    
    "Hydra 20":             {"Title": "Hydra 20", "Quantiles": []},
    "IF-båt":               {"Title": "IF-båt|IF båt|IF segelbåt|if s-|IF -|fin IF|if e |IF med|If 1061|IF nr 947|If 1975|IF marieholm|IF SWE|IF-Segelbåt|IF- båt|IF Felicia", "Quantiles": []},
    "Iw 23":                {"Title": "Iw 23|Iw23", "Quantiles": []},
    "Iw 31":                {"Title": "IW31|IW-31", "Quantiles": []},    
    "Jasmine 25":           {"Title": "Jasmine 25|Jasmin 25", "Quantiles": []},        
    "Jeanneau Selection 37": {"Title": "Jeanneau Selection 37", "Quantiles": []}, 
    "Jeanneau Sun Odyssey 32.2": {"Title": "Jeanneau Sun Odyssey 32.2|Jeanneau Sun Odyssey 32,2", "Quantiles": []},    
    "Jeanneau Sun Odyssey 35": {"Title": "Jeanneau Sun Odyssey 35|Jeanneau Sun Odessy 35", "Quantiles": []},
    "Johnson 23":           {"Title": "Johnson 23", "Quantiles": []},
    "Johnson 26":           {"Title": "Johnson 26|Jonsson 26", "Quantiles": []},
    "Karlskrona Viggen":        {"Title": "Karlskrona Viggen|Karlskronaviggen", "Quantiles": []},
    "Lady Helmsman":        {"Title": "Lady Helmsman", "Quantiles": []},
    "Lady Helmsman II":     {"Title": "Lady Helmsman II", "Quantiles": []},
    "Laurinkoster 28":           {"Title": "Laurinkoster 28|Laurin 28", "Quantiles": []},
    "Laurinkoster 32":           {"Title": "Laurinkoster 32|Laurin 32|L32 Mk1", "Quantiles": []},
    "Linjett 30":           {"Title": "Linjett 30", "Quantiles": []},    
    "Linjett 32":           {"Title": "Linjett 32", "Quantiles": []},
    "Linjett 33":           {"Title": "Linjett 33", "Quantiles": []},
    "Linjett 34":           {"Title": "Linjett 34", "Quantiles": []},
    "Linjett 35":           {"Title": "Linjett 35", "Quantiles": []},
    "Linjett 37":           {"Title": "Linjett 37", "Quantiles": []},
    "Linjett 40":           {"Title": "Linjett 40", "Quantiles": []},
    "LM 30":                {"Title": "LM 30|LM30", "Quantiles": []},        
    "Macgregor":            {"Title": "Macgregor|Mac Gregor", "Quantiles": []},
    "Magnifik midget":      {"Title": "Magnifik midget|Magnefik Midget|Magnific Midget", "Quantiles": []},
    "Malö 106":             {"Title": "Malö 106|Malø 106", "Quantiles": []},
    "Malö 116":             {"Title": "Malö 116", "Quantiles": []},    
    "Malö 36":              {"Title": "Malö 36", "Quantiles": []},
    "Malö 40":              {"Title": "Malö 40", "Quantiles": []},
    "Malö 42":              {"Title": "Malö 42", "Quantiles": []},
    "Malö 50":              {"Title": "Malö 50", "Quantiles": []},    
    "Mamba 29":             {"Title": "Mamba 29", "Quantiles": []},    
    "Mamba 31":             {"Title": "Mamba 31|Mamba 31/", "Quantiles": []},
    "Mamba 311":            {"Title": "Mamba 311", "Quantiles": []},        
    "Marieholm AC-20":      {"Title": "Marieholm AC 20|Marieholm AC-20|Marieholm AC20", "Quantiles": []},
    "Marieholm MS20":       {"Title": "Marieholm MS 20|Marieholm MS20|Marieholm MS-20|MS20|Marieholms ms 20", "Quantiles": []},
    "Marieholm S20":        {"Title": "Marieholm S 20|Marieholm S20|Marieholm S-20|Marieholm 20", "Quantiles": []},
    "Marieholm 26":         {"Title": "Marieholm 26", "Quantiles": []},    
    "Marieholm 32":         {"Title": "Marieholm 32", "Quantiles": []},
    "Maxi 108":              {"Title": "Maxi 108", "Quantiles": []},
    "Maxi 68":              {"Title": "Maxi 68", "Quantiles": []},
    "Maxi 77":              {"Title": "Maxi 77|Maxi77|Maxi-77", "Quantiles": []},
    "Maxi 84":              {"Title": "Maxi 84|Maxi -84", "Quantiles": []},  
    "Maxi 87":              {"Title": "Maxi 87", "Quantiles": []},
    "Maxi 909":             {"Title": "Maxi 909", "Quantiles": []},
    "Maxi 95":              {"Title": "Maxi 95", "Quantiles": []},
    "Maxi 999":             {"Title": "Maxi 999", "Quantiles": []},
    "Maxi Fenix":           {"Title": "Maxi Fenix|Fenix Dinette", "Quantiles": []},
    "Maxi Magic":           {"Title": "Maxi Magic", "Quantiles": []},
    "Maxi Mixer":           {"Title": "Maxi Mixer", "Quantiles": []},
    "Maxi Racer":           {"Title": "Maxi Racer", "Quantiles": []},
    "Melges 24":            {"Title": "Melges 24", "Quantiles": []},    
    "Monark 540":           {"Title": "Monark 540", "Quantiles": []},            
    "Monark 606":           {"Title": "Monark 606|606", "Quantiles": []},        
    "Monark 700":           {"Title": "Monark 700", "Quantiles": []},            
    "Murena 30":            {"Title": "Murena 30", "Quantiles": []},
    "Mustang Junior":       {"Title": "Mustang Junior", "Quantiles": []},
    "Möre 30":              {"Title": "Möre 30", "Quantiles": []},
    "Mälar 30":             {"Title": "Mälar 30|Mälar-30", "Quantiles": []},
    "Najad 34":             {"Title": "Najad 34", "Quantiles": []},
    "Najad 361":             {"Title": "Najad 361", "Quantiles": []},    
    "Nauticat 33":          {"Title": "Nauticat 33", "Quantiles": []},
    "Nordisk familjebåt":              {"Title": "Nordisk familjebåt|NF ", "Quantiles": []},
    "Nord 80":              {"Title": "Nord 80", "Quantiles": []},
    "Norlin 37":            {"Title": "Norlin 37", "Quantiles": []},
    "Northwind 47":         {"Title": "Northwind 47|Northwind47", "Quantiles": []},    
    "Oe 32":                {"Title": "Oe 32|Oe32", "Quantiles": []},
    "Oe 36":                {"Title": "Oe 36|Oe36", "Quantiles": []},
    "Ohlson 8:8":           {"Title": "Ohlson 8:8|Ohlsson 8:8", "Quantiles": []},
    "Ohlson 22":            {"Title": "Ohlson 22|Ohlsson 22|Olsson 22", "Quantiles": []},
    "Ohlson 29":            {"Title": "Ohlson 29|Ohlsson 29|Olsson 29", "Quantiles": []},
    "Omega 28":             {"Title": "Omega 28|Omega28", "Quantiles": []},
    "Omega 30":             {"Title": "Omega 30", "Quantiles": []},
    "Omega 34":             {"Title": "Omega 34", "Quantiles": []},
    "Omega 36":             {"Title": "Omega 36", "Quantiles": []},
    "Omega 42":             {"Title": "Omega 42", "Quantiles": []},
    "Opal 46":              {"Title": "Opal 46", "Quantiles": []},
    "Parant 25":            {"Title": "Parant 25", "Quantiles": []},
    "Passad":               {"Title": "Passad", "Quantiles": []},
    "Player 311":           {"Title": "Player 311", "Quantiles": []},
    "Polca 28":             {"Title": "Polca 28|Polka 28", "Quantiles": []},
    "Pop 16":               {"Title": "Pop 16", "Quantiles": []},
    "Rapier 31":            {"Title": "Rapier 31", "Quantiles": []},    
    "RB 98":                {"Title": "RB 98", "Quantiles": []},
    "Rival 22":             {"Title": "Rival 22|Rival22", "Quantiles": []},
    "Rigel 98":             {"Title": "Rigel 98", "Quantiles": []},
    "RJ-85":                {"Title": "RJ-85|RJ 85|RJ85", "Quantiles": []},
    "Rock 20":              {"Title": "Rock 20", "Quantiles": []},
    "S30":                  {"Title": "S30|S-30|S 30", "Quantiles": []},
    "Safir":                {"Title": "safir", "Quantiles": []},
    "Scampi":               {"Title": "Scampi", "Quantiles": []},
    "Scanmar 345":          {"Title": "Scanmar 345", "Quantiles": []},
    "Shark 24":             {"Title": "Shark 24|Shark24", "Quantiles": []},
    "Scanmar 33":           {"Title": "Scanmar 33", "Quantiles": []},
    "Scanner 38":           {"Title": "Scanner 38", "Quantiles": []},
    "Shipman 28":           {"Title": "Shipman 28", "Quantiles": []},
    "Skippi 650":           {"Title": "Skippi 650", "Quantiles": []},
    "Smaragd":              {"Title": "Smaragd", "Quantiles": []},
    "Soling":               {"Title": "Soling", "Quantiles": []},    
    "Späckhuggare":         {"Title": "Späckhuggare", "Quantiles": []},    
    "Stortriss":            {"Title": "Stortriss", "Quantiles": []},
    "Sveakryssare":            {"Title": "Sveakryssare", "Quantiles": []},    
    "Sweden Yachts 34":     {"Title": "Sweden Yachts 34|Sweden Yacht C34|Sweden Yacht C 34|Sweden Yacht 34", "Quantiles": []},
    "Sweden Yachts 340":    {"Title": "Sweden Yachts 340", "Quantiles": []},
    "Sweden Yachts 370":    {"Title": "Sweden Yachts 370", "Quantiles": []}, 
    "Swede 38":             {"Title": "Swede 38|Swede-38", "Quantiles": []},
    "Sunwind 20":           {"Title": "Sunwind 20|Sunvind 20", "Quantiles": []},
    "Sunwind 26":           {"Title": "Sunwind 26", "Quantiles": []},
    "Sunwind 29":           {"Title": "Sunwind 29", "Quantiles": []},
    "Tetis 29":             {"Title": "Tetis 29", "Quantiles": []},    
    "Trio 80":              {"Title": "Trio 80", "Quantiles": []},
    "Trio 92":              {"Title": "Trio 92", "Quantiles": []},
    "Triss Magnum":         {"Title": "Triss Magnum", "Quantiles": []},
    "Triss Norlin":         {"Title": "Triss Norlin", "Quantiles": []},    
    "Tur 80":               {"Title": "Tur 80", "Quantiles": []},
    "Tur 84":               {"Title": "Tur 84", "Quantiles": []},
    "Tur 97":               {"Title": "Tur 97", "Quantiles": []},    
    "Uttern 450":           {"Title": "Uttern 450", "Quantiles": []},        
    "Vagabond 31":          {"Title": "Vagabond 31|Vagabond31", "Quantiles": []},
    "Valiant 18":           {"Title": "Valiant 18|Valliant 18", "Quantiles": []},
    "W30 Kompromiss":       {"Title": "W30 Kompromiss|W30 - Kompromiss", "Quantiles": []},
    "Wasa 30":              {"Title": "Wasa 30", "Quantiles": []},
    "Wasa 360":             {"Title": "Wasa 360", "Quantiles": []},
    "Wasa 41":              {"Title": "Wasa 41", "Quantiles": []},
    "Wasa 410":             {"Title": "Wasa 410", "Quantiles": []},
    "Wasa 55":              {"Title": "Wasa 55", "Quantiles": []},
    "Winga 29":             {"Title": "Winga 29|Winga29", "Quantiles": []},
    "Winga 750":            {"Title": "Winga 750", "Quantiles": []},
    "Winga 78":             {"Title": "Winga 78|Winga-78", "Quantiles": []},
    "Winga 87":             {"Title": "Winga 87|Winga87", "Quantiles": []},
    "Vindö 30":             {"Title": "Vindö 30", "Quantiles": []},
    "Vindö 32":             {"Title": "Vindö 32|Vindö-32", "Quantiles": []},
    "Vindö 40":             {"Title": "Vindö 40", "Quantiles": []},
    "Väderö 24":            {"Title": "Väderö 24", "Quantiles": []},        
    "Västbris":             {"Title": "Västbris", "Quantiles": []},    
    "X-79":                 {"Title": "X-79", "Quantiles": []},
    "X-99":                 {"Title": "X99|X-99", "Quantiles": []},
    "Åh 30":                {"Title": "Åh 30|Åh30", "Quantiles": []},

    "Albin Ballad":                {"Title": "Albin Ballad Joker S30", "Quantiles": []},
"Piraya":                {"Title": "Piraya", "Quantiles": []}
    }
for i in my_boats.keys():
    df.loc[df["Title"].str.contains(my_boats[i]["Title"], case=False), "Boat"] = i
    prices = df.loc[df["Boat"] == i]["Price"] 
    lengths = df.loc[df["Boat"] == i]["Length"]
    my_boats[i]["Quantiles"] = np.quantile(prices,q=[0.25,0.5,0.75], interpolation="nearest")
    df.loc[df["Boat"] == i, "25th_quartile"] = my_boats[i]["Quantiles"][0]
    df.loc[df["Boat"] == i, "Median"] = my_boats[i]["Quantiles"][1] #Medial boat price per model
    df.loc[df["Boat"] == i, "75th_quartile"] = my_boats[i]["Quantiles"][2]
    df.loc[df["Boat"] == i, "Length_median"] = np.median(lengths) #Median boat length per model
    df["lt_25th_quartile"] = np.where((df["Price"] < df["25th_quartile"]), 1,0)
    df["gt_75th_quartile"] = np.where((df["Price"] > df["75th_quartile"]), 1,0)
    df["gt_median"] = np.where((df["Price"] > df["Median"]), 1,0)
    df["sthlm"] = np.where((df["Place_fix"] == "Stockholm"), 1,0)

############################################################################################################
total_obs_n_old = len(pd.read_csv("C:\\Users\\jbuep\\My Drive\\Data\\sailboats\\cleaned_data.csv"))
total_obs_n = len(df)
categ_obs_n = len(df.loc[df["Boat"].notna()])
print("\nNumber of observations in original dataset" + str(total_obs_n_old).rjust(20,"."))
print("Number of observations in new dataset" + str(total_obs_n).rjust(20,"."))
print("Number of observations categorized in new" + str(categ_obs_n).rjust(8,"."))
print("Categorization ratio: {0:.1%}".format(round(categ_obs_n / total_obs_n ,3)).rjust(20,"."))
print("\nBoat models with counts >= 20:")
for i, name in enumerate(df["Boat"].value_counts().index.to_list()):
    if df["Boat"].value_counts()[i] >= 20:
        print(str(name).ljust(15,".") + str(df["Boat"].value_counts()[i]).rjust(6,"."))
    

#Export cleaned data
outfile = os.path.join(csv_path, "cleaned_data.csv")
df.to_csv(outfile, index=False)

# Export supplementary analysis data
location_counts = df["Place_fix"].value_counts()
with pd.ExcelWriter("C:/Users/jbuep/My Drive/Data/sailboats/analysis.xlsx") as writer:
    location_counts.to_excel(writer, sheet_name="place_fix")
    df[["Title", "Boat", "DateTime", "Price", "Description"]].to_excel(writer, sheet_name="Title")

print("Script 02_data_cleaning.py is done!")