import pandas as pd 
from pathlib import Path
import re

def read_fortnox_1930_kontoanalys(filepath):
    account_1930_file = open(filepath)
    account_1930_text = account_1930_file.read()
    account_1930_file.close()
    account_1930_text_split = account_1930_text.split('\n')

    vernr_pattern = r'\b(?:[A-Z]{1,2} [0-9]{1,4})\b'
    row_values_list = []
    for row in account_1930_text_split:
        if len(row.split('\t')) <= 1:
            continue
        if row.split('\t')[0] == 'Konto':
            columns = [x for x in row.split('\t') if ((x != '\xa0') & (x != ''))]
        if bool(re.search(vernr_pattern, row.split('\t')[1])):
            row_values = row.split('\t')
            row_values.pop(2)
            row_values = [x if x != '\xa0' else '' for x in row_values if ((x != ''))]
            row_values_list.append(row_values)
    account_1930_df = pd.DataFrame(row_values_list, columns=columns)

    for column in ['Debet', 'Kredit', 'Saldo']:
        account_1930_df[column] = account_1930_df[column].str.replace(' ', '')
        account_1930_df[column] = account_1930_df[column].str.replace(',', '.')
        account_1930_df[column] = pd.to_numeric(account_1930_df[column])
    account_1930_df['Datum'] = account_1930_df['Datum'].str.replace(' ', '')
    account_1930_df['Datum'] = pd.to_datetime(account_1930_df['Datum'])
    
    account_1930_df.Kredit = account_1930_df.Kredit * -1
    account_1930_df["Belopp"] = account_1930_df.Debet.combine_first(account_1930_df.Kredit)
    account_1930_df.drop(['Debet', 'Kredit'], axis=1, inplace=True)

    account_1930_df_reduced = account_1930_df[['Datum', 'Text', 'Saldo', 'Belopp']].copy()
    account_1930_df_reduced.set_index('Datum', inplace=True)
    account_1930_df_reduced['Källa'] = 'Fortnox'

    return account_1930_df_reduced

