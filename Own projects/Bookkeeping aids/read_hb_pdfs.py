from tabula import read_pdf
import pandas as pd

def read_hb_files(filepath):
    df = read_pdf(filepath,pages="all")[0]
    for column in ['Insättning/Uttag', 'Bokfört saldo']:
        df[column] = df[column].str.replace(' ', '')
        df[column] = df[column].str.replace(',', '.')
        df[column] = pd.to_numeric(df[column])

    for column in ['Bokföringsdag', 'Reskontradag',   'Valutadag']:
        df[column] = df[column].str.replace(' ', '')
        df[column] = pd.to_datetime(df[column])


    df_reduced = df[['Bokföringsdag', 'Referens', 'Insättning/Uttag', 'Bokfört saldo']].copy()
    df_reduced.rename({'Bokföringsdag': 'Datum', 'Referens': 'Text', 'Insättning/Uttag': 'Belopp', 'Bokfört saldo': 'Saldo'}, axis=1, inplace=True)
    df_reduced.set_index('Datum', inplace=True)
    df_reduced['Källa'] = 'Bank'
    return df_reduced
