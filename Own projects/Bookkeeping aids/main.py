import read_fortnox_kontoanalys, read_hb_pdfs
import pandas as pd

hb_filepath = r"C:\Users\jbuep\My Drive\Personligt\Kod\python\Own projects\Bookkeeping aids\input\Handelsbanken Transaktioner oktober.pdf"
account_1930_filepath = r"C:\Users\jbuep\My Drive\Personligt\Kod\python\Own projects\Bookkeeping aids\input\Fortnox 1930 report.txt"

hb_df = read_hb_pdfs.read_hb_files(filepath=hb_filepath)
account_1930_df = read_fortnox_kontoanalys.read_fortnox_1930_kontoanalys(filepath=account_1930_filepath)


# Merge datadets and roll out dates
df_full = pd.concat([hb_df, account_1930_df], axis=0)

start_date = min(account_1930_df.index.min(), hb_df.index.min())
end_date = max(account_1930_df.index.max(), hb_df.index.max())
dfx = pd.DataFrame(index=pd.date_range(start_date, end_date))

df_joined = dfx.join(df_full, how='left')

df_joined.to_clipboard()
print("Script executed")