import pandas as pd
import numpy as np


bank_account_file = r"C:\Users\jbuep\My Drive\Personligt\Kod\python\Own projects\Bookkeeping aids\bank_account_history.xlsx"
bokio_1930_file = r"C:\Users\jbuep\My Drive\Personligt\Kod\python\Own projects\Bookkeeping aids\1930_Huvudbok 2023-01-01-2023-12-31 240113.xlsx"


df_bank_account = pd.read_excel(bank_account_file)
df_1930 = pd.read_excel(bokio_1930_file)

#Adjust datasets
df_1930.drop(0, axis=0, inplace=True)
df_1930.drop(len(df_1930), axis=0, inplace=True)
df_1930.columns = ['Datum', 'Verifikat_bokio', 'Text', 'Debet', 'Kredit', 'Saldo']
df_1930.drop('Verifikat_bokio', axis=1, inplace=True)
df_1930.Datum = pd.to_datetime(df_1930.Datum)
df_1930.replace(to_replace=0.00, value=np.nan, inplace=True)
df_1930.Kredit = df_1930.Kredit * -1
df_1930["Belopp"] = df_1930.Debet.combine_first(df_1930.Kredit)
df_1930["Belopp_ackumulerat"] = df_1930["Belopp"].cumsum()
df_1930.drop(['Debet', 'Kredit'], axis=1, inplace=True)
df_1930.set_index('Datum', inplace=True)
df_1930['Källa'] = 'Bokio'

df_bank_account["Belopp_ackumulerat"] = df_bank_account['Belopp'].cumsum()
df_bank_account['Bokföringsdatum'] = pd.to_datetime(df_bank_account['Bokföringsdatum'])
df_bank_account['Valutadatum'] = pd.to_datetime(df_bank_account['Valutadatum'])
df_bank_account.set_index('Bokföringsdatum', inplace=True)
df_bank_account.drop(['Verifikationsnummer', 'Valutadatum'], axis=1, inplace=True)
df_bank_account = df_bank_account[['Text/mottagare', 'Saldo', 'Belopp', 'Belopp_ackumulerat']]
df_bank_account['Källa'] = 'Bank'
df_bank_account.columns = df_1930.columns



# Merge datadets and roll out dates
df_full = pd.concat([df_bank_account, df_1930], axis=0)

start_date = min(df_1930.index.min(), df_bank_account.index.min())
end_date = max(df_1930.index.max(), df_bank_account.index.max())
dfx = pd.DataFrame(index=pd.date_range(start_date, end_date))

df_joined = dfx.join(df_full, how='left')
df_joined.loc[(df_joined['Text'].str.contains('Tx')) | 
              (df_joined['Text'].str.endswith(' b')) | 
              (df_joined['Text'].str.contains('(?i)rapport')) | 
              (df_joined['Text'].str.contains('20230401'))
              , 'tx_or_transfer_b'] = 1
df_joined['tx_or_transfer_b'].fillna(value= 0, inplace = True)


df_joined.index.name='Datum'
df_joined.groupby(['Datum', 'Källa'])[['Saldo','Belopp', 'Belopp_ackumulerat']].min()



df_joined.to_clipboard()
print("Script executed")

