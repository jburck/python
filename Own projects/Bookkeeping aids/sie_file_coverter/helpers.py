import re
import datetime
import pandas as pd

def text_to_list_of_tuples(data):
    data_list = data.split("\n")
    list_transactions = []
    pattern_nominal = r'\} (-?\d+(\.\d+)?)'
    pattern_date = r'\b\d{6}\b'
    pattern_account = r'(\d+) \{'

    for line in data_list:
        if (line.startswith("#PSALDO 0")) | line.startswith("#PSALDO -1"):
            matches_nominal = re.search(pattern_nominal, line)
            date_matches = re.findall(pattern_date, line)
            account_matches = re.search(pattern_account, line)

            if matches_nominal:
                second_to_last_numerical_value = matches_nominal.group(1)
            else:
                second_to_last_numerical_value = None
            if date_matches:
                date = date_matches[0]
                date = datetime.date(int(date[:4]), int(date[-2:]), 1)
            else:
                date = None  # or handle the case when there is no date
            if account_matches:
                account = account_matches.group(1)
            else:
                account = None  # or handle the case when there is no account        
            list_transactions.append((date, account, second_to_last_numerical_value))
    return list_transactions


def list_to_df(data_list):
    data_df = pd.DataFrame(data_list, columns=['date', 'account', 'nominal'])
    #data_df.set_index('date', inplace=True)
    data_df.date = pd.to_datetime(data_df.date)
    data_df.nominal = pd.to_numeric(data_df.nominal)
    return data_df
