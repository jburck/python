
def new_column_value(account):
    account = int(account)
    if account == 1110:
        return 'Byggnader'
    elif account == 1119:
        return 'Ackumulerade avskrivningar på byggnader'
    elif account == 1130:
        return 'Mark'
    elif account == 1354:
        return 'Obligationer'
    elif account == 1510:
        return 'Kundfordringar'
    elif account == 1630:
        return 'Avräkning för skatter och avgifter (skattekonto)'
    elif account == 1730:
        return 'Förutbetalda försäkringspremier'
    elif account == 1790:
        return 'Övriga förutbetalda kostnader och upplupna intäkter'
    elif account == 1930:
        return 'Företagskonto Eken'
    elif account == 1938:
        return 'Faktura- och betaltjänster'
    elif account == 1940:
        return 'Placeringskonto Eken'
    elif account == 2081:
        return 'Aktiekapital'
    elif account == 2091:
        return 'Balanserad vinst eller förlust'
    elif account == 2093:
        return 'Erhållna aktieägartillskott'
    elif account == 2099:
        return 'Årets resultat'
    elif account == 2350:
        return 'Andra långfristiga skulder till kreditinstitut'
    elif account == 2397:
        return 'Mottagna depositioner, långfristiga'
    elif account == 2410:
        return 'Andra kortfristiga låneskulder till kreditinstitut'
    elif account == 2641:
        return 'Debiterad ingående moms'
    elif account == 2971:
        return 'Förutbetalda hyresintäkter'
    elif account == 2990:
        return 'Övriga upplupna kostnader och förutbetalda intäkter'
    elif account == 3004:
        return 'Hyresintäkter, momsfria'
    elif account == 3985:
        return 'Erhållna statliga bidrag'
    elif account == 3999:
        return 'Övriga rörelseintäkter'
    elif account == 5040:
        return 'Vatten och avlopp'
    elif account == 5060:
        return 'Städning och renhållning'
    elif account == 5070:
        return 'Reparation och underhåll av lokaler'
    elif account == 5165:
        return 'Trädgårdsskötsel'
    elif account == 5160:
        return 'Städning och renhållning'
    elif account == 5170:
        return 'Reparation och underhåll av fastighet'
    elif account == 5191:
        return 'Fastighetsskatt/fastighetsavgift'
    elif account == 5310:
        return 'El för drift'
    elif account == 5411:
        return 'Förbrukningsinventarier med en livslängd på mer än ett år'
    elif account == 5420:
        return 'Programvaror'
    elif account == 5460:
        return 'Förbrukningsmaterial'
    elif account == 5690:
        return 'Övriga kostnader för transportmedel'
    elif account == 6060:
        return 'Kreditförsäljningskostnader'
    elif account == 6061:
        return 'Kreditupplysning'
    elif account == 6062:
        return 'Inkasso och KFM-avgifter'
    elif account == 6250:
        return 'Postbefordran'
    elif account == 6310:
        return 'Företagsförsäkringar'
    elif account == 6530:
        return 'Redovisningstjänster'
    elif account == 6570:
        return 'Bankkostnader'
    elif account == 6590:
        return 'Övriga externa tjänster'
    elif account == 6950:
        return 'Tillsynsavgifter myndigheter'
    elif account == 6991:
        return 'Övriga externa kostnader, avdragsgilla'
    elif account == 7821:
        return 'Avskrivningar på byggnader'
    elif account == 8311:
        return 'Ränteintäkter från bank'
    elif account == 8314:
        return 'Skattefria ränteintäkter'
    elif account == 8410:
        return 'Räntekostnader för långfristiga skulder'
    elif account == 8490:
        return 'Övriga skuldrelaterade poster'
    else:
        return 'Other'


def identify_renovations(data_df):
    data_df.loc[(data_df.date == '2022-08-01') & (data_df.account == '5170'), 'account'] = '5171' # köksbygge
    data_df.loc[(data_df.date == '2023-12-01') & (data_df.account == '6590'), 'account'] = '6591' # OVK


# Vad är felavhjälpande underhåll https://www.idus.se/blogg/sa-blir-ni-battre-pa-avhjalpande-underhall/
def account_segmentation(account_number):
    account_number = int(account_number)
    if account_number in [3004, 3985, 3999, 8311, 8314]:
        return 'Hyrer & övriga intäkter'
    elif account_number in [5040, 5191, 5310, 6310, 6950]:
        return 'Driftskostnader'
    elif account_number in (5171, 6591):
        return 'Renoveringsprojekt & kapitalvaror'
    elif account_number in [5420, 6060, 6061, 6062, 6530, 8490]:
        return 'Administrativa kostnader'
    elif account_number in [5060, 5070, 5160, 5165, 5170, 5411, 5460, 5690, 6250, 6590, 6991]:
        return 'Fastighetsförvaltning/underhåll'
    elif account_number == 7821:
        return 'Avskrivningar'
    elif account_number in [6570, 8410]:
        return 'Bank & Räntekostnader'
    else:
        return 'Övriga'