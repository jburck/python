
'''
1. Öppna SE fil med text editor och spara ner som txt.
2. Byt ut alla † till å, „ till ä , ” till ö

2024-02-07: "corrections" tar bort vissa poster som inte är representativa.
'''

import helpers, segmentation
import datetime
import pandas as pd

'''
- redovisningstjänster 7500 
- el för december 3122 + 3746 = 6868 (lägg ej till eftersom denna kostnad har försköts i jan 2023 också)
- Rörinspektion 2800 (lägg ej till då denna kostand försköts i jan 2023 också)
'''

corrections = [(datetime.date(2023,4,1), '6530', 15750),
                 (datetime.date(2023,5,1), '6530', -15750),]


with open("C:\\Users\\jbuep\\My Drive\\Personligt\\Kod\\python\\Own projects\\Bookkeeping aids\\sie_file_coverter\\CitySwedenPropertiesAB2023.txt", "r", encoding="latin-1") as file:
    data = file.read().rstrip()

data_list = helpers.text_to_list_of_tuples(data=data)
data_df = helpers.list_to_df(data_list=data_list)
data_df['account_name'] = data_df['account'].apply(segmentation.new_column_value)
segmentation.identify_renovations(data_df)
data_df['segmentation'] = data_df['account'].apply(segmentation.account_segmentation)

index_list = []
for correction in corrections:
    index_list.append(data_df.loc[((data_df.date == pd.to_datetime(correction[0])) &
                      (data_df.account == correction[1]) &
                      (data_df.nominal == correction[2])
                      )].index[0])
data_df.drop(index_list, inplace=True)

print("Script has executed")