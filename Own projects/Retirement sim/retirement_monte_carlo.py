''' 
TODO: 
2. Portföljutvecklingsgraf - byt till area plots med 50% spann, 75% spann 90% spann.
3. Portföljutvecklingsgraf - Uppdatera axis-labels (ålder och portföljvärde)
4. Nytt tillgångsslag med viss allokering och kovarians med aktier
5. Sök efter safe withdrawal rate, pensionsålder eller sparmängd givet viss konfidensnivå
6. histogram som visar distribution av misslyckade utfall över år.
7. Skapa funktioner och helper-fil
8. Skapa konfig-fil
9. Populera word dokument med grafer och statistik för snabbare leverans?

Done: 
Ta väck onödig output tex age+1 portfölj
Bugg: Om portfoliosize today är 0 så blir det en bugg med att ta ut percentiler då variansen år 1 = 0
'''

import helpers
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

######### SIMULATION SETTINGS #########
n_simulations = 1000 # Number of simulations to run

######### PERSONAL FINANCE VARIABLES #########
portfolio_size_today = 0 # This is the value of your savings today
monthly_withdrawal = 15000 #This is the monthly amount you plan to take out of your retirement accounts 
monthly_deposits = 30000   #This is the amount you plan to add to savings until retirement.
ratio_state_p_age = 0.8 # The ratio of monthly_withdrawal to take out during state pension age
ratio_retirement_2 = 0.4 # The ratio of monthly_withdrawal to take out the last few years
ratio_bad_year = 1 # Ratio of monthly withdrawal if stock return is negative. Periods retirement 1 -> retirement 2.

extra_withdrawal_perc = 0.00 # Value of portfolio to withdraw extra when it exceeds the value-threshold
extra_withdrawal_threshold = 20 # value of portfolio in relatio to YEARLY withdrawal at which extra withdrawal takes place

########## PORTFOLIO VARIABLES #########
p_annual_return_1 = 0.055 # This is annual average return on the portfolio for the first period
p_stddev = 0.16 # This is stadard deviation of the anual return

########## AGE VARIABLES #########
age = 36 # curret age
retirement_age_1 = 50 #This is the age at which you retire
state_pension_age = 70 # This is the age at which you get state pension
retirement_age_2 = 85 #This is the age at which you become inactive
death_age = 92 # This is how long you assume to live. https://www.ons.gov.uk/peoplepopulationandcommunity/birthsdeathsandmarriages/lifeexpectancies/articles/whatareyourchancesoflivingto100/2016-01-14

#plt.xkcd()

##################################
### RUN MONTE CARLO SIMULATION ###
##################################
rw_sequence_list = []
withdrawal_sequence_list = []
for i in range(n_simulations):
    rw_sequence, withdrawal_sequence = helpers.random_walk(start=age, death_age=death_age, value=portfolio_size_today,
    p_annual_return_1=p_annual_return_1, p_stddev=p_stddev,  monthly_deposits=monthly_deposits, 
                retirement_age_1=retirement_age_1, state_pension_age=state_pension_age, monthly_withdrawal=monthly_withdrawal, 
                ratio_bad_year=ratio_bad_year, retirement_age_2=retirement_age_2, ratio_state_p_age=ratio_state_p_age,  
                ratio_retirement_2=ratio_retirement_2, extra_withdrawal_threshold=extra_withdrawal_threshold, 
                extra_withdrawal_perc=extra_withdrawal_perc)
    rw_sequence_list.append(rw_sequence)
    withdrawal_sequence_list.append(withdrawal_sequence)


############################
### CALCULATE STATISTICS ###
############################
# Make list of list into a numpy array to get last value
rw_sequence_array = np.array(rw_sequence_list)
final_value_list = rw_sequence_array[:,-1]
n_tot = len(final_value_list)
n_nonzero = np.count_nonzero(final_value_list)
sucess_rate = n_nonzero / n_tot
print("##### ",round(sucess_rate*100, 1), "% of cases are sucessful #####")

# Get some percentiles of portfolio value at retirement age
year_before_retirement = retirement_age_1 - age
retirement_value_list = rw_sequence_array[:,year_before_retirement]
retirement_value_list.sort()

list_length = len(retirement_value_list)
tenth_perc = int(list_length * 0.1)
fifthyeth_perc = int(list_length * 0.5)
ninetyeeth_perc = int(list_length * 0.9)

print("Portfolio value at retirement age: ")
print("10th perc:", round(retirement_value_list[tenth_perc]))
print("50th perc:", round(retirement_value_list[fifthyeth_perc]))
print("90th perc:", round(retirement_value_list[ninetyeeth_perc]))

    
###########################################
############ PLOT RANDOM WALKS ############
###########################################
for i in range (50):
    plt.plot(rw_sequence_list[i], alpha=1)
    plt.ticklabel_format(style="plain")
    plt.ylim(top=25000000, bottom=0)
    plt.xlim(left=0)
    plt.title("Portfolio development scenarios")
plt.show()

for i in range (50):
    plt.plot(withdrawal_sequence_list[i], alpha=1)
    plt.ticklabel_format(style="plain")
    plt.ylim(top=1000000, bottom=0)
    plt.xlim(left=0)
    plt.title("Yearly withdrawal scenarios")
plt.show()


#########################################
### CALCULATE AND PLOT AVERAGE CURVES ###
#########################################
rw_sequence_array_sorted = np.sort(rw_sequence_array, axis=0)
prtf_fifthyeth_perc_seq, prtf_twentyfifth_perc_seq, prtf_fifth_perc_seq = [[] for i in range(3)]
for i in range(len(rw_sequence_array_sorted[1,:])):
    prtf_fifthyeth_perc = np.percentile(rw_sequence_array_sorted[:,i], 50)
    prtf_twentyfifth_perc = np.percentile(rw_sequence_array_sorted[:,i], 25)
    prtf_fifth_perc = np.percentile(rw_sequence_array_sorted[:,i], 5)
    prtf_fifthyeth_perc_seq.append(prtf_fifthyeth_perc)
    prtf_twentyfifth_perc_seq.append(prtf_twentyfifth_perc)
    prtf_fifth_perc_seq.append(prtf_fifth_perc)

plt.plot(prtf_fifth_perc_seq, "red", label="5%")
plt.plot(prtf_twentyfifth_perc_seq, "orange", label="25%")
plt.plot(prtf_fifthyeth_perc_seq, "green", label="50%")
plt.title("Typical portfolio scenarios")
plt.legend()
plt.show()

# Withdrawal averages
wd_sequence_array = np.array(withdrawal_sequence_list)
wd_final_value_list = wd_sequence_array[:,-1]
wd_sequence_array_sorted = np.sort(wd_sequence_array, axis=0)
wd_fifthyeth_perc_seq, wd_twentyfifth_perc_seq, wd_fifth_perc_seq = [[] for i in range(3)]

for i in range(len(wd_sequence_array_sorted[1,:])):
    wd_fifthyeth_perc = np.percentile(wd_sequence_array_sorted[:,i], 50)
    wd_twentyfifth_perc = np.percentile(wd_sequence_array_sorted[:,i], 25)
    wd_fifth_perc = np.percentile(wd_sequence_array_sorted[:,i], 5)
    wd_fifthyeth_perc_seq.append(wd_fifthyeth_perc)
    wd_twentyfifth_perc_seq.append(wd_twentyfifth_perc)
    wd_fifth_perc_seq.append(wd_fifth_perc)

plt.plot(wd_fifth_perc_seq, "red", label="5%")
plt.plot(wd_twentyfifth_perc_seq, "orange", label="25%")
plt.plot(wd_fifthyeth_perc_seq, "green", label="50%")
plt.title("Typical withdrawal scenarios")
plt.legend()
plt.show()


# Calculate yearly portfolio values that resulted in success

df_rw = pd.DataFrame(rw_sequence_list)
df_rw_not_zero = df_rw[df_rw.iloc[:,-1] > 0]
portfolio_min_values = df_rw_not_zero.min().values
# portfolio_median_values = df_rw_not_zero.median().values
# portfolio_first_quartile_values = df_rw_not_zero.quantile(q=0.25)
plt.plot(portfolio_min_values, "red", label="Lowest")
plt.title("Yearly portfolio values that resulted in sucess")
plt.ticklabel_format(style="plain")
plt.legend()
plt.show()

print("Lowest portfolio value at retirement that was sucessful:", round(int(portfolio_min_values[year_before_retirement]), -3))