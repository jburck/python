import random

def random_walk(start, death_age, value, p_annual_return_1, p_stddev, monthly_deposits, 
                retirement_age_1, state_pension_age, monthly_withdrawal, ratio_bad_year,
                retirement_age_2, ratio_state_p_age,  ratio_retirement_2, extra_withdrawal_threshold,
                extra_withdrawal_perc):
    rw_sequence = [value]
    withdrawal_sequence = []
    extra_withdrawal = 0
    real_withdrawal = 0
    for i in range(start, death_age):
        d_value = value * random.normalvariate(p_annual_return_1, p_stddev)

        if i < retirement_age_1:
            d_value += (monthly_deposits*12)
        elif i < state_pension_age:
            if d_value < 0:
                real_withdrawal = (monthly_withdrawal*12*ratio_bad_year)
                d_value -= real_withdrawal
            else:
                real_withdrawal = (monthly_withdrawal*12)
                d_value -= real_withdrawal
        elif i < retirement_age_2:
            if d_value < 0:
                real_withdrawal = (monthly_withdrawal*12*ratio_state_p_age*ratio_bad_year)
                d_value -= real_withdrawal
            else:
                real_withdrawal = (monthly_withdrawal*12*ratio_state_p_age)
                d_value -= real_withdrawal
        elif i < death_age:
            real_withdrawal = (monthly_withdrawal*12*ratio_retirement_2)
            d_value -= real_withdrawal
       
        # Update value. If the portfolio exceeds extra withdrawal threshold, then withdraw more. 
        
        if (value + d_value) <= 0:
            value = 0
        elif value > (monthly_withdrawal * 12 * extra_withdrawal_threshold):
            extra_withdrawal = value * extra_withdrawal_perc
            value = value + d_value - extra_withdrawal 
            real_withdrawal = real_withdrawal + extra_withdrawal
        else:
            value = value + d_value

        # Withdrawal sequence
        if i < retirement_age_1:
            real_withdrawal = 0
        elif value < real_withdrawal:
            real_withdrawal = value
        
        withdrawal_sequence.append(real_withdrawal)
        rw_sequence.append(value)
    return rw_sequence, withdrawal_sequence