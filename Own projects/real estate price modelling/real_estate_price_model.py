import warnings, helpers
import pandas as pd

"""
Idéer:
Feature engineering (Lägg till kommun / län?)
"""

warnings.filterwarnings("ignore")
random_state = 42
oot = True
oot_start_date = '2023-03-01'
n_cv = 4
n_mf_combinations = 2
n_knear_combinations = 2
n_lasso_alphas = 20
plots = False
label             = "Price-to-rent"
variables         =  ["Byggår","Hyresintäkt","Kallhyra","Antal lgh","Boyta","Antal lokaler","Lokalyta","Yta_total","Enheter_total","Tomt kvm","Renoveringsobjekt","Invånare tätort","Avstånd tätort, km","t","läge","läge2","Avstånd malmö/gbg"]
features          = ["Renoveringsobjekt", "läge", "t"]

#### DATA IMPORT #### 
df = pd.read_excel("C:/Users/jbuep/My Drive/CitySweden Properties AB/05. Kalkyler och planering/04. Fastighetsdatabas/Objektdatabas.xlsx", \
                   sheet_name="Data", parse_dates=True, index_col="Objektnr") 
df.set_index('logg-datum', inplace=True)
print(df.describe())
helpers.feature_engineering(df) 

#### PLOTTING ####
helpers.plot_mean_time_series(df=df, plot=plots, variables=["Price-to-rent", "Hyresintäkt", "läge", "Avstånd malmö/gbg"])  # Plot Variables over time - inconsistent sampling
helpers.plot_pairplots(df=df, plot=plots,
                       column_list=[[label]+["Byggår", "Boyta", "Lokalyta", "Yta_total", "t"],
                                   [label]+["Antal lgh", "Antal lokaler", "Enheter_total", "Hyresintäkt" ],
                                   [label]+["läge", "läge2", "Avstånd malmö/gbg"]],)
helpers.plot_correlations(df=df, plot=plots,
                          column_list=[[label]+["Byggår", "Boyta", "Lokalyta", "Yta_total", "t"],
                                       [label]+["Antal lgh", "Antal lokaler", "Enheter_total", "Hyresintäkt"],
                                       [label]+["läge", "läge2", "Avstånd malmö/gbg"]])

#### DATA PREP ####
df_poly = helpers.feature_transformation_and_imputation(df=df, variables=variables, label=label)
X_train, X_test, y_train, y_test  = helpers.tt_split(df=df_poly, label=label, oot=oot, oot_start_date=oot_start_date, test_size=0.15, rand=42)
df["Dataset"] = ["Train" if x in y_train.index else "Test" for x in df_poly.index]

# Stepwise
import pandas as pd
import numpy as np
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from mlxtend.feature_selection import SequentialFeatureSelector, ExhaustiveFeatureSelector


print("Running exhaustive-selection...")
efs = ExhaustiveFeatureSelector(linear_model.LinearRegression(),
                                min_features=2,
                                max_features=2,
                                scoring='r2',
                                print_progress=True,
                                n_jobs=3,
                                cv=3)
selected_features_efs = efs.fit(X_train, y_train)
helpers.run_OLS(X=X_train[list(selected_features_efs.best_feature_names_)], y=y_train)


print("Running forward-selection...")
sfs = SequentialFeatureSelector(linear_model.LinearRegression(),
                                k_features=(4),
                                forward=True,
                                scoring='r2',
                                fixed_features=selected_features_efs.best_idx_,
                                cv=5)
selected_features_sfs = sfs.fit(X_train, y_train)
helpers.run_OLS(X=X_train[list(selected_features_sfs.k_feature_names_)], y=y_train)

