from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
from sklearn.preprocessing import PolynomialFeatures

### PLOTTING #######################################################################################

def plot_mean_time_series(df, plot, variables, freq="Q"):
    if plot:
        by_logg_datum = df.groupby(pd.Grouper(key="logg-datum", freq=freq))
        for var in variables:
            plt.title(var)
            plt.plot(by_logg_datum[var].mean())
            plt.xlim(left=pd.to_datetime('2021-08-12'))
            plt.show()

def plot_pairplots(df, column_list, plot):
    sns.set_theme(style="ticks")
    if plot:
        for columns in column_list:
            sns.pairplot(df[columns]) 
            plt.show()

def plot_correlations(df, column_list, plot):
    if plot: 
        for columns in column_list:
            corr = df[columns].corr() # 2022-10-30: Renoveringsobjekt 0.6, läge2 -0.53, Hyresintäkt -0.23
            sns.heatmap(corr, cmap="RdBu_r", vmin=-1.0,vmax=1.0, annot=True, linewidths=0.1, linecolor="white")
            plt.show()

### DATA PREPARATION ###############################################################################
def feature_engineering(df):
    df["Yta_total"] = df["Boyta"] + df["Lokalyta"]
    df["Enheter_total"] = df["Antal lgh"] + df["Antal lokaler"]

def feature_transformation_and_imputation(df, variables, label):
    df = df.fillna(df.mean())
    poly = PolynomialFeatures(2)
    df_poly = pd.DataFrame(poly.fit_transform(df[variables]), columns=poly.get_feature_names_out(df[variables].columns))
    df_poly[label] = df[label].values
    df_poly.set_index(df.index, inplace=True)
    return df_poly

def tt_split(df, label, oot, oot_start_date, test_size, rand=42):
    columns = list(df.columns)
    columns.remove(label)
    if oot:
        X_train = df.loc[df.index <= '2023-02-01'][columns]
        y_train = df.loc[df.index <= '2023-02-01'][label]
        X_test = df.loc[df.index > '2023-02-01'][columns]
        y_test = df.loc[df.index > '2023-02-01'][label]
    else:
        X_train, X_test, y_train, y_test = train_test_split(df[columns],df[label],test_size=test_size, \
                                                            random_state=rand)
    print(f"\nTrain sample size: {len(X_train)}\nTest sample size: {len(X_test)}")
    return X_train, X_test, y_train, y_test

### MODELLING ######################################################################################
def run_OLS(X,y):
    regression_model = sm.OLS(endog=y, exog=sm.add_constant(X))
    regression_results = regression_model.fit()
    print(regression_results.summary())