############################################### 
# Real estate investment monte carlo simulation
###############################################

'''
TODO: Promisory note amortisation is not correct. I believe it is never ending...
'''


# Note: monthy tax payments have a conservative effect on cashflow compared to yeary tax calculations.
from os import replace
import random
import time
import numpy as np
import pandas as pd
import  matplotlib.pyplot as plt
from matplotlib import style
style.use('seaborn-whitegrid') #bmh ggplot classic seaborn-whitegrid seaborn-dark-palette
import configparser

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 

excel_path = r"C:\Users\jbuep\My Drive\Data\property investment stochastic simulation"
object = 'Bladingevagen 2 dec2022' # Objects: Stallängsvägen, Storgatan, Queckfeldtsgatan

# Read stats from configuration file
config = configparser.ConfigParser()
config.readfp(open(r'C:\Code\python\Own projects\Property investment simulator\property_config_ANSI.txt'))
price_building =        config.getint(object, 'price_building')
expenses_broker_amount= config.getint(object, 'expenses')
living_space =          config.getint(object, 'living_space')
income =                config.getint(object, 'income')
apartments_n =          config.getint(object, 'apartments_n')
commercial_space =      config.getint(object, 'commercial_space')
promissory_note =       config.getint(object, 'promissory_note')  # 7,5% promissory note on blädingev. 2 and 3 @ 9.5MSEK = 0.708 MSEK

#####################
# Simulation settings
#####################
# Continuous stochastic model: 1, stochastic stressed model:2 or deterministic model: 3
model_type              = 1
runs                    = 10
plot_series_n           = 10
months_n                = 180

#########################
## Real estate attributes
#########################
rent                    = (income / apartments_n) / 12
vacancy_rate            = 0.05 # Deterministic model
vacant_at_start_n       = 0
kvm_cost_living         = 402 # 402 kr är viktad kostnad för B2 vid 450kr bostad och 350 lokal
kvm_cost_commercial     = 350

#Adjustments
price_building          = price_building - 0
start_cash              = 200000
max_cash_amount         = 300000 

##########################################
# Emergency measures on/off and threshold
##########################################
emergency_measures      = 1
emergency_threshold     = 100000
expense_debt_payoff_threshold = 150000
emergency_expense_buffer = 0.1 # factor to increase broker's minimum expense to reflect realistic survival expenses.

########################
# Taxes and depreciation
########################
company_tax_rate        = 0.206
dividend_tax            = 0.20 # Det finns ingen speciell utdelningsskatt utan detta skattas i privat inkomstdeklaration
interest_deduction_rate = 0.3  #<---- Interest deduction calculation needs to be confirmed. Rate is 30%
depreciation_rate       = 0.02
depreciation_value      = 0.8 # ratio of real estate price that is depriciating

##################
### Loan settings 
##################
# Bank Loan specifics
interest_rate           = 0.02 # Margin to Riksbanken rate
amortization_rate       = 0.02
# Fixed term loan settings
interest_rate_fixed_n   = 0 # Fixed rate loan period
interest_rate_fixed     = 0.06
leverage_ratio          = 0.75 # Company finances Blädingev. 2 & 3 at 75%, Blädingevägen 4 at 70% =   (10 762 000 loan / 14 750 000 value) = 0.7296 lvt / lverage ratio
loan_initial_amount     = price_building * leverage_ratio
investment_capital      = price_building * (1-leverage_ratio)

# Promisory note settings
loan_pn_initial_amount  = promissory_note
interest_rate_pn        = 0.05 # Margin to Riksbanken rate
amortization_rate_pn    = 1/2 

###################
# Scenario settings
###################
expenses_standardized_amount = (living_space*kvm_cost_living) + (commercial_space*kvm_cost_commercial)

# probabilities of occupied apartment becoming empty and empty apartment being filled
# 3 year expected rent period: p = 0.028
# 50% fillrate = 5% vacancy
# 25% fillrate = 10% vacancy
p_monthly_vacancy       = 0.0278
p_monthly_fill          = 0.5

# Market rate assumptions
interest_rate_freeze_n  = 3 # Number of months until mkt rate can change
interest_rate_change_p  = 0.29
interest_rate_shift     = 0.07 # Upper limit for interest rate in stressed scenario
riksbanken_rate         = 0.025 # Riksbanken start rate
riksbanken_rate_floor   = 0.00
riksbanken_rate_cap     = 0.05
rent_cost_increase      = 0.02 #Rate at which rents and costs increase yearly. Inflation rate

# Deterministic model adjustments
if model_type == 3:
    rent = rent * (1-vacancy_rate)
    p_monthly_fill = 1
    p_monthly_vacancy = 0.00
    interest_rate_change_p = 0.0

####### LOOP OVER TIME #########

def mkt_rate(type, rate, m):
    # Determine market rate: Continous stochastic
    r = random.uniform(0,1)
    if type in (1,3):
        if (m > interest_rate_freeze_n) and (m % 2 == 0):
            if r <= interest_rate_change_p:
                rate_change = random.choice([-0.0025, 0.005])
            else:
                rate_change = 0
            if (rate <= riksbanken_rate_floor) and (rate_change < 0):
                rate = rate
            elif (rate >= riksbanken_rate_cap) and (rate_change > 0):
                rate = rate
            else:
                rate += rate_change

    elif type == 2:
    # Determine market rate: Stressed scenario with drift towards rate sihft
        if (m > interest_rate_freeze_n) and (m % 2 == 0) and (rate < (interest_rate_shift-interest_rate)):
            if r <= interest_rate_change_p + 0.05:
                rate_change = 0.005
            else:
                rate_change = 0
            if (rate <= riksbanken_rate_floor) and (rate_change < 0):
                rate = rate
            else:
                rate += rate_change
    return rate

def random_sequence(riksbanken_rate, cash_amount, loan_initial_amount,loan_pn_initial_amount, expenses_standardized_amount, expenses_broker_amount):
    df = pd.DataFrame()
    expense_debt            = 0
    loan_amount_updated     = loan_initial_amount
    loan_amount_pn_updated  = loan_pn_initial_amount
    rent_yearly             = rent
    net_cashflow_cum        = 0
    depreciation_amount_monthly = (depreciation_value * price_building * depreciation_rate) / 12
    # Create dictionary of apartments with value for how many months they will be 
    # vacant at time t
    apartment_status = {}
    for i in range(1,apartments_n+1):
        if i <= vacant_at_start_n:
            apartment_status["Apartment {}".format(i)] = 0
        else:
            apartment_status["Apartment {}".format(i)] = 1

    for i in range(1,months_n+1):
        # Determine vacancy status
        for j in apartment_status:
            r = random.uniform(0,1)
            if (apartment_status[j] == 1) and (r <= p_monthly_vacancy):
                apartment_status[j] = 0
            elif (apartment_status[j] == 0) and (r <= p_monthly_fill):
                apartment_status[j] = 1

        ##########################
        # Amortize and update loan
        ##########################
        amortization_amount = (loan_initial_amount * amortization_rate) / 12
        loan_amount_updated -= amortization_amount

        amortization_amount_pn = max(0 , (loan_pn_initial_amount * amortization_rate_pn) / 12)
        loan_amount_pn_updated = max(0, (loan_amount_pn_updated - amortization_amount_pn))

        ################
        # Interest rates
        ################
        # Call on mkt_rate function to determine riksbanken_rate
        riksbanken_rate = mkt_rate(model_type, rate=riksbanken_rate, m=i)

        # Determine loan interest rate based on riksbanken rate
        if i > interest_rate_fixed_n:
            interest_rate_month = riksbanken_rate + interest_rate #Riksbanken_rate can be positive and negative
        else:
            interest_rate_month = interest_rate_fixed
        # Permissory Note - does not have a fixed period option
        interest_rate_pn_month = riksbanken_rate + interest_rate_pn

        occupied_flats_n = sum(1 for value  in apartment_status.values() if value == 1)

        # Determine expenses - if it is an emergency, expenses drop to minimum monthly expenses + x%
        if (emergency_measures == 1) and (cash_amount < emergency_threshold):
            expenses_amount_monthly = (expenses_broker_amount / 12) * (1+emergency_expense_buffer)
            expense_debt += (expenses_standardized_amount / 12) - expenses_amount_monthly
        else:
            expenses_amount_monthly = expenses_standardized_amount / 12

        # Pay off expense debt
        if (cash_amount > expense_debt_payoff_threshold) and (expense_debt > 1):
            expenses_amount_monthly += (expenses_standardized_amount / 12) - (expenses_broker_amount / 12)
            expense_debt -= (expenses_standardized_amount / 12) - (expenses_broker_amount / 12)

        # Get the actual revenue and interest cost
        interest_amount = (loan_amount_updated * interest_rate_month) / 12
        interest_amount_pn = max(0, (loan_amount_pn_updated * interest_rate_pn_month)/12 )
        # Determine interest deduction from profit
        interest_deduction = interest_amount * interest_deduction_rate
                
        if i % 12 == 0:
            rent_yearly                     = rent_yearly * (1+rent_cost_increase)
            expenses_standardized_amount    = expenses_standardized_amount * (1+rent_cost_increase)
            expenses_broker_amount          = expenses_broker_amount * (1+rent_cost_increase)
        revenue_gross = (rent_yearly * occupied_flats_n)

        # Calculate yearly tax, interest and amortization
        EBT = revenue_gross - expenses_amount_monthly - interest_amount - interest_amount_pn - depreciation_amount_monthly
        tax_payment = max((EBT-interest_deduction) * company_tax_rate,0)
        cash_out = expenses_amount_monthly + interest_amount + interest_amount_pn + amortization_amount + amortization_amount_pn + tax_payment
        net_cashflow =  revenue_gross - cash_out

        if i % 12 == 1:
            EBT_yearly = EBT
            tax_payment_yearly = 0
            amortization_yearly = amortization_amount
            profit = 0
            net_cashflow_cum = net_cashflow
        elif i % 12 == 0:
            EBT_yearly += EBT
            amortization_yearly += amortization_amount
            net_cashflow_cum += net_cashflow

            if EBT_yearly > 0:
                tax_payment_yearly = ((EBT_yearly) * (company_tax_rate))
            else:
                tax_payment_yearly = 0
            profit = EBT_yearly - tax_payment_yearly
        else:
            EBT_yearly += EBT
            amortization_yearly += amortization_amount
            tax_payment_yearly = 0
            profit = 0
            net_cashflow_cum += net_cashflow
        
        if max_cash_amount == 0:
            cash_amount = cash_amount + revenue_gross - cash_out
        else:
            cash_amount = min(cash_amount + revenue_gross - cash_out, max_cash_amount)

        loan_to_equity = loan_amount_updated / price_building

        # Add information to the dataframe
        other_statistics = {"Month": i
            ,"Outstanding_Loan": round(loan_amount_updated) 
            ,"Outstanding_pn_loan": round(loan_amount_pn_updated)
            ,"Loan_to_equity": loan_to_equity
            ,"Occupied_flats": occupied_flats_n 
            ,"Interest_r": interest_rate_month 
            ,"Interest_paym": round(interest_amount) 
            ,"Interest_paym_pn": round(interest_amount_pn)
            ,"Interest_deduction": round(interest_deduction)
            ,"Expenses": round(expenses_amount_monthly)
            ,"Expense_deffered": round(expense_debt)
            ,"Amort_amt": round(amortization_amount) 
            ,"Amort_amt_pn": round(amortization_amount_pn)
            ,"depreciation" : round(depreciation_amount_monthly)
            ,"Tax_paym": round(tax_payment)
            ,"Yearly_tax": round(tax_payment_yearly)
            ,"Cash_in": round(revenue_gross) 
            ,"Rent adjusted": round(rent_yearly)
            ,"Cash_out": round(cash_out)
            ,"Cost standard": round(expenses_standardized_amount)
            ,"Cost broker": round(expenses_broker_amount)
            ,"Cash_net": round(net_cashflow)
            ,"Cash_net_cum": round(net_cashflow_cum)
            ,"Cash_amt" : round(cash_amount)
            ,"Profit" : round(profit)
            }

        apartment_status_df = pd.DataFrame(apartment_status, index=[0])
        other_statistics_df = pd.DataFrame(other_statistics, index=[0])
        df_tmp = apartment_status_df.join(other_statistics_df)
        df = df.append(df_tmp)
    return df

d = {}
d_defaulted = {}
tic = time.perf_counter()
for i in range(runs):
    d[i] = random_sequence(riksbanken_rate, start_cash, loan_initial_amount,loan_pn_initial_amount, expenses_standardized_amount=expenses_standardized_amount, expenses_broker_amount=expenses_broker_amount)
    if d[i]["Cash_amt"].min() < 0:
        d_defaulted[i] = d[i] #Collect cases that defaulted

d[0].to_excel(excel_path + r'\first_run.xlsx', index = False)

print("Number of defaulted cases: " + str(len(d_defaulted)))
writer = pd.ExcelWriter(excel_path + r'\defaulted.xlsx', engine='xlsxwriter')
for sheet, frame in  d_defaulted.items():
    frame.to_excel(writer, sheet_name = "run_" + str(sheet))
writer.save()
toc = time.perf_counter()

# Create relevant lists for further calculations
interest_r_list = []
for i in range(len(d[0])):
    interest_r_sub_list = []
    for j in range(len(d)):
        interest_r_sub_list.append(d[j].Interest_r.values[i])
        avg_rate = sum(interest_r_sub_list) / len(interest_r_sub_list)
        rate_25th_perc,rate_50th_perc,rate_75th_perc = [i for i in np.percentile(interest_r_sub_list, [25,50,75])]
    interest_r_list.append((avg_rate, rate_25th_perc,rate_50th_perc,rate_75th_perc))
interest_r_df = pd.DataFrame(interest_r_list)


for i in range(min(len(d), plot_series_n)):
    plt.plot(d[i].Month, d[i].Cash_amt, alpha=1,)
    plt.ticklabel_format(style="plain")
    plt.axhline(y=0, color='k', linestyle='solid', linewidth=1)
    plt.axhline(y=emergency_threshold, color='k', linestyle='dotted', linewidth=1)
    plt.axhline(y=expense_debt_payoff_threshold, color='k', linestyle='dotted', linewidth=1)
    plt.ylim(top=400000, bottom=0)
    plt.xlim(left=0, right=180)
    plt.title("Cash balance")
plt.show()

for i in range(min(len(d), plot_series_n)):
    plt.plot(d[i].Month, d[i].Interest_r, alpha=0.1)
plt.plot(interest_r_df[0], alpha=1, label="Average rate")
plt.xlim(left=0, right=180)
#plt.ylim(top=interest_rate_shift+0.005)
plt.legend()
plt.title("Interest rate")
plt.ticklabel_format(style="plain")
plt.show()


# Caculate yearly averages
df_y = pd.DataFrame()
df_y["Year"] = d[0].loc[d[0].Profit != 0].Month / 12
for i in d:
    df_y["Profit_r{}".format(i)] = d[i].loc[d[i].Profit != 0].Profit
    df_y["Cash_net_cum_r{}".format(i)] = d[i].loc[d[i].Profit != 0].Cash_net_cum
    df_y["Equity"] = (price_building - d[i].loc[d[i].Profit != 0].Outstanding_Loan)
    df_y["ROE_r{}".format(i)] = d[i].loc[d[i].Profit != 0].Profit / (price_building - d[i].loc[d[i].Profit != 0].Outstanding_Loan)
    df_y["ROI_r{}".format(i)] = d[i].loc[d[i].Profit != 0].Profit / investment_capital
    df_y["Expenses_r{}".format(i)] = d[i].loc[d[i].Profit != 0].Expenses

df_y["Free_cashflow_avg"] = df_y[df_y.filter(like='Cash_net_cum_r').columns].mean(axis=1)
df_y["Free_cashflow_stddev"] = df_y[df_y.filter(like='Cash_net_cum_r').columns].std(axis=1)
df_y["Profit_avg"] = df_y[df_y.filter(like='Profit_r').columns].mean(axis=1)
df_y["Profit_stdev"] = df_y[df_y.filter(like='Profit_r').columns].std(axis=1)
df_y["Profit_lcl"] =    df_y.Profit_avg - (df_y.Profit_stdev *2)
df_y["Profit_ucl"] =    df_y.Profit_avg + (df_y.Profit_stdev *2)
df_y["Expenses_avg"] =  df_y[df_y.filter(like='Expenses_r').columns].mean(axis=1)
df_y["ROE_avg"] =       df_y[df_y.filter(like='ROE_r').columns].mean(axis=1)
df_y["ROI_avg"] =       df_y[df_y.filter(like='ROI_r').columns].mean(axis=1)

# fig, ax = plt.subplots()
# ax.plot(df_y.Year,df_y.Profit_avg)
# ax.fill_between(df_y.Year, (df_y.Profit_lcl), (df_y.Profit_ucl), color='b', alpha=.1)
# plt.plot(df_y.Year, df_y[df_y.filter(like='Profit_r').columns], alpha=0.2)
# plt.ylim(bottom=0)
# plt.xlim(left=0)
# plt.title("Yeary profits")
# plt.show()

# plt.plot(df_y.Year, df_y[df_y.filter(like='ROE_avg').columns], label='ROE', alpha=1)
# plt.plot(df_y.Year, df_y[df_y.filter(like='ROI_avg').columns], label='ROI', alpha=1)
# plt.legend()
# plt.ylim(bottom=0)
# plt.xlim(left=0)
# plt.show()

failed_n = 0
for i in d:
    if min(d[i].Cash_amt) < 0:
        failed_n += 1
failed_perc = (failed_n / len(d)) * 100

# Calculate real vacancy rate
occupancy_means = []
for i in d:
    occupancy_means.append(d[i].Occupied_flats.mean())

occupancy_means_arr = np.array(occupancy_means)
occupancy_mean = np.mean(occupancy_means_arr)
occupancy_std = np.std(occupancy_means_arr)
occupancy_rate_real = occupancy_mean / apartments_n
occupancy_rate_real_llim = (occupancy_mean - (occupancy_std*2)) / apartments_n
occupancy_rate_real_ulim = (occupancy_mean + (occupancy_std*2)) / apartments_n



print("\n"*2)
print("Initial Loan:", round(loan_initial_amount))
print("Equity:", round(investment_capital))
print("Broker cost/living space", round(expenses_broker_amount/living_space))


print("{}% of cases failed to meet interest payments".format(round(failed_perc, 1)))
print("Average occupancy rate {}%".format(round(occupancy_rate_real*100,1)))
print("95% of cases had an average occupancy rate between {}% and {}%".format(round(occupancy_rate_real_llim*100, 1), round(occupancy_rate_real_ulim*100,1)))
print("Simulation executed in {} seconds".format(toc - tic))
print("\n"*1)


print("End of program")

df_y.to_excel(excel_path + r'\yearly_stats.xlsx', index = False)


