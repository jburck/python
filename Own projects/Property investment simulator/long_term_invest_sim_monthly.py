# Programmer:        Johan Burck
# Description:       Second version of simluation for longer term investment decisions
# Development start: 2021-06-20

'''
2023-01-18 
1. Removed erroneous assumption that profit would be reduced by a "interest rate deduction"
2. Put expense calculation into separate function and corrected a defect
'''

# Idéer: 1. Funktion som räknar ut beskattning för fåmansbolag. Huvudregel samt Förenklingsregeln - https://www.foretagarna.se/juridisk-faq/skatter/utdelning-enligt-famansforetagsreglerna/
# TODO: 1. Kontrollräkna vinst m.m. Det verkar inte stämma 2023-01-18

import pandas as pd
import numpy as np
import time
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from scipy.interpolate import griddata
from pandas import ExcelWriter 
import configparser

# Read stats from configuration file
# Objects: Stallängsvägen, Storgatan, Queckfeldtsgatan 31, Queckfeldtsgatan 92, Blädingevägen 4
config = configparser.ConfigParser()
object = "Bladingevagen 4"

config.readfp(open(r'C:\Code\python\Own projects\Property investment simulator\property_config_ANSI.txt'))
property_price_tmp =    config.getint(object, 'price_building')
kvm =               config.getint(object, 'living_space')
income_i =          config.getint(object, 'income')

excel_path = "C:\Code\python\Own projects\Property investment simulator" #Output

property_price               = property_price_tmp
n_months                     = 240
amortization_rate            = 0.02 # Higher than bank amortization rate can be defended by accounting for promisory note terms
interest_rate                = 0.045

# TAXES
skatt                        = 0.206
income_tax                   = 0.31
state_inc_tax_1_tier         = 0.2
dividend_tax_r               = 0.20

interest_deduction_rate      = 0.30
expenses_kvm                 = [525] 
expenses_kvm_steady          = 550 # 500kr/kvm = Bankschablon 2022/2023
vacancy_rate                 = 0.03 # Citysweden 2022 had vacancy rate of roughly 1,5% (not weighted by income)
inflation_rate               = 0.025 # Used to adjust income and expenses
appreciation_ratio           = 0.75 # ratio of how much of the property appreciation that can fund next property
depreciation                 = 0.016 # depreciation of book value of property (not land) 2% * 80% property to land ratio

inflation_r_monthly         = ((1 + inflation_rate) ** (1/12)) - 1
amortization_r_monthly      = ((1 + amortization_rate) ** (1/12)) - 1
interest_r_monthly          = ((1 + interest_rate) ** (1/12)) - 1
income_monthly               = income_i / 12

inkomstbasbelopp            = 68200 # 68200 för 2021
grundavdrag                 = 25000 # 0 % skatt <----- nivå beror på inkomst
state_income_tax_threhsold  = 443200 # första nivån https://sv.wikipedia.org/wiki/Skatt_i_Sverige

expenditure_rate             = 0.0225 # % of price that is paid as expenditures (0.0425 + 0,0025)/2 every other is sold as a company
expenditure_amount           = expenditure_rate * property_price


# Parameters to analyze
leverage                    = [0.7]
capital_injection_months    = [[3, 12]]
capital_injection_extra     = [[1000000,1000000]]
capital_injection_monthly   = [0]
capital_injection_m_start   = 1
capital_injection_m_end     = [36]

fixed_extraction_month      = "y" # y/n
capital_extraction_m_start  = 120
capital_extraction_fix      = 0.95      # Either rate or sum. If sum, then it will be adjusted for inflation. Will be extracted at stated start-month above.
''' capital_extraction_nofix:
    Extraction starts when this amount can be extracted from the monthly profit * the extraction fix rate above. 
    Svensk medianlön 2021: 32400 (efter skatt: 24800) motsvarar ca 81000 capital extraction (div*2)/(1-t)
    Nettolön på 40k = 57000k brutto, 142500 '''
capital_extraction_nofix    = [81000]     # change to net monthly salary per person

runs = len(leverage) * len(capital_injection_months) * len(expenses_kvm) * len(capital_injection_m_end) * len(capital_extraction_nofix)
runtime = runs * 8.6 #roughly the time for one 18month run
print("Estimated runtime is {} seconds".format(round(runtime))) 

def dividend_tax_calc(dividend, index):
    state_income_tax_threhsold_month = state_income_tax_threhsold  / 12 * (index/100)
    # Förenklingsregeln - utan sparat utdelningsutrymme
    tax_on_capital_simplified       = ( (inkomstbasbelopp/2) * (index/100) * 2.75 * dividend_tax_r) /12
    tax_on_income_simplified        = max(0, (((dividend) - (inkomstbasbelopp/2 * (index/100) * 2.75 / 12) - (grundavdrag * (index/100) / 12) - max(0, (dividend-state_income_tax_threhsold_month))) * income_tax))
    state_tax_on_income_simplified  = max(0, ((dividend)-state_income_tax_threhsold_month)) * (income_tax + state_inc_tax_1_tier)
    tax_simplified = tax_on_capital_simplified + tax_on_income_simplified + state_tax_on_income_simplified
    return tax_on_capital_simplified, tax_on_income_simplified, state_tax_on_income_simplified, tax_simplified

def calc_expenses(kvm, cost_kvm, cost_kvm_steady, houses_n, index, steady_state):
    if steady_state:
        expenses_per_house_monthly = (kvm * cost_kvm_steady) / 12
    else:
        expenses_per_house_monthly = (kvm * cost_kvm) / 12
    return (expenses_per_house_monthly * houses_n) * (index/100)

def simulation(leverage, capital_injection_extra, capital_injection_months,capital_injection_monthly, capital_injection_m_end, expenses_kvm, capital_extraction_nofix, depreciation):
    to_append= []
    capital_injection_m_end_setting = capital_injection_m_end
    steady_state = False
    for i in range(n_months+1):
        capital_injection = 0
        if i in capital_injection_months:
            index_month = capital_injection_months.index(i)
            capital_injection += capital_injection_extra[index_month]
        if i != 0 and capital_injection_m_start <= i <= capital_injection_m_end:
            capital_injection += capital_injection_monthly

        if i == 0:
            index = 100
            cash            = 0
            amortization    = 0
            interest_payment = 0
            profit          = 0
            profit_cum      = 0
            interest_deduction = 0
            capital_extraction_amt = 0
            dividend_pp            = 0
            dividend_pp_nvp        = 0
            dividend_max_nvp       = 0
            net_salary_pp_npv      = 0
            tax_on_capital_simplified = 0
            tax_on_income_simplified = 0
            state_tax_on_income_simplified = 0
            expenses_total_monthly = calc_expenses(kvm=kvm, cost_kvm=expenses_kvm, cost_kvm_steady=expenses_kvm_steady, houses_n=1, index=index, steady_state=steady_state)
            income          = income_monthly * (1-vacancy_rate)
            loan            = property_price * leverage
            assets          = property_price
            property_value  = property_price
            equity_property = property_price * (1-leverage)
            equity_total    = property_price * (1-leverage) + cash
            capital_injection_m_end = capital_injection_m_end
            excess_equity   =   0
            investment_capability_leveraged = 0
            houses_n        =   1
            houses_n_theoretical = 1
            appreciation    = 0
            
        else: #If not month = 0
            index           = index * (1+inflation_r_monthly)
            expenses_total_monthly = calc_expenses(kvm=kvm, cost_kvm=expenses_kvm, cost_kvm_steady=expenses_kvm_steady, houses_n=1, index=index, steady_state=steady_state)
            if investment_capability_leveraged >= (property_price + (expenditure_amount/ (1-leverage) )): #If we invest
                houses_n += 1
                equity_contribution = (equity_property + appreciation - (property_value * (1-leverage)))  / (1-leverage)
                cash_reduction = (property_price - equity_contribution) * (1-leverage)
                income      = (income_monthly * (1-vacancy_rate) *houses_n )  * (index/100)
                property_value = property_value + property_price + appreciation # Add accumulated appreciation to the new property price 
                appreciation = property_value * inflation_r_monthly  # Reset appreciation
                loan        = + (property_value * leverage) - amortization
                interest_payment = ((loan + (loan - ((property_value * leverage) * amortization_r_monthly))) / 2) * interest_r_monthly
                cash        = cash - cash_reduction - expenditure_amount + (((income + interest_deduction - expenses_total_monthly - interest_payment)*(1-skatt))-amortization) + capital_injection - capital_extraction_amt
            else: #If no investment
                interest_payment = ((loan + (loan - ((property_value * leverage) * amortization_r_monthly))) / 2) * interest_r_monthly
                income      = (income_monthly * (1-vacancy_rate) *houses_n )  * (index/100)
                appreciation = appreciation + (property_value * inflation_r_monthly * appreciation_ratio)
                loan        = loan - amortization
                cash        = cash + (((income + interest_deduction - expenses_total_monthly - interest_payment)*(1-skatt))-amortization) + capital_injection - capital_extraction_amt
            
            if fixed_extraction_month == "y":
                if i >= capital_extraction_m_start:
                    amortization = 0
                    depreciation = 0
                    steady_state = True
                    if 0 <= capital_extraction_fix <=1:
                        capital_extraction_amt = profit * capital_extraction_fix
                    else:
                        capital_extraction_amt = capital_extraction_fix * (index/100)
                else:
                    amortization    = (property_value * leverage) * amortization_r_monthly
            elif fixed_extraction_month == "n":
                if (profit * capital_extraction_fix) >= (capital_extraction_nofix * (index/100)):
                    steady_state = True
                    capital_injection_m_end = i 
                    amortization = 0
                    depreciation = 0
                    capital_extraction_amt = (capital_extraction_nofix * (index/100))
                else:
                    amortization    = (property_value * leverage) * amortization_r_monthly
            
            profit              = (income - expenses_total_monthly - (depreciation * property_value)- interest_payment)*(1-skatt)
            profit_cum          = profit_cum + profit
            dividend_pp         = (capital_extraction_amt * (1-dividend_tax_r)) / 2
            dividend_pp_nvp     = dividend_pp / (index/100)
            dividend_max_nvp    = (profit * (1-dividend_tax_r) /2) / (index/100)

            tax_on_capital_simplified, tax_on_income_simplified, state_tax_on_income_simplified, simplified_tax = dividend_tax_calc(dividend_pp, index)
            net_salary_pp       = dividend_pp - (simplified_tax / 2)
            net_salary_pp_npv   = max(net_salary_pp / (index/100), 0)

            equity_property     =   property_value - loan  
            assets              = property_value + cash
            equity_total        = assets - loan
            excess_equity       = equity_total - (property_value * (1-leverage))
            investment_capability_leveraged = ((excess_equity) / (1-leverage)) + (appreciation / (1-leverage))
            houses_n_theoretical = houses_n + (investment_capability_leveraged / (property_price*(index/100)))

        to_append.append([i,                   round(cash),                               round(property_value),                  appreciation,               loan,                        
                        capital_injection,    round(income),                  round(expenses_total_monthly),                        round(interest_payment), round(amortization),   round(profit),                      
                        round(houses_n),            round(investment_capability_leveraged) ,        round(interest_deduction),
                        round(capital_extraction_amt) , round(dividend_pp),  round(dividend_pp_nvp), round(net_salary_pp_npv),
                        round(tax_on_capital_simplified), round(tax_on_income_simplified), round(state_tax_on_income_simplified),
                        capital_injection_monthly, houses_n_theoretical, index, leverage, expenses_kvm, capital_extraction_nofix, capital_injection_months,capital_injection_m_end_setting  ])
    df = pd.DataFrame.from_records(to_append, columns=[
                        "Month",     "Cash" ,               "Property_value",               "Appreciation",             "Loan",                     
                        "Capital_injection",        "Income",                       "Expenses",                     "Interest_amount", "amortization",          "Profit",                    
                        "houses_n",                 "Invest_cap_leveraged",                      "Interest deduction", 
                        "Capital_extraction_amount" , "Dividend_pp",  "Dividend_pp_nvp", "net_salary_pp_npv", "Capital_tax", "Income tax", "State tax",
                          "4.cap. inj. monthly am.", "HOUSES_N_THEORETICAL", "index", "1.Leverage", "2.Expenses/kvm", "3.capital_extraction_nofix", "5.cap. inj. structure"
                        , "6.cap. inj. month end"])
    return df

tic = time.perf_counter()
df_results = pd.DataFrame()
df_results_all = pd.DataFrame()
w = ExcelWriter(excel_path +'\output.xlsx')
w = ExcelWriter(excel_path +'\output_all.xlsx')

for lev_i in leverage:
    for month,extra in zip(capital_injection_months,capital_injection_extra):
        for inject_monthly_i in capital_injection_monthly:
            for exp in expenses_kvm:
                for extraction in capital_extraction_nofix: #loop over capital extraction amounts
                    for m_end in capital_injection_m_end:
                        df = simulation(lev_i, extra, month,inject_monthly_i, m_end, exp, extraction, depreciation=depreciation)
                        df_results =  df_results.append(df.iloc[-1])
                        df_results_all =  df_results_all.append(df)
                        df_results.to_excel(excel_path + "\output.xlsx",index=False)
                        #df.to_excel(excel_path + "\df_lev{lev}_capinj{capinj}_capy{capy}_Y{y}.xlsx".format(lev= lev_i, capinj=inject_i,capy=inject_monthly_i,y=year_i),index=False)
                        df_results_all.to_excel(excel_path + "\output_all.xlsx",index=False)
        # print(df_results[["Cap_inj_control","capital_injection_monthly","Leverage",,"houses_n_theoretical"]].tail(50))
toc = time.perf_counter()

print("Done!")
print("Simulation executed in {} seconds".format(toc - tic))

######################################################################################################
# PLOTTING                                  
######################################################################################################

# Target variable is the theoretical number of houses Z, but X and Y can be changed in the response plane plot
# "Cap_inj_control" , "capital_injection_monthly"
var_X = "Leverage"
var_Y = "capital_injection_monthly"
var_Z = "Cap_inj_control" #Steady_salary_monthly, houses_n_theoretical

#Plot the respnse surface
# X, Y = np.meshgrid(df_results[var_X].values, df_results[var_Y].values)
# Z = df_results[var_Z].values

# x1 = np.linspace(df_results[var_X].min(), df_results[var_X].max(), len(df_results[var_X].unique()))
# y1 = np.linspace(df_results[var_Y].min(), df_results[var_Y].max(), len(df_results[var_Y].unique()))
# x2, y2 = np.meshgrid(x1, y1)
# z2 = griddata((df_results[var_X], df_results[var_Y]), df_results[var_Z], (x2, y2), method='cubic')

# fig = plt.figure()
# ax = fig.gca(projection='3d')
# surf = ax.plot_surface(x2, y2, z2, rstride=1, cstride=1, cmap=cm.seismic,
#     linewidth=0, antialiased=False)
# ax.set_zlim(auto=True)
# ax.zaxis.set_major_locator(LinearLocator(10))
# ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
# fig.colorbar(surf, shrink=0.5, aspect=5)
# plt.title(var_X + " " + var_Y)
# plt.show()
