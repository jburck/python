#%%
import pandas as pd
import numpy as np
from sklearn import tree
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import cross_val_score

target = "houses_n_theoretical"
features = ["Cap_inj_control", "Leverage", "Year"]

#%%
df = pd.read_excel("C:\Datasets\long_term_invest\output.xlsx")

Y = df[target]
X = df[features]
# %%
clf_tree = DecisionTreeRegressor(max_depth=3, criterion="mae")
clf_tree.fit(X, Y)
scores = cross_val_score(clf_tree,  X, Y, scoring="neg_mean_squared_error",cv=3)
scores

# %%
example = [360000, 0.5, 5]
example_shape = np.reshape(example, (1, -1))
scored_example = clf_tree.apply(example_shape)

# %%
tree.plot_tree(clf_tree, feature_names=["Capital injection", "Leverage", "Years"], rounded=True)

# %%
print(clf_tree.feature_importances_)
# %%
