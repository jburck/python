from tkinter import *
objects_n = 23

top = Tk()

L0 = Label(top, text="Objekt")  
L0.pack( side = TOP)
E0 = Entry(top, bd =3)
E0.insert(0,"Queckfeldtsgatan 31")
E0.pack(side = TOP)

L1 = Label(top, text="property_price_adjustment")
L1.pack( side = TOP)
E1 = Entry(top, bd =3)
E1.insert(0,0)
E1.pack(side = TOP)

L2 = Label(top, text="n_months")
L2.pack( side = TOP)
E2 = Entry(top, bd =3)
E2.insert(0,180)
E2.pack(side = TOP)

L3 = Label(top, text="amortization_rate")
L3.pack( side = TOP)
E3 = Entry(top, bd =3)
E3.insert(0,0.0333)
E3.pack(side = TOP)

L4 = Label(top, text="interest_rate")
L4.pack( side = TOP)
E4 = Entry(top, bd =3)
E4.insert(0,0.02)
E4.pack(side = TOP)

L8 = Label(top, text="expenses_kvm")
L8.pack( side = TOP)
E8 = Entry(top, bd =3)
E8.insert(0,450)
E8.pack(side = TOP)

L9 = Label(top, text="expenses_kvm_steady")
L9.pack( side = TOP)
E9 = Entry(top, bd =3)
E9.insert(0,425)
E9.pack(side = TOP)

L10 = Label(top, text="vacancy_rate")
L10.pack( side = TOP)
E10 = Entry(top, bd =3)
E10.insert(0,0.05)
E10.pack(side = TOP)

L11 = Label(top, text="inflation_rate")
L11.pack( side = TOP)
E11 = Entry(top, bd =3)
E11.insert(0,0.01)
E11.pack(side = TOP)

L12 = Label(top, text="leverage")
L12.pack( side = TOP)
E12 = Entry(top, bd =3)
E12.insert(0,0.70)
E12.pack(side = TOP)

L13 = Label(top, text="capital_injection_months")
L13.pack( side = TOP)
E13 = Entry(top, bd =3)
E13.insert(0,[11,12,36])
E13.pack(side = TOP)

L14 = Label(top, text="capital_injection")
L14.pack( side = TOP)
E14 = Entry(top, bd =3)
E14.insert(0, 500000)
E14.pack(side = TOP)

L16 = Label(top, text="capital_injection_monthly")
L16.pack( side = TOP)
E16 = Entry(top, bd =3)
E16.insert(0, 20000)
E16.pack(side = TOP)

L17 = Label(top, text="capital_injection_m_start")
L17.pack( side = TOP)
E17 = Entry(top, bd =3)
E17.insert(0, 37)
E17.pack(side = TOP)

L18 = Label(top, text="capital_injection_m_end")
L18.pack( side = TOP)
E18 = Entry(top, bd =3)
E18.insert(0, 120)
E18.pack(side = TOP)

L19 = Label(top, text="fixed_extraction_month")
L19.pack( side = TOP)
E19 = Entry(top, bd =3)
E19.insert(0, "y")
E19.pack(side = TOP)

L20 = Label(top, text="capital_extraction_m_start")
L20.pack( side = TOP)
E20 = Entry(top, bd =3)
E20.insert(0, 120)
E20.pack(side = TOP)

L21 = Label(top, text="capital_extraction_fix")
L21.pack( side = TOP)
E21 = Entry(top, bd =3)
E21.insert(0, 0.5)
E21.pack(side = TOP)

L22 = Label(top, text="capital_extraction_nofix")
L22.pack( side = TOP)
E22 = Entry(top, bd =3)
E22.insert(0, 62000)
E22.pack(side = TOP)

button = Button(
    text="Run!",
    width=15,
    height=5,
    bg="blue",
    fg="yellow",
)
button.pack(side=RIGHT)


top.mainloop()