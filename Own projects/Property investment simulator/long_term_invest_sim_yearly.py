# Programmer:        Johan Burck
# Description:       Second version of simluation for longer term investment decisions
# Development start: 2021-06-20

# Objekt: https://beskrivning.fasad.eu/objekt/1523763/beskrivning.html

import pandas as pd
import numpy as np

from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from scipy.interpolate import griddata
from pandas import ExcelWriter 

excel_path = "C:\Datasets\long_term_invest"

n_years                      = [10]
property_price               = 4500000
amortization_rate            = 0.033
interest_rate                = 0.02
skatt                        = 0.206
skatt_utd                    = 0.20
income_i                     = 418900
expenses_i                   = 123200 #(308 kvm @ 400 kr/kvm/år)
kvm                          = 308
expenses_kvm_steady          = 400 # Should NOT be compounded here
vacancy_rate                 = 0.05
inflation_rate               = 0.01

# Capital injection
capital_injection_annual    = 0

# Parameters to analyze
leverage                    = [0.7]
capital_injection_years     = np.array([1,2,3])
capital_injection           = [1830000]
cap_inj_year_adj            = [0]
capital_injection_yearly    = [240000,360000,480000,540000]
# If injection year = 5 then adj of 4 equals injection first year

# Target variable is the theoretical number of houses Z, but X and Y can be changed in the response plane plot
# "Cap_inj_control" , "cap_inj_year_adj", "capital_injection_yearly"
var_X = "Leverage"
var_Y = "capital_injection_yearly"
var_Z = "Cap_inj_control" #Steady_salary_monthly, houses_n_theoretical

def simulation(n_years, leverage, capital_injection, capital_injection_years,capital_injection_yearly, cap_inj_year_adj):
    to_append= []
    capital_injection_years = capital_injection_years - cap_inj_year_adj
    for i in range(n_years+1):
        if i in capital_injection_years:
            capital_injection_extra = capital_injection 
        elif i != 0:
            capital_injection_extra = capital_injection_yearly
        else:
            capital_injection_extra = 0
        if i == 0:
            cash            = 0
            amortization    = 0
            interest_payment = 0
            profit          = 0
            profit_cum      = 0
            expenses        = expenses_i
            income          = income_i * (1-vacancy_rate)
            loan            = property_price * leverage
            assets          = property_price
            property_value  = property_price
            equity_property = property_price * (1-leverage)
            equity_total = property_price * (1-leverage) + cash
            excess_equity   =   0
            investment_capability_leveraged = 0
            houses_n        =   1
            houses_n_theoretical = 1
            appreciation    = 0

            # Variables for steady state calculation
            steady_income   = 0
            steady_costs    = 0
            steady_interest = 0
            expenses_kvm_steady_adj = 0
            steady_profit   = 0
            steady_salary_monthly = 0
            steady_salary_monthly_disc = 0
        else: #If not year = 0

            if investment_capability_leveraged >= property_price: #If we invest
                equity_contribution = (equity_property + appreciation - (property_value * (1-leverage)))  / (1-leverage)
                cash_reduction = (property_price - equity_contribution) * (1-leverage)
                income      = (income + (income_i * (1-vacancy_rate))) * (1+inflation_rate)
                expenses    = (expenses + expenses_i) * (1+inflation_rate)

                property_value = property_value + property_price + appreciation # Add accumulated appreciation to the new property price 
                appreciation = property_value * inflation_rate                  # Reset appreciation

                loan        = + (property_value * leverage) - amortization
                interest_payment = ((loan + (loan - ((property_value * leverage) * amortization_rate))) / 2) * interest_rate
                cash        = cash - cash_reduction + (((income - expenses - interest_payment)*(1-skatt))-amortization) + capital_injection_annual + capital_injection_extra
            else: #If no investment
                interest_payment = ((loan + (loan - ((property_value * leverage) * amortization_rate))) / 2) * interest_rate
                income      = (income) * (1+inflation_rate)
                expenses    = (expenses) * (1+inflation_rate)
                appreciation = appreciation + (property_value * inflation_rate)
                loan        = loan - amortization
                cash        = cash + (((income - expenses - interest_payment)*(1-skatt))-amortization) + capital_injection_annual + capital_injection_extra


            amortization    = (property_value * leverage) * amortization_rate
            profit          = (income - expenses - interest_payment)*(1-skatt)
            profit_cum      = profit_cum + profit
            
            equity_property =   property_value - loan  
            
            assets          = property_value + cash
            equity_total    = assets - loan
            excess_equity   = equity_total - (property_value * (1-leverage))
            investment_capability_leveraged = ((excess_equity) / (1-leverage)) + (appreciation / (1-leverage))
            houses_n        = property_value / property_price
            houses_n_theoretical = (property_value + investment_capability_leveraged) / property_price
            
            if i == n_years:
                # Calculate long term revenue
                steady_income   = (income / houses_n) * houses_n_theoretical
                expenses_kvm_steady_adj = expenses_kvm_steady * (1+inflation_rate)**(n_years-1)                
                steady_costs    = kvm * houses_n_theoretical * expenses_kvm_steady_adj
                steady_interest = ((property_value + appreciation) * leverage) * interest_rate
                steady_profit   = (steady_income - steady_costs - steady_interest)*1-skatt
                steady_salary_monthly   = (steady_profit * (1-skatt_utd)) /2 /12
                steady_salary_monthly_disc = steady_salary_monthly/((1+inflation_rate)**(n_years-1))
        to_append.append([i,                        round(property_value),          appreciation,       loan,                       round(cash), 
                        capital_injection_extra,    round(income),                  round(expenses),    round(interest_payment),    round(profit), 
                        round(profit_cum),          leverage,                       capital_injection,  cap_inj_year_adj,           capital_injection_yearly, 
                        round(houses_n),            round(houses_n_theoretical,1),
                        round(investment_capability_leveraged) ,round(amortization),round(steady_income),      round(steady_costs),                round(steady_interest),
                        round(steady_salary_monthly), round(steady_salary_monthly_disc) ])
    df = pd.DataFrame.from_records(to_append, columns=[
                        "Year",                     "Property_value",               "Appreciation",     "Loan",                     "Cash", 
                        "Capital_injection",        "Income",                       "Expenses",         "Interest_amount",          "Profit", 
                        "Profit_cum",               "Leverage",                     "Cap_inj_control",  "cap_inj_year_adj",         "capital_injection_yearly", 
                        "houses_n",                 "houses_n_theoretical",
                        "Invest_cap_leveraged",     "amortization",                 "Steady_income",    "Steady_costs",             "Steady_interest",
                        "Steady_salary_monthly", "Steady_salary_monthly_disc"])
    return df



df_results = pd.DataFrame()
w = ExcelWriter(excel_path +'\output.xlsx')
for lev_i in leverage:
    for adj_i in cap_inj_year_adj:
        for inject_i in capital_injection:
            for inject_yearly_i in capital_injection_yearly:
                for year_i in n_years:
                    df = simulation(year_i, lev_i, inject_i, capital_injection_years,inject_yearly_i, adj_i)
                    df_results =  df_results.append(df.iloc[-1])
                    # df_results["steady_income"] = ((df_results["houses_n_theoretical"] * income_i) * (1- vacancy_rate))
                    # df_results["steady_costs"] = (df_results["houses_n_theoretical"] * kvm * expenses_kvm_steady)
                    # df_results["steady_interest"] = (df_results["houses_n_theoretical"] * property_price * lev_i * interest_rate)
                    # df_results["steady_loan"] = (df_results["houses_n_theoretical"] * property_price * lev_i)
                    # df_results["test_prop_price"] = (property_price)
                    # df_results["test_leverage"] = ( lev_i)

                    df_results.to_excel(excel_path + "\output.xlsx",index=False)
                    df.to_excel(excel_path + "\df_lev{lev}_capinj{capinj}_capy{capy}_Y{y}.xlsx".format(lev= lev_i, capinj=inject_i,capy=inject_yearly_i,y=year_i),index=False)

                    # df_results["steady_revenue_yearly"] = \
                    #     ((df_results["houses_n_theoretical"] * income_i) * (1- vacancy_rate)) \
                    #     - (df_results["houses_n_theoretical"] * kvm * expenses_kvm_steady) \
                    #     - (df_results["houses_n_theoretical"] * property_price * lev_i * interest_rate)
print(df_results[["Cap_inj_control","capital_injection_yearly","Leverage","cap_inj_year_adj","houses_n_theoretical","Steady_salary_monthly"]].tail(50))




# Plot the respnse surface

X, Y = np.meshgrid(df_results[var_X].values, df_results[var_Y].values)
Z = df_results[var_Z].values

# re-create the 2D-arrays
x1 = np.linspace(df_results[var_X].min(), df_results[var_X].max(), len(df_results[var_X].unique()))
y1 = np.linspace(df_results[var_Y].min(), df_results[var_Y].max(), len(df_results[var_Y].unique()))
x2, y2 = np.meshgrid(x1, y1)
z2 = griddata((df_results[var_X], df_results[var_Y]), df_results[var_Z], (x2, y2), method='cubic')

fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(x2, y2, z2, rstride=1, cstride=1, cmap=cm.seismic,
    linewidth=0, antialiased=False)
# surf = ax.plot_wireframe(x2, y2, z2, rstride=1, cstride=1)

    
ax.set_zlim(auto=True)

ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)
plt.title(var_X + " " + var_Y)


plt.show()

print(df)