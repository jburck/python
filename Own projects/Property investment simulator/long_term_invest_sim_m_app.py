



# SIMULATOR
#####################################################################################################################################################################################################

# Programmer:        Johan Burck
# Description:       Second version of simluation for longer term investment decisions
# Development start: 2021-06-20

# Objekt: https://beskrivning.fasad.eu/objekt/1523763/beskrivning.html

def sim():

    import pandas as pd
    import numpy as np

    from mpl_toolkits import mplot3d
    import matplotlib
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    from matplotlib import cm
    from matplotlib.ticker import LinearLocator, FormatStrFormatter
    from scipy.interpolate import griddata
    from pandas import ExcelWriter 
    import configparser
    import pathlib
    import os

    
    matplotlib.use('TKAgg')
    # Read stats from configuration file
    # Objects: Stallängsvägen, Storgatan, Queckfeldtsgatan 31, Queckfeldtsgatan 92
    config = configparser.ConfigParser()
    object = (E0.get())

    path = os.environ["HOMEPATH"]
    root = "C:\\" +path + "\desktop\sim"

    config.readfp(open(root + r'\property_config.txt'))
    property_price_tmp =    config.getint(object, 'price_building')
    kvm =               config.getint(object, 'living_space')
    income_i =          config.getint(object, 'income')

    excel_path = root

    property_price          = property_price_tmp + int((E1.get()))
    n_months_tmp                =  (E2.get())
    n_months                    = list(map(int, n_months_tmp.split()))  

    amortization_rate            = float((E3.get()))
    interest_rate                = float((E4.get()))
    skatt                        = 0.206
    dividend_tax_r               = float((E5.get()))
    interest_deduction_rate      = 0.30
    expenses_kvm                 = int(E8.get())
    expenses_kvm_steady          = int(E9.get()) # expenses/kvm in the steady state
    vacancy_rate                 = float((E10.get()))
    inflation_rate               = 0.015

    inflation_r_monthly         = ((1 + inflation_rate) ** (1/12)) - 1
    amortization_r_monthly      = ((1 + amortization_rate) ** (1/12)) - 1
    interest_r_monthly          = ((1 + interest_rate) ** (1/12)) - 1
    income_monthly               = income_i / 12
    expenses_i                   = kvm * expenses_kvm 
    expenses_monthly             = expenses_i / 12
    expenditure_rate             = 0.045 # % of price that is paid as expenditures
    expenditure_amount           = expenditure_rate * property_price

    # Capital injection
    capital_injection_annual    = 0

    # Parameters to analyze
    leverage_tmp                = (E12.get())
    leverage                    = list(map(float, leverage_tmp.split()))
    capital_injection_months_tmp= (E13.get())
    capital_injection_months    = np.array(list(map(int, capital_injection_months_tmp.split())))
    #capital_injection_months
    capital_injection_tmp       = (E14.get())
    capital_injection           = list(map(int, capital_injection_tmp.split()))
    cap_inj_month_adj           = [0] #adjust the inection months by shifting
    capital_injection_monthly_tmp=(E16.get())
    capital_injection_monthly   = list(map(int, capital_injection_monthly_tmp.split()))
    capital_injection_m_start   = int(E17.get())
    capital_injection_m_end     = int(E18.get())

    fixed_extraction_month      = (E19.get()) # y/n
    capital_extraction_m_start  = int(E20.get())
    capital_extraction_fix      = float((E21.get()))      # Either rate or sum. If sum, then it will be adjusted for inflation. Will be extracted ad stated start-month above.
    capital_extraction_nofix    = int((E22.get()))     # Extraction starts when this amount can be extracted from the monthly profit * the extraction fix rate above. Will also end capital_injection_m_end
                                            # Svensk medianlön 2021: 32400, efter skatt: 24800. 

    # Target variable is the theoretical number of houses Z, but X and Y can be changed in the response plane plot
    # "Cap_inj_control" , "cap_inj_month_adj", "capital_injection_monthly"
    var_X = "CAPITAL_INJECTION_MONTHLY"
    var_Y = "Leverage"
    var_Z = "Profit" #Steady_salary_monthly, houses_n_theoretical

    def simulation(n_months, leverage, capital_injection, capital_injection_months,capital_injection_monthly, cap_inj_month_adj, capital_injection_m_end):
        to_append= []
        capital_injection_months = capital_injection_months - cap_inj_month_adj

        for i in range(n_months+1):
            if i in capital_injection_months:
                capital_injection_extra = capital_injection
            elif i != 0 and capital_injection_m_start <= i <= capital_injection_m_end:
                capital_injection_extra = capital_injection_monthly
            else:
                capital_injection_extra = 0

            if i == 0:
                cash            = 0
                amortization    = 0
                interest_payment = 0
                profit          = 0
                profit_cum      = 0
                interest_deduction = 0
                capital_extraction_amt = 0
                dividend_pp            = 0
                dividend_pp_nvp        = 0
                dividend_max_nvp       = 0
                expenses        = expenses_monthly
                income          = income_monthly * (1-vacancy_rate)
                loan            = property_price * leverage
                assets          = property_price
                property_value  = property_price
                equity_property = property_price * (1-leverage)
                equity_total    = property_price * (1-leverage) + cash
                capital_injection_m_end = capital_injection_m_end
                excess_equity   =   0
                investment_capability_leveraged = 0
                houses_n        =   1
                houses_n_theoretical = 1
                appreciation    = 0
                index = 100

            else: #If not month = 0
                index           = index * (1+inflation_r_monthly)
                if investment_capability_leveraged >= (property_price + (expenditure_amount/ (1-leverage) )): #If we invest
                    houses_n += 1
                    equity_contribution = (equity_property + appreciation - (property_value * (1-leverage)))  / (1-leverage)
                    cash_reduction = (property_price - equity_contribution) * (1-leverage)
                    income      = (income_monthly * (1-vacancy_rate) *houses_n )  * (index/100)
                    expenses    = (expenses_monthly * houses_n) * (index/100)
                    property_value = property_value + property_price + appreciation # Add accumulated appreciation to the new property price 
                    appreciation = property_value * inflation_r_monthly                  # Reset appreciation
                    loan        = + (property_value * leverage) - amortization
                    interest_payment = ((loan + (loan - ((property_value * leverage) * amortization_r_monthly))) / 2) * interest_r_monthly
                    cash        = cash - cash_reduction - expenditure_amount + (((income + interest_deduction - expenses - interest_payment)*(1-skatt))-amortization) + capital_injection_annual + capital_injection_extra - capital_extraction_amt
                else: #If no investment
                    interest_payment = ((loan + (loan - ((property_value * leverage) * amortization_r_monthly))) / 2) * interest_r_monthly
                    income      = (income_monthly * (1-vacancy_rate) *houses_n )  * (index/100)
                    expenses    = (expenses_monthly * houses_n) * (index/100)
                    appreciation = appreciation + (property_value * inflation_r_monthly)
                    loan        = loan - amortization
                    cash        = cash + (((income + interest_deduction - expenses - interest_payment)*(1-skatt))-amortization) + capital_injection_annual + capital_injection_extra - capital_extraction_amt

                if fixed_extraction_month == "y":
                    if i >= capital_extraction_m_start:
                        amortization = 0
                        if 0 <= capital_extraction_fix <=1:
                            capital_extraction_amt = profit * capital_extraction_fix
                        else:
                            capital_extraction_amt = capital_extraction_fix * (index/100)
                    else:
                        amortization    = (property_value * leverage) * amortization_r_monthly
                elif fixed_extraction_month == "n":
                    if (profit * capital_extraction_fix) >= (capital_extraction_nofix * (index/100)):
                        capital_injection_m_end = i 
                        amortization = 0
                        capital_extraction_amt = (capital_extraction_nofix * (index/100))
                        expenses = (houses_n * kvm * expenses_kvm_steady/12) * ((1+inflation_r_monthly)**i)
                    else:
                        amortization    = (property_value * leverage) * amortization_r_monthly
                
                interest_deduction = interest_payment * interest_deduction_rate
                profit          = (income - expenses - interest_payment + interest_deduction)*(1-skatt) #<-- confirm that interest deduction should be taxed
                profit_cum      = profit_cum + profit
                dividend_pp = (capital_extraction_amt * (1-dividend_tax_r)) / 2
                dividend_pp_nvp = dividend_pp / (index/100)
                dividend_max_nvp = (profit * (1-dividend_tax_r) /2) / (index/100)

                equity_property =   property_value - loan  
                assets          = property_value + cash
                equity_total    = assets - loan
                excess_equity   = equity_total - (property_value * (1-leverage))
                investment_capability_leveraged = ((excess_equity) / (1-leverage)) + (appreciation / (1-leverage))
                houses_n_theoretical = houses_n + (investment_capability_leveraged / (property_price*(index/100)))

            to_append.append([i,                   round(cash),                               round(property_value),                  appreciation,               loan,                        
                            capital_injection_extra,    round(income),                  round(expenses),                        round(interest_payment), round(amortization),   round(profit),                      
                            round(houses_n),            round(investment_capability_leveraged) ,        round(interest_deduction),
                            round(capital_extraction_amt) , round(dividend_pp),  round(dividend_pp_nvp), round(dividend_max_nvp),
                            capital_injection, cap_inj_month_adj, capital_injection_monthly, houses_n_theoretical, index, leverage  ])
        df = pd.DataFrame.from_records(to_append, columns=[
                            "Month",     "Cash" ,               "Property_value",               "Appreciation",             "Loan",                     
                            "Capital_injection",        "Income",                       "Expenses",                     "Interest_amount", "amortization",          "Profit",                    
                            "houses_n",                 "Invest_cap_leveraged",                      "Interest deduction", 
                            "Capital_extraction_amount" , "Dividend_pp",  "Dividend_pp_nvp", "dividend_max_nvp",
                            "CAP_INJ_CONTROL", "CAP_INJ_MONTH_ADJ", "CAPITAL_INJECTION_MONTHLY", "HOUSES_N_THEORETICAL", "index", "Leverage"])
        return df



    df_results = pd.DataFrame()
    w = ExcelWriter(excel_path +'\output.xlsx')
    for lev_i in leverage:
        for adj_i in cap_inj_month_adj:
            for inject_i in capital_injection:
                for inject_monthly_i in capital_injection_monthly:
                    for year_i in n_months:
                        df = simulation(year_i, lev_i, inject_i, capital_injection_months,inject_monthly_i, adj_i, capital_injection_m_end)
                        df_results =  df_results.append(df.iloc[-1])
                        df_results.to_excel(excel_path + "\output.xlsx",index=False)
                        df.to_excel(excel_path + "\\" + object + "lev{lev}_capinj{capinj}_capm{capy}_M{y}.xlsx".format(lev= lev_i, capinj=inject_i,capy=inject_monthly_i,y=year_i),index=False)
    print(df_results[["CAP_INJ_CONTROL","CAPITAL_INJECTION_MONTHLY","Leverage","CAP_INJ_MONTH_ADJ","HOUSES_N_THEORETICAL"]].tail(50))



# PLOTS                                 
##############################################################################################################################################################################

    #Time series plots
    if check_var_1.get():
        ax1 = plt.gca()
        df.plot(x="Month", y="Cash", ax=ax1, grid=True)
        df.plot(x="Month", y="Property_value", ax=ax1, grid=True)
        df.plot(x="Month", y="Invest_cap_leveraged", ax=ax1, grid=True, label="Leveraged cash and equity amount")
        plt.show()

    if check_var_2.get():
        ax2 = plt.gca()
        df.plot(kind='line',x="Month", y="Capital_injection", ax=ax2, grid=True)
        df.plot(kind='line',x="Month", y='Capital_extraction_amount',ax=ax2, grid=True)
        df.plot(kind='line',x="Month", y='Dividend_pp', color='red', ax=ax2, grid=True, label="Dividend per person (2)")
        plt.ylim(bottom=0)
        plt.show()
    
    if check_var_3.get():
        ax3 = plt.gca()
        df.plot(kind='line',x="Month", y="Income", color='forestgreen',ax=ax3, grid=True)
        df.plot(kind='line',x="Month", y='Expenses',color='red',ax=ax3, grid=True)
        df.plot(kind='line',x="Month", y='Interest_amount', color='salmon', ax=ax3, grid=True)
        df.plot(kind='line',x="Month", y='amortization', color='darkorange', ax=ax3, grid=True)
        df.plot(kind='line',x="Month", y='Interest deduction', color='limegreen', ax=ax3, grid=True)
        plt.ylim(bottom=0)
        plt.show()

    if check_var_4.get():
        # Plot the respnse surface
        X, Y = np.meshgrid(df_results[var_X].values, df_results[var_Y].values)
        Z = df_results[var_Z].values

        x1 = np.linspace(df_results[var_X].min(), df_results[var_X].max(), len(df_results[var_X].unique()))
        y1 = np.linspace(df_results[var_Y].min(), df_results[var_Y].max(), len(df_results[var_Y].unique()))
        x2, y2 = np.meshgrid(x1, y1)
        z2 = griddata((df_results[var_X], df_results[var_Y]), df_results[var_Z], (x2, y2), method='cubic')

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        surf = ax.plot_surface(x2, y2, z2, rstride=1, cstride=1, cmap=cm.seismic,
            linewidth=0, antialiased=False)
        ax.set_zlim(auto=True)
        ax.zaxis.set_major_locator(LinearLocator(10))
        ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
        ax.zaxis.set_label_text(var_Z)
        ax.yaxis.set_label_text(var_Y)
        ax.xaxis.set_label_text(var_X)
        fig.colorbar(surf, shrink=0.5, aspect=5)
        plt.title(var_X + ",  " + var_Y + ",  " + var_Z)
        plt.show()


# GUI
########################################################################################################################################################################################################################

from tkinter import *

top = Tk()
top.geometry("700x960")

check_var_1 = IntVar()
check_var_2 = IntVar()
check_var_3 = IntVar()
check_var_4 = IntVar()


leftframe = Frame(top, relief=RIDGE)
leftframe.pack( side = RIGHT )

box1 = Checkbutton (leftframe, text="Investment graph", variable=check_var_1)
box1.pack(side=TOP)

box2 = Checkbutton (leftframe, text="In/outflow graph", variable=check_var_2)
box2.pack(side=TOP)

box3 = Checkbutton (leftframe, text="Income expense graph", variable=check_var_3)
box3.pack(side=TOP)

box4 = Checkbutton (leftframe, text="Profit-Leverage-Injection graph\n(Requires multiple entries for leverage and Monthly capital injection)", variable=check_var_4)
box4.pack(side=TOP)

Titel = Label(top, text="Real estate investment simulator", bg="black", fg="white")  
Titel.pack( side = TOP)

L0 = Label(top, text="Objekt")  
L0.pack( side = TOP)
E0 = Entry(top, bd =3)
E0.insert(0,"Queckfeldtsgatan 31")
E0.pack(side = TOP)

L1 = Label(top, text="Property price adjustment")
L1.pack( side = TOP)
E1 = Entry(top, bd =3)
E1.insert(0,0)
E1.pack(side = TOP)

L2 = Label(top, text="Simulation period, months*")
L2.pack( side = TOP)
E2 = Entry(top, bd =3)
E2.insert(0,120)
E2.pack(side = TOP)

L3 = Label(top, text="Amortization_rate")
L3.pack( side = TOP)
E3 = Entry(top, bd =3)
E3.insert(0,0.0333)
E3.pack(side = TOP)

L4 = Label(top, text="Interest_rate")
L4.pack( side = TOP)
E4 = Entry(top, bd =3)
E4.insert(0,0.02)
E4.pack(side = TOP)

L5 = Label(top, text="Dividend tax rate")
L5.pack( side = TOP)
E5 = Entry(top, bd =3)
E5.insert(0,0.30)
E5.pack(side = TOP)

L8 = Label(top, text="Expenses kvm")
L8.pack( side = TOP)
E8 = Entry(top, bd =3)
E8.insert(0,450)
E8.pack(side = TOP)

L9 = Label(top, text="Expenses/kvm steady state")
L9.pack( side = TOP)
E9 = Entry(top, bd =3)
E9.insert(0,425)
E9.pack(side = TOP)

L10 = Label(top, text="Vacancy rate")
L10.pack( side = TOP)
E10 = Entry(top, bd =3)
E10.insert(0,0.05)
E10.pack(side = TOP)

L11 = Label(top, text="Inflation rate")
L11.pack( side = TOP)
E11 = Entry(top, bd =3)
E11.insert(0,0.01)
E11.pack(side = TOP)

L12 = Label(top, text="Leverage*")
L12.pack( side = TOP)
E12 = Entry(top, bd =3)
E12.insert(0,0.70)
E12.pack(side = TOP)

L13 = Label(top, text="Extra capital injection months*")
L13.pack( side = TOP)
E13 = Entry(top, bd =3)
E13.insert(0,[12,36])
E13.pack(side = TOP)

L14 = Label(top, text="Extra capital injection amount*")
L14.pack( side = TOP)
E14 = Entry(top, bd =3)
E14.insert(0, 500000)
E14.pack(side = TOP)

L16 = Label(top, text="Monthly capital injection*")
L16.pack( side = TOP)
E16 = Entry(top, bd =3)
E16.insert(0, 20000)
E16.pack(side = TOP)

L17 = Label(top, text="Monthly capital injection start month")
L17.pack( side = TOP)
E17 = Entry(top, bd =3)
E17.insert(0, 37)
E17.pack(side = TOP)

L18 = Label(top, text="Monthly capital injection end month")
L18.pack( side = TOP)
E18 = Entry(top, bd =3)
E18.insert(0, 120)
E18.pack(side = TOP)

L19 = Label(top, text="Use fixed extraction month (y/n)?")
L19.pack( side = TOP)
E19 = Entry(top, bd =3)
E19.insert(0, "y")
E19.pack(side = TOP)

L20 = Label(top, text="Capital extraction start month")
L20.pack( side = TOP)
E20 = Entry(top, bd =3)
E20.insert(0, 121)
E20.pack(side = TOP)

L21 = Label(top, text="Fixed capital extraction amount/ratio for fixed start")
L21.pack( side = TOP)
E21 = Entry(top, bd =3)
E21.insert(0, 0.5)
E21.pack(side = TOP)

L22 = Label(top, text="Capital extraction amount for dynamic start")
L22.pack( side = TOP)
E22 = Entry(top, bd =3)
E22.insert(0, 100000)
E22.pack(side = TOP)

button = Button(leftframe,
    text="Run!",
    width=10,
    height=3,
    bg="forestgreen",
    fg="yellow", command=sim
)
button.pack(side=BOTTOM)


top.mainloop()