# Project Asteroids 
# Ska innehålla 1. Pixelgrafik 2. Ljud 3. Startskärm 4. Någon twist på orginalet?
# Datum: 2022-06-13

# On buttons: https://www.youtube.com/watch?v=al_V4OGSvFU&ab_channel=BaralTech
# On screens: https://www.youtube.com/watch?v=GMBqjxcKogA&ab_channel=BaralTech

from matplotlib.font_manager import get_font
import pygame, sys

pygame.init()
size = width, height = 600,600
screen = pygame.display.set_mode(size)
main_font = pygame.font.SysFont(None, 50)

class Button():
    def __init__(self, image, x_pos, y_pos, text_input):
        self.image = image
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.rect = self.image.get_rect(center=(self.x_pos,self.y_pos))
        self.text_input = text_input
        self.text = main_font.render(self.text_input,True,"white")
        self.text_rect = self.text.get_rect(center=(self.x_pos, self.y_pos))
    def update(self): #puts the button&text on the screen at the rect location
        screen.blit(self.image, self.rect)
        screen.blit(self.text, self.text_rect)
    def check_for_input(self, position): #Method to detect when mouse if hovering above the rect
        if position[0] in range(self.rect.left, self.rect.right) and \
            position[1] in range(self.rect.top, self.rect.bottom):
            print("Button Press!")

clock = pygame.time.Clock()

button_surface = pygame.image.load("button_4182376.png") # Load the button image
button_surface = pygame.transform.scale(button_surface, (150,150))

button = Button(button_surface, width/2, 100, "START")

def menu():
    pygame.display.set_caption("Menu")
    while True:

        play_mouse_pos = pygame.mouse.get_pos()

        screen.fill("black")
        font = pygame.font.SysFont(None, 50)
        score_img = font.render("Start menu", True, "white")
        screen.blit(score_img, (5,5))
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                button.check_for_input(pygame.mouse.get_pos())


        button.update()

        pygame.display.update()

menu()