# Shooting game to learn some pygame basics. 
# There is a bug where two enemies are hit at the same time. The game crashes.

import pygame
import random

clock = pygame.time.Clock()

def show_go_screen(screen, width, height, score):
    while 1==1:
        font = pygame.font.SysFont(None, 32)
        game_over_img = font.render("GAME OVER", True, "red")
        play_again_img = font.render("Your score was {}. Play again? (y/n)".format(score), True, "red")
        screen.blit(game_over_img, (width/2-(game_over_img.get_width()/2), height/2))
        screen.blit(play_again_img, (width/2-(play_again_img.get_width()/2), height/2 + 20))
        pygame.display.update()
        
        for event in pygame.event.get():
            #Only do something if the event is of type quit
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_y:
                    main()
                if event.key == pygame.K_n:
                    running = False
                    pygame.quit()
                    quit()

# Main function
def main():
    pygame.init()
    size = width, height = (600, 600)
    screen = pygame.display.set_mode(size)
    pygame.display.set_caption("My first pygame - controls: WASD & right click")

    score = 0
    n_opponents = 1
    character_loc_x = 50
    character_loc_y = 50
    character_velocity = 2
    opponent_speed = 0.8
    player_shots = []

    opponents = [{"pos_x": 450, "pos_y": 450, "size_x": 30, "size_y": 30}]
    bulletspeed = 6
    fps = 60

    def character_movement(x,y):
        if character.top <= 5:
            y = 6
        elif character.bottom >= height-5:
            y = height-character.height-6
        else:
            y += character_vel_y
        if character.left <= 5:
            x = 6
        elif character.right >= width-5:
            x = width-character.width-6
        else:
            x += character_vel_x
        return x, y
    
    def opponent_movement(x,y):
        # Opponent movement logic
        if character_loc_y > y:
            y += opponent_speed
        else:
            y -= opponent_speed
        if character_loc_x > x:
            x += opponent_speed
        else:
            x -= opponent_speed
        return x, y
    
    def redraw_window(score, bullets, opponents):
        # Draw gameboard
        pygame.Surface.fill(screen, (75,75,75))
        pygame.draw.aaline(screen, "black" ,(5,5), (5,height-5))
        pygame.draw.aaline(screen, "black" ,(5,5), (width-5,5))
        pygame.draw.aaline(screen, "black" ,(width-5,5), (width-5,height-5))
        pygame.draw.aaline(screen, "black" ,(width-5,height-5), (5,height-5))
        # Score
        font = pygame.font.SysFont(None, 24)
        score_img = font.render("Score: {}".format(score), True, "black")
        screen.blit(score_img, (5,5))

        # Draw characters
        pygame.draw.rect(screen, (255,255,255), character)

        for i in opponents:
            pygame.draw.rect(screen, (0,0,0), i["Rect"])

        # Draw bullets
        for bullet in bullets:
            pygame.draw.rect(screen, "white", bullet)

        pygame.display.update()

    character_vel_x = 0
    character_vel_y = 0
    
    game_over = False
    running = True
    # Main loop
    while running:
        if game_over:
            show_go_screen()
        clock.tick(fps)
        #calculate positions
        character = pygame.Rect(character_loc_x,character_loc_y,30,30)
        
        if n_opponents > len(opponents):
                opponents.append({"pos_x": random.randrange(0,width), "pos_y": height+100, "size_x": 30, "size_y": 30})

        for i in range(len(opponents)):
            opponents[i]['pos_x'], opponents[i]['pos_y'] = opponent_movement(opponents[i]['pos_x'], opponents[i]['pos_y'])
            #opponent = pygame.Rect(opponent['pos_x'],opponent['pos_y'],30,30)
            opponents[i]["Rect"] = pygame.Rect(opponents[i]['pos_x'],opponents[i]['pos_y'],30,30)


        #opponent = pygame.Rect(450,450,30,30)


        # Update bullet positions and delete bullets outside of screen
        bullets = []
        shots_to_delete = []
        for index, shot in enumerate(player_shots):
            shot[0][0] += shot[1][0]
            shot[0][1] += shot[1][1]
            bullets.append(pygame.Rect(shot[0][0], shot[0][1], 2,3))
            #shot[2] = pygame.Rect(shot[0][0], shot[0][1], 2,3)
            if (shot[0][0] <= 0) or (shot[0][1] <= 0) or (shot[0][0] >= width) or (shot[0][1] >= height):
                shots_to_delete.append(index)

        # Collision detection 
        opponents_rect_list = [d["Rect"] for d in opponents]
        if pygame.Rect.collidelist(character, opponents_rect_list) != -1:
            show_go_screen(screen, width, height, score)

        # loop over opponents
        for i in opponents_rect_list:
            if pygame.Rect.collidelist(i, bullets) != -1:
                shots_to_delete.append(pygame.Rect.collidelist(i, bullets))
                score += 10

        for i in shots_to_delete:
            if pygame.Rect.collidelist(bullets[i], opponents_rect_list) != -1:
                del opponents[pygame.Rect.collidelist(bullets[i], opponents_rect_list)]
                n_opponents += 1

        # Remove obsolete objects
        for i in sorted(shots_to_delete, reverse=True):
            del player_shots[i]
            

        redraw_window(score, bullets, opponents)
 
        # Event handling gets all events from the event queue
        for event in pygame.event.get():
            #Only do something if the event is of type quit
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                quit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                character_center_pos = (int((character.left+character.right)/2) , int((character.bottom+character.top)/2))                
                trajectory_x = round((mouse_pos[0] - character_center_pos[0]) / (abs(mouse_pos[0] - character_center_pos[0]) + abs(mouse_pos[1] - character_center_pos[1])) * bulletspeed, 3)
                trajectory_y = round((mouse_pos[1] - character_center_pos[1]) / (abs(mouse_pos[0] - character_center_pos[0]) + abs(mouse_pos[1] - character_center_pos[1])) * bulletspeed, 3)
                player_shots.append([list(character_center_pos), (trajectory_x, trajectory_y)])                
                print(player_shots)

            # Player movement
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_s:
                    character_vel_y += character_velocity
                elif event.key == pygame.K_w:
                    character_vel_y += -character_velocity
                if event.key == pygame.K_d:
                    character_vel_x += character_velocity
                elif event.key == pygame.K_a:
                    character_vel_x += -character_velocity

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_s:
                    character_vel_y -= character_velocity
                elif event.key == pygame.K_w:
                    character_vel_y -= -character_velocity
                if event.key == pygame.K_d:
                    character_vel_x -= character_velocity
                elif event.key == pygame.K_a:
                    character_vel_x -= -character_velocity

        character_loc_x, character_loc_y = character_movement(character_loc_x,character_loc_y)


# run the main function only if this module is executed as the main script
# (if you import this as a module then nothing is executed)
if __name__ == '__main__':
    main()