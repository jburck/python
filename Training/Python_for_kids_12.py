#Try creating a canvas

import time
from tkinter import *
import random

tk = Tk()
canvas = Canvas(tk, width=500, height=500)
canvas.pack()


#create a rectangle

canvas.create_rectangle(10,10,50,50)


#fill the screen with triangles

for x in range(1,100,2):
    randnum = random.randint(1,500)
    randnum2 = random.randint(1,500)
    canvas.create_polygon(randnum,randnum2,randnum+15,randnum2+5,randnum-10,randnum2-20, fill="Red", outline="Black")


my_triangle = canvas.create_polygon(15, 50,15+15,50+5,15-10,50-20, fill="Blue", outline="Black")
for x in range(1,100):
    canvas.move(my_triangle, 5,2+round(x/5))
    tk.update()
    time.sleep(0.05)

