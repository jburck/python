#Save a list to a dat file

import pickle

my_list = ["Ada", "SQL", "VBA", "SAS", "Python"]

save_file = open("save.dat", "wb")
pickle.dump(my_list, save_file)
save_file.close()

#Load file from a dat file and print it


load_file = open("save.dat", "rb")
loaded_data = pickle.load(load_file)
load_file.close()

print(loaded_data)
