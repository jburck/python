import pandas as pd
import numpy as np
from pandasql import sqldf, load_births

births = load_births()
q = '''
SELECT 
date(date) as BateOfBirth,
sum(births) as "Total Births"
FROM births
WHERE births < 300045
GROUP BY date
limit 10
;
'''
sqldf(q)



#Create function to specify local/global variable scope - useful when having multiple queries across the program

def pysql(q):
    return sqldf(q, globals())

pysql(q)