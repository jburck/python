
# LESSER OF TWO EVENS: Write a function that returns the lesser of two given numbers if both numbers are even, but returns the greater if one or both numbers are odd

# lesser_of_two_evens(2,4) --> 2
# lesser_of_two_evens(2,5) --> 5


def lesser_of_two_evens(a,b):
    if a % 2 == 0 and b % 2 == 0:
        val = min(a, b)
    else:
        val = max(a,b)
    return val

lesser_of_two_evens(2,5)

# ANIMAL CRACKERS: Write a function takes a two-word string and returns True if both words begin with same letter


def animal_crackers(str):
    my_str_list = str.split()
    return my_str_list[0][0] == my_str_list[1][0]

animal_crackers(str)

# MAKES TWENTY: Given two integers, return True if the sum of the integers is 20 or if one of the integers is 20. If not, return False
def makes_twenty(a, b):
    return a + b == 20 or a == 20 or b == 20

makes_twenty(20,7)