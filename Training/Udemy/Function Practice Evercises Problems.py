# Do 1 problem per level

# OLD MACDONALD: Write a function that capitalizes the first and fourth letters of a name

def old_macdonald(str):
    newstr = str[0].upper()  + str[1:3] + str[3].upper() + str[4:]
    return newstr

old_macdonald("testar")


# FIND 33: Given a list of ints, return True if the array contains a 3 next to a 3 somewhere.
alist = [3,1,3,4]

def has_33(mylist):
    stored_integer = int()
    indicator = bool()
    for integer in mylist:
        indicator = integer == stored_integer and integer == 3
        if indicator:
            return indicator
        stored_integer = integer
    return indicator
        
has_33( alist )
