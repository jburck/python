# https://github.com/Pierian-Data/Complete-Python-3-Bootcamp/blob/master/03-Methods%20and%20Functions/08-Functions%20and%20Methods%20Homework.ipynb

# 1
# Write a function that computes the volume of a sphere given its radius.

def vol(rad):
    V = (4/3)*3.14* (rad**3)
    return V

vol(2)


# 2
# Write a function that checks whether a number is in a given range (inclusive of high and low)

# 2.a
def ran_check(num,low,high):
    if low <= num <= high:
        return "Number is within the range"
    else:
        return "Number is not within the range"

ran_check(0,1,4)


# 2.b (Returning a boolean)
def ran_bool(num,low,high):
    return low <= num <= high

ran_bool(4,5,6)

# 3
# Write a Python function that accepts a string and calculates the number of upper case letters and lower case letters.

def case_counter(my_string):
    upper_counter = 0
    lower_counter = 0
    other_counter = 0

    for letter in my_string:
        if letter.isupper():
            upper_counter += 1
        elif letter.islower():
            lower_counter +=1
        else:
            other_counter +=1
    print(f"No. of Upper case characters : {upper_counter} \nNo. of Lower case Characters : {lower_counter}")
case_counter("I am TesTing this String function out yO!!!! ")


# 4
# Write a Python function that takes a list and returns a new list with unique elements of the first list.

a_list = [1,2,5,23,2,4,4,5,65,1,23,5,3,5,6,6,2]

def get_me_uniques(my_list):
    my_set = set(my_list)
    my_newlist = list(my_set)
    return my_newlist
get_me_uniques(a_list)


# 5
# Write a Python function to multiply all the numbers in a list.

# 6
# Write a Python function that checks whether a passed in string is palindrome or not.



# %%
