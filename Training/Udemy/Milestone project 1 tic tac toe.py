# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
from IPython.display import clear_output

def screen(list):
    clear_output()
    print(        "\n\n"        +  "\n\n {} | {} | {} \n {} | {} | {} \n {} | {} | {} \n".format( list[6], list[7], list[8], list[3], list[4], list[5], list[0], list[1], list[2])         )


# %%
def turn(player):
    return input("Player " + player + ", enter your square (1-9).")


# %%
def check_win_con(alist):
    return        ((alist[0] == alist[1] == alist[2]) and alist[0] != " ")     or ((alist[3] == alist[4] == alist[5]) and alist[3] != " ")     or ((alist[6] == alist[7] == alist[8]) and alist[6] != " ")     or ((alist[0] == alist[3] == alist[6]) and alist[0] != " ")     or ((alist[7] == alist[4] == alist[1]) and alist[7] != " ")     or ((alist[8] == alist[5] == alist[2]) and alist[8] != " ")     or ((alist[6] == alist[4] == alist[2]) and alist[6] != " ")     or ((alist[8] == alist[4] == alist[0]) and alist[8] != " ")



# %%


print(        "\n\n"        + "Hello and welcome to tic tac toe! The goal is to get three symbols in a row. Use the keypad to put your mark on the board."        
        )

mylist = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
screen(mylist)
mylist = [" ", " ", " ", " ", " ", " ", " ", " ", " "]

XO = input("Player 1, do you want to be X or O?")
if XO == "O":
    P1 = "O"
    P2 = "X"
else:
    P1 = "X"
    P2 = "O"

start = input("Do you want to start Y/N?")
if start == "Y":
    wincon = 0
    i = 0
    while wincon == False:
        if i % 2 == 0:
            x = turn(P1)
            mylist[int(x)-1] = P1
            i += 1
            screen(mylist)
            wincon = check_win_con(mylist)
        else:
            x = turn(P2)
            mylist[int(x)-1] = P2
            i += 1
            screen(mylist)
            wincon = check_win_con(mylist)
    print("Congratulations! The game is over!")


# %%


