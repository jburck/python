#Print annual weight equivalent on the moon

myweight = 77
gain = 1
moon_factor = 0.165
years = 15

newweight = myweight
for year in range(years):
    newweight += gain
    print(newweight*moon_factor)

