

class giraffe():

    def __init__(self, spots):
        self.spots = spots

    def left_foot_fwd(self):
        print("Left foot forward")

    def right_foot_fwd(self):
        print("Right foot forward")
        
    def left_foot_bck(self):
        print("Left foot backward")

    def right_foot_bck(self):
        print("Right foot backward")

    def dance(self):
        self.left_foot_fwd()
        self.right_foot_fwd()
        self.left_foot_bck()
        self.right_foot_bck()
        
reginald = giraffe (10)
reginald.dance()
