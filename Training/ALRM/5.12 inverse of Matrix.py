#ALRM Chapter 5 problem 12

#Find inverse of a matrix

import pandas as pd
import numpy as np

data = [ [5,1,3], [4,0,5], [1,9,6] ]

#As pandas Dataframe
A = pd.DataFrame(data)

#As Numpy array (seems that numpy is the prefered library for linear algebra
#https://hackr.io/blog/numpy-matrix-multiplication
A_np = np.array(data)


A_np_inv = np.linalg.inv(A_np) 

print('check if inverse matrix is correct by multiplying with A. Should produce the Identity matrix: \n ', np.dot(A_np, A_np_inv) )
