import pandas as pd #TO import data
import matplotlib.pyplot as plt # To visualize
from sklearn import linear_model #Functionalities needed to implement linear regression "SKLearn is pretty much the golden standard when it comes to machine learning in Python."
import numpy as np

#Import the CSV, add headers
df_C02 = pd.read_csv(
    "C:\Skrivbordsmapp\Python practice\Applied linear regression models Data\Appendix C datasets\C02.csv"
    , header=None
    , names= [
    "ID number",
    "County",
    "State",
    "Land Area",
    "Total population",
    "Age 18-34 in %",
    "Age 65 or older in %",
    "Number of active physicians",
    "Number of hospital beds",
    "Total serious crimes",
    "High school graduates in %",
    "Bachelor's degrees in %",
    "Below poverty level in %",
    "Unemployment in %",
    "Per capita income",
    "Total personal income",
    "Geographic region"]
    )

#Print data to make sure it has been imported correctly
print (df_C02[["Number of active physicians", "Total population", "Number of hospital beds", "Total personal income"]])

#Check if there are missing values
print("Mising values = " , df_C02.isnull().values.any())

##################################################
#RUN REGRESSION
##################################################

#Create regression object
reg_1 = linear_model.LinearRegression()
reg_2 = linear_model.LinearRegression()
reg_3 = linear_model.LinearRegression()

#Specify the model
reg_1.fit(df_C02[["Total population"]], #X
        df_C02["Number of active physicians"]) #Y

reg_2.fit(df_C02[["Number of hospital beds"]], #X
        df_C02["Number of active physicians"]) #Y

reg_3.fit(df_C02[["Total personal income"]],    #X
        df_C02["Number of active physicians"]) #Y

#Print the coefficients
print("Model: Number of active physicians = ",
      round(reg_1.intercept_, 4) , " + " ,
      round(reg_1.coef_[0], 4), "* Total population"
      )

print("Model: Number of active physicians = ",
      round(reg_2.intercept_ , 2), " + " ,
      round(reg_2.coef_[0], 2), "* Number of hospital beds",
      )

print("Model: Number of active physicians = ",
      round(reg_3.intercept_, 2), " + " ,
      round(reg_3.coef_[0], 2), "* Total personal income"
      )

print("R2_1: ", round(reg_1.score(df_C02[["Total population"]],
        df_C02["Number of active physicians"]),2))

print("R2_2: ", round(reg_2.score(df_C02[["Number of hospital beds"]],
        df_C02["Number of active physicians"]),2))

print("R2_3: ", round(reg_3.score(df_C02[["Total personal income"]],
        df_C02["Number of active physicians"]),2))

pred_1 = reg_1.predict(df_C02[["Total population"]])
pred_2 = reg_2.predict(df_C02[["Number of hospital beds"]])
pred_3 = reg_3.predict(df_C02[["Total personal income"]])    

print(pred_1[:5])
print(pred_2[:5])
print(pred_3[:5])

##################################################
#LET'S PLOT
##################################################

#Convert dataframe into np.array for matplotlib input
C02_as_array = df_C02.values

#Make a figure object
fig_1 = plt.figure()
plt.scatter(C02_as_array[:,4],C02_as_array[:,7])
plt.plot(C02_as_array[:,4],pred_1,'black', linewidth=2.0)
plt.title('X = Total population')
plt.show(fig_1) #Must when woking in IDLE

fig_2 = plt.figure()
plt.scatter(C02_as_array[:,8],C02_as_array[:,7])
plt.plot( C02_as_array[:,8], pred_2, 'black', linewidth=2.0)
plt.title('X = Number of hospital beds')
plt.show(fig_2) #Must when woking in IDLE

fig_3 = plt.figure()
plt.scatter(C02_as_array[:,15],C02_as_array[:,7])
plt.plot( C02_as_array[:,15], pred_3, 'black', linewidth=2.0)
plt.title('X = Total personal income')
plt.show(fig_3) #Must when woking in IDLE


##################################################
#CALCULATE MEAN SUM of ERRORS (MSE)
##################################################

#Square the errors
SE_1 = (C02_as_array[:,4] - pred_1)**2
SE_2 = (C02_as_array[:,8] - pred_2)**2
SE_3 = (C02_as_array[:,15] - pred_3)**2


#Sum the squared errors
SSE_1 = np.sum(SE_1 , 0)
SSE_2 = np.sum(SE_2 , 0)
SSE_3 = np.sum(SE_3 , 0)

print(SSE_1)
print(SSE_2)
print(SSE_3)

#df = n-p-1
n = np.count_nonzero(SE_1,0)
k = 1 #Single factor model

print("n: ", n)
print("k: ", k)

MSE_1 = SSE_1 / (n - k - 1)
MSE_2 = SSE_2 / (n - k - 1)
MSE_3 = SSE_3 / (n - k - 1)

print("MSE 1: ", MSE_1)
print("MSE 2: ", MSE_2)
print("MSE 3: ", MSE_3)

print("Smallest MSE: ", min(MSE_1, MSE_2, MSE_3))

