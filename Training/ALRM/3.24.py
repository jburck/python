#Project 3.24 from Applied Linear Regression Models

import numpy as py #Useful for matrix algebra and faster than vanilla python list operations
import pandas as pd #Dataframes baby!
import matplotlib.pyplot as plt
from sklearn import linear_model as lm
import math #for square root

from scipy import stats #For getting t value

#Create empty dataframe
data = [[1,5,63],
        [2,8,67],
        [3,11,74],
        [4,7,64],
        [5,13,75],
        [6,12,69],
        [7,12,90],
        [8,6,60],
        ]

#Create dataframe from list
df_blood = pd.DataFrame(data, columns=['i','Age, Xi','Blood Preassure, Yi'])

#Take a look at the array
print(df_blood)

#Plot the data
plt.scatter(df_blood.iloc[:, [1]], df_blood.iloc[:, [2]])
plt.xlabel(df_blood.columns[1])
plt.ylabel(df_blood.columns[2])
plt.show()

#Create regression object
reg = lm.LinearRegression()
#Create model object
model = reg.fit(df_blood.iloc[:, [1]], df_blood.iloc[:, [2]])
#Print regression function
print ('\n Y =', round(model.intercept_.item(), ndigits=2), '+', round(model.coef_.item(), ndigits=2), 'X \n')
#Score the dataframe with predictions
pred = pd.DataFrame(model.predict(df_blood.iloc[:, [1]]))
#Add variable 'pred' and 'res' to blood dataframe
df_blood['pred'] = pred
df_blood['res'] = df_blood['pred'] - df_blood['Blood Preassure, Yi']
#Take another look at the dataframe
print(df_blood)

#Plot regression line over scatterplot
plt.plot(df_blood.iloc[:, [1]], df_blood.iloc[:, [3]])
plt.scatter(df_blood.iloc[:, [1]], df_blood.iloc[:, [2]])
plt.xlabel(df_blood.columns[1])
plt.ylabel(df_blood.columns[2])
plt.show()

#Residualplot over X
plt.scatter(df_blood.iloc[:, [1]], df_blood.iloc[:, [4]])
plt.xlabel(df_blood.columns[1])
plt.ylabel(df_blood.columns[4])
plt.show()

reg2 = lm.LinearRegression()

model2 = reg2.fit(df_blood[df_blood.i != 7].iloc[:, [1]] , df_blood[df_blood.i != 7].iloc[:, [2]])
print('\n Y =', round(model2.intercept_.item(), ndigits=2), '+', round(model2.coef_.item(), ndigits=2), 'X \n')

df_Newblood = df_blood[df_blood.i != 7]
pred2 = pd.DataFrame(model2.predict(df_blood.iloc[:, [1]]))
df_Newblood['pred2'] = pred2


#Residualplot over X
plt.plot(df_Newblood.iloc[:, [1]], df_Newblood.iloc[:, [5]])
plt.scatter(df_Newblood.iloc[:, [1]], df_Newblood.iloc[:, [2]])
plt.xlabel(df_Newblood.columns[1])
plt.ylabel(df_Newblood.columns[2])
plt.show()

res2 = pd.DataFrame(df_Newblood['pred2'] - df_Newblood['Blood Preassure, Yi'])
df_Newblood['res2'] = res2
df_Newblood['ressqr'] = res2 ** 2

mean_x = df_Newblood['Age, Xi'].mean(0)
df_Newblood['Xi_Xmean_dif_sqr'] = (df_Newblood['Age, Xi'] - mean_x)**2

SSE = sum(df_Newblood['ressqr'])
n = py.ma.size(df_Newblood['ressqr'], 0)
k = 1
MSE = SSE / n-k-1
t = stats.t.ppf(1-0.025, n-2)

df_Newblood['Xi_Xmean_dif_sqr_sum'] = sum(df_Newblood['Xi_Xmean_dif_sqr'])
df_Newblood['thisX_Xmean_dif'] = (df_Newblood['Age, Xi'] - mean_x)**2
df_Newblood['S_pred_sqr'] =  MSE*(1+(1/n)+(df_Newblood['thisX_Xmean_dif'] / df_Newblood['Xi_Xmean_dif_sqr_sum']))
df_Newblood['S_pred'] = df_Newblood['S_pred_sqr'] ** 0.5
df_Newblood['Pred_interval_upper'] = df_Newblood['pred2'] + (t*df_Newblood['S_pred'])
df_Newblood['Pred_interval_lower'] = df_Newblood['pred2'] - (t*df_Newblood['S_pred'])

