#ALRM chapter 6 Problem 6.9

# a. Stem plot
# b. Time plot for each predictor variable
# c. Satterplot matrix and correlation matrix

import pandas as pd

import stemgraphic as sg #can't produce chart in IDLE?
#sg.stem_graphic(list(X1.T), scale = 10)

from matplotlib import pyplot as plt

#Read CSV file and assign names since it has no headers
df = pd.read_csv("C:\Skrivbordsmapp\Python\Applied linear regression models Data\Datasets\CH6PR09.csv", names=['y', 'x1', 'x2', 'x3' ])

Y = df.iloc[ :, [0]]
X1 = df.iloc[ :, [1]]
X2 = df.iloc[ :, [2]]
X3 = df.iloc[ :, [3]]



dataX1 = list(X1['x1'].T)
dataX1 = list(X2['x2'].T)
 
# Make stems by dividing by 100000
stemsX1 = list(round(X1['x1'].T / 100000))
stemsX2 = list(round(X2.T))
  
plt.ylabel('Data')   # for label at y-axis 
  
plt.xlabel('stems')   # for label at x-axis 
  
plt.xlim(0, 60)   # limit of the values at x axis 
  
plt.stem(stems, data)   # required plot 

plt.show()
