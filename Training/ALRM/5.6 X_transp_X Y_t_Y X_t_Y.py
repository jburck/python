#ALRM chapter 5 problem 5.6

#Manual Input of Data into DataFrame
#Matrix operations: a. Y'Y b. X'X c. X'Y

# Data from 1.21

import pandas as pd

Y = pd.DataFrame([
    16.0  ,
    9.0    ,
   17.0   ,
   12.0    ,
   22.0   ,
   13.0    ,
    8.0   ,
   15.0    ,
   19.0   ,
   11.0   
    ])

data = [
    [1, 1],
    [1, 0],
    [1, 2],
    [1, 0],
    [1, 3],
    [1, 1],
    [1, 0],
    [1, 1],
    [1, 2],
    [1, 0],
    ] 

# Create the pandas DataFrame 
X = pd.DataFrame(data) 


# ".T" is the transposed matrix.
#".dot" Compute the matrix multiplication between the DataFrame and other.
#https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.dot.html

Ytrans_Y = Y.T.dot(Y)

Xtrans_X = X.T.dot(X)

Xtrans_Y = X.T.dot(Y)

print('Ytrans_Y: ', Ytrans_Y)
print('Xtrans_X: ', Xtrans_X)
print('Xtrans_Y: ', Xtrans_Y)
    
