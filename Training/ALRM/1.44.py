#Regress Bachelor on per capita income for each of the geographoc regions. State the estimated regression functions
#Are the estimated regression funtions similar?
#Calculate MSE

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

import seaborn as sb #Seaborn is a library for making statistical graphics in Python. It is built on top of matplotlib and closely integrated with pandas data structures.


df_CDI = pd.read_csv("C:\Skrivbordsmapp\Python practice\Applied linear regression models Data\Appendix C datasets\C02.csv"
    , header=None
    , names= [
    "ID number",
    "County",
    "State",
    "Land Area",
    "Total population",
    "Age 18-34 in %",
    "Age 65 or older in %",
    "Number of active physicians",
    "Number of hospital beds",
    "Total serious crimes",
    "High school graduates in %",
    "Bachelor's degrees in %",
    "Below poverty level in %",
    "Unemployment in %",
    "Per capita income",
    "Total personal income",
    "Geographic region"]
    )

df_CDI_reduced = df_CDI[["Per capita income", "Bachelor's degrees in %", "Geographic region"]]

df_CDI_reduced.info()

#Relation between the variables
plt.scatter(df_CDI["Per capita income"], df_CDI["Bachelor's degrees in %"])
plt.show()

#Normality of the variables
sb.distplot(df_CDI["Per capita income"])
plt.show()
sb.distplot(df_CDI["Bachelor's degrees in %"])
plt.show()

#Define the regressands and regressors
X1 = df_CDI["Bachelor's degrees in %"][df_CDI["Geographic region"] == 1]
Y1 = df_CDI["Per capita income"][df_CDI["Geographic region"] == 1]

X2 = df_CDI["Bachelor's degrees in %"][df_CDI["Geographic region"] == 2]
Y2 = df_CDI["Per capita income"][df_CDI["Geographic region"] == 2]

X3 = df_CDI["Bachelor's degrees in %"][df_CDI["Geographic region"] == 3]
Y3 = df_CDI["Per capita income"][df_CDI["Geographic region"] == 3]

X4 = df_CDI["Bachelor's degrees in %"][df_CDI["Geographic region"] == 4]
Y4 = df_CDI["Per capita income"][df_CDI["Geographic region"] == 4]

######CHECK Unique values
#df_CDI2=df_CDI.drop_duplicates(["Geographic region"])
#df_CDI2
reg1 = LinearRegression()
reg2 = LinearRegression()
reg3 = LinearRegression()
reg4 = LinearRegression()

print('X1: ' , X1[0:4])
X1 = X1.values.reshape(-1,1)
Y1 = Y1.values.reshape(-1,1)

print('X1 reshaped: ' , X1[0:4])

X2 = X2.values.reshape(-1,1)
Y2 = Y2.values.reshape(-1,1)

X3 = X3.values.reshape(-1,1)
Y3 = Y3.values.reshape(-1,1)

X4 = X4.values.reshape(-1,1)
Y4 = Y4.values.reshape(-1,1)

reg1.fit(X1,Y1)
reg2.fit(X2,Y2)
reg3.fit(X3,Y3)
reg4.fit(X4,Y4)

print('Y =', reg1.intercept_, '+ X1', reg1.coef_)
print('Y =', reg2.intercept_, '+ X2', reg2.coef_)
print('Y =', reg3.intercept_, '+ X3', reg3.coef_)
print('Y =', reg4.intercept_, '+ X4', reg4.coef_)

pred1 = reg1.predict(X1)
pred2 = reg2.predict(X2)
pred3 = reg3.predict(X3)
pred4 = reg4.predict(X4)

Sqr_residual_1 =  (pred1 - Y1)**2
Sqr_residual_2 =  (pred2 - Y2)**2
Sqr_residual_3 =  (pred3 - Y3)**2
Sqr_residual_4 =  (pred4 - Y4)**2

SSE1 = sum(Sqr_residual_1)
SSE2 = sum(Sqr_residual_2)
SSE3 = sum(Sqr_residual_3)
SSE4 = sum(Sqr_residual_4)

k = 1
n1 = np.ma.size(pred1,0)
n2 = np.ma.size(pred2,0)
n3 = np.ma.size(pred3,0)
n4 = np.ma.size(pred4,0)

MSE1 = SSE1 / (n1 - k - 1)
MSE2 = SSE2 / (n2 - k - 1)
MSE3 = SSE3 / (n3 - k - 1)
MSE4 = SSE4 / (n4 - k - 1)

print('MSE1: ', MSE1, 'MSE2: ', MSE2, 'MSE3: ', MSE3, 'MSE4: ', MSE4)
