
import numpy as np #Matrix operations.
import pandas as pd #For importing data into dataframe. Dependency towards numpy
from sklearn import linear_model as lm #For running linear regression

df_C02 = pd.read_csv("C:\Skrivbordsmapp\Python\Applied linear regression models Data\Appendix C datasets\C02.csv"
                     ,header=None
                     ,names= [
                            "ID number",
                            "County",
                            "State",
                            "Land Area",
                            "Total population",
                            "Age 18-34 in %",
                            "Age 65 or older in %",
                            "Number of active physicians",
                            "Number of hospital beds",
                            "Total serious crimes",
                            "High school graduates in %",
                            "Bachelor's degrees in %",
                            "Below poverty level in %",
                            "Unemployment in %",
                            "Per capita income",
                            "Total personal income",
                            "Geographic region"])


var_index = [3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16]

r2_list = []

for x in var_index:

    reg = lm.LinearRegression()
    model = reg.fit(df_C02.iloc[:, [x]], df_C02.iloc[:, [7]])
    r2 = reg.score(df_C02.iloc[:, [x]], df_C02.iloc[:, [7]])
    print("Number of active physicians = ",model.intercept_, "+", model.coef_, df_C02.columns[x], "\n Coef. of Determ.:", r2)

    r2_list.append(r2)

#Sortera denna skiten på nåt grymt sätt
