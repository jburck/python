import turtle

turtle1 = turtle.Pen()
turtle2 = turtle.Pen()
turtle3 = turtle.Pen()
turtle4 = turtle.Pen()

turtle1.forward(100)
turtle2.forward(120)
turtle3.forward(120)
turtle4.forward(100)

turtle1.left(90)
turtle2.left(90)
turtle3.right(90)
turtle4.right(90)

turtle1.forward(100)
turtle2.forward(80)
turtle3.forward(80)
turtle4.forward(100)

turtle1.right(90)
turtle2.right(90)
turtle3.left(90)
turtle4.left(90)

turtle1.forward(120)
turtle2.forward(80)
turtle3.forward(80)
turtle4.forward(120)
